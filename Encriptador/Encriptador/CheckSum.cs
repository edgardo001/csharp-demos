﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Encriptador.Utiles
{
//Calcular el checksum en MD5 ó SHA1 de un fichero
//publicado el martes, diciembre 01, 2009
//Muchas veces necesitamos un identificador único para un fichero, que nos garantice además una diferenciación frente a cambios que se puedan producir en el fichero. Uno de los mecanismos ampliamente utilizados es la creación de un checksum (valor numérico que se calcula usando la información de un fichero). Existen muchos algoritmos matemáticos para esta tarea, pero los más extendidos con MD5 y SHA1.
//Aquí les presento una función en C# que les permitirá calcular el checksum MD5 y SHA1 de determinado fichero.
//Para MD5:

//Recuerden siempre poner en uso las bibliotecas criptográficas de .Net
//using System;
//using System.Security.Cryptography;
//using System.IO;

    /// <summary>
    /// Genera hash para ficheros, no se debe instanciar
    /// </summary>
    internal class checkSum
    {
        /// <summary>
        /// Genera hash MD5
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>

        internal static string GetMD5HashFromFile(string fileName)
        {
            MD5 md5 = MD5.Create();
            StringBuilder sb = new StringBuilder();

            using (FileStream fs = File.Open(fileName, FileMode.Open))
                foreach (byte b in md5.ComputeHash(fs))
                    sb.Append(b.ToString("x2").ToLower());
            return sb.ToString();
        }

        /// <summary>
        /// Genera hash SHA1
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>

        internal static string GetSHA1HashFromFile(string fileName)
        {
            SHA1 sha1 = SHA1.Create();
            StringBuilder sb = new StringBuilder();

            using (FileStream fs = File.Open(fileName, FileMode.Open))
                foreach (byte b in sha1.ComputeHash(fs))
                    sb.Append(b.ToString("x2").ToLower());
            return sb.ToString();
        }
    }
}
