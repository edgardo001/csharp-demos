﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Encriptador
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            txtKey.Text = "Passw0rd";
        }

        private void btnEncriptar_Click(object sender, EventArgs e)
        {
            try
            {
                txtResultado.Text = Encriptador.Utiles.Encrypt.EncryptKey(txtFrase.Text, txtKey.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void btnDesencriptar_Click(object sender, EventArgs e)
        {
            try
            {
                txtResultado.Text = Encriptador.Utiles.Encrypt.DecryptKey(txtFrase.Text, txtKey.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            try
            {
                txtKey.Text = "";
                txtFrase.Text = "";
                txtResultado.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void btnCopiar_Click(object sender, EventArgs e)
        {
            try
            {
                Clipboard.SetDataObject(txtResultado.Text);             
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void btnSeleccionar_Click(object sender, EventArgs e)
        {
            try
            {
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string sFileName = openFileDialog1.FileName;
                    txtRutaFichero.Text = sFileName;
                    txtCheckSum.Text = Encriptador.Utiles.checkSum.GetMD5HashFromFile(sFileName);
                }                   
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void txtCopiarCheckSum_Click(object sender, EventArgs e)
        {
            try
            {
                Clipboard.SetDataObject(txtCheckSum.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void btnLimpiarCheckSum_Click(object sender, EventArgs e)
        {
            try
            {
                txtRutaFichero.Text = "";
                txtCheckSum.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void txtInvertir_Click(object sender, EventArgs e)
        {
            try
            {
                txtFrase.Text = txtResultado.Text;
                txtResultado.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }
    }
}
