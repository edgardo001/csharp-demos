﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Finisar.SQLite;// se debe agregar la referencia de SQLite.net (SQLite.NET.dll)
//ademas se deben copiar sqlite3.dll y SQlite.dll (para version 2)al directorio donde estara el ejecutable
//Lo que yo hice, fue agregarlas al proyecto como elementos existentes,
//luego seleccionarlas en proyecto e ir a propiedades, luego modificar:
//"Copiar en el directorio de salida: Copiar Siempre"

/** Todo lo necesario esta dentro del proyecto, revisar el interior de sus carpeta **/

namespace conexionSqlite
{
    class Program
    {

        static void Main(string[] args)
        {
            SQLiteConnection conn = new SQLiteConnection();
            conn.ConnectionString = @"Data Source=bd.db;Version=3;New=True;Compress=True;";//Para crear la db si no existe
            try
            {
                //Abro la conexion
                conn.Open();
                Console.WriteLine("Conexion Sqlite3 OK");
                String createTable = "CREATE TABLE COMPANY(" +
                                    "ID INT PRIMARY KEY NOT NULL," +
                                    "NAME TEXT NOT NULL," +
                                    "AGE INT NOT NULL," +
                                    "ADDRESS CHAR(50), " +
                                    "SALARY REAL" +
                                    "); ";
                //Creo una tabla
                SQLiteCommand command = new SQLiteCommand(createTable, conn);
                int varCreateTable = command.ExecuteNonQuery();
                Console.WriteLine("Se crea tabla COMPANY con retorno ExecuteNonQuery(): " + varCreateTable);

                //inserto un registro en la tabla
                command.CommandText = "insert into company(ID, NAME, AGE, ADDRESS, SALARY)values(1, 'edgardo vasquez', 26, 'Av Independencia #1234', 1000000);";
                int varInsertData = command.ExecuteNonQuery();
                Console.WriteLine("Se inserta registro en COMPANY con retorno ExecuteNonQuery(): " + varCreateTable);

                //obtengo el registro de la tabla
                command.CommandText = "Select * from COMPANY";
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Console.WriteLine("El nombre ingresado es: " + reader["NAME"].ToString());
                    Console.WriteLine("Su sueldo es de: " + reader["SALARY"].ToString());
                }

                conn.Close();
                Console.WriteLine("Se cierra conexion a Sqlite3");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to connect to data source: " + ex.Message);
            }
            finally
            {
                //Cierro la conexion
                conn.Close();
                Console.ReadLine();
            }
        }
    }
}
