﻿namespace trocearImagen_cs
{
    partial class fTamañoDinamico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.chkAjustarAuto = new System.Windows.Forms.CheckBox();
            this.Label2 = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.Label3 = new System.Windows.Forms.Label();
            this.txtColumnas = new System.Windows.Forms.TextBox();
            this.txtFilas = new System.Windows.Forms.TextBox();
            this.btnTrocear = new System.Windows.Forms.Button();
            this.gbImagenes = new System.Windows.Forms.GroupBox();
            this.btnExaminar = new System.Windows.Forms.Button();
            this.txtImagen = new System.Windows.Forms.TextBox();
            this.picMain = new System.Windows.Forms.PictureBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picMain)).BeginInit();
            this.SuspendLayout();
            // 
            // chkAjustarAuto
            // 
            this.chkAjustarAuto.AutoSize = true;
            this.chkAjustarAuto.Location = new System.Drawing.Point(241, 42);
            this.chkAjustarAuto.Name = "chkAjustarAuto";
            this.chkAjustarAuto.Size = new System.Drawing.Size(142, 17);
            this.chkAjustarAuto.TabIndex = 7;
            this.chkAjustarAuto.Text = "Ajustar automáticamente";
            this.toolTip1.SetToolTip(this.chkAjustarAuto, "Ajustar la imagen automáticamente al cambiar de tamaño");
            this.chkAjustarAuto.UseVisualStyleBackColor = true;
            this.chkAjustarAuto.CheckedChanged += new System.EventHandler(this.chkAjustarAuto_CheckedChanged);
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Location = new System.Drawing.Point(12, 43);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(121, 13);
            this.Label2.TabIndex = 3;
            this.Label2.Text = "Dimensiones, columnas:";
            this.toolTip1.SetToolTip(this.Label2, "El número de columnas y filas (estos valores solo se tienen en cuenta al pulsar e" +
                    "n Trocear)");
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(12, 17);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(90, 13);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Imagen a trocear:";
            this.toolTip1.SetToolTip(this.Label1, "La imagen a trocear");
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(173, 43);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(28, 13);
            this.Label3.TabIndex = 5;
            this.Label3.Text = "filas:";
            // 
            // txtColumnas
            // 
            this.txtColumnas.Location = new System.Drawing.Point(139, 40);
            this.txtColumnas.Name = "txtColumnas";
            this.txtColumnas.Size = new System.Drawing.Size(28, 20);
            this.txtColumnas.TabIndex = 4;
            this.txtColumnas.Text = "3";
            // 
            // txtFilas
            // 
            this.txtFilas.Location = new System.Drawing.Point(207, 40);
            this.txtFilas.Name = "txtFilas";
            this.txtFilas.Size = new System.Drawing.Size(28, 20);
            this.txtFilas.TabIndex = 6;
            this.txtFilas.Text = "3";
            // 
            // btnTrocear
            // 
            this.btnTrocear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTrocear.Location = new System.Drawing.Point(390, 38);
            this.btnTrocear.Name = "btnTrocear";
            this.btnTrocear.Size = new System.Drawing.Size(75, 23);
            this.btnTrocear.TabIndex = 8;
            this.btnTrocear.Text = "Trocear";
            this.btnTrocear.UseVisualStyleBackColor = true;
            this.btnTrocear.Click += new System.EventHandler(this.btnTrocear_Click);
            // 
            // gbImagenes
            // 
            this.gbImagenes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbImagenes.Location = new System.Drawing.Point(231, 75);
            this.gbImagenes.Margin = new System.Windows.Forms.Padding(12, 12, 4, 4);
            this.gbImagenes.Name = "gbImagenes";
            this.gbImagenes.Size = new System.Drawing.Size(233, 262);
            this.gbImagenes.TabIndex = 9;
            this.gbImagenes.TabStop = false;
            this.gbImagenes.Text = "Imagen troceada:";
            this.gbImagenes.Resize += new System.EventHandler(this.gbImagenes_Resize);
            // 
            // btnExaminar
            // 
            this.btnExaminar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExaminar.Location = new System.Drawing.Point(390, 12);
            this.btnExaminar.Name = "btnExaminar";
            this.btnExaminar.Size = new System.Drawing.Size(75, 23);
            this.btnExaminar.TabIndex = 2;
            this.btnExaminar.Text = "Examinar...";
            this.btnExaminar.UseVisualStyleBackColor = true;
            this.btnExaminar.Click += new System.EventHandler(this.btnExaminar_Click);
            // 
            // txtImagen
            // 
            this.txtImagen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtImagen.Location = new System.Drawing.Point(108, 14);
            this.txtImagen.Name = "txtImagen";
            this.txtImagen.Size = new System.Drawing.Size(276, 20);
            this.txtImagen.TabIndex = 1;
            this.txtImagen.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtImagen_KeyPress);
            // 
            // picMain
            // 
            this.picMain.Image = global::trocearImagen_cs.Properties.Resources.Guille28sep06_1003_240x308;
            this.picMain.Location = new System.Drawing.Point(21, 75);
            this.picMain.Margin = new System.Windows.Forms.Padding(12, 12, 12, 3);
            this.picMain.Name = "picMain";
            this.picMain.Size = new System.Drawing.Size(186, 235);
            this.picMain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picMain.TabIndex = 19;
            this.picMain.TabStop = false;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(21, 315);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(186, 16);
            this.progressBar1.TabIndex = 20;
            // 
            // timer1
            // 
            this.timer1.Interval = 300;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // fTamañoDinamico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(477, 349);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.chkAjustarAuto);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.txtColumnas);
            this.Controls.Add(this.txtFilas);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.btnTrocear);
            this.Controls.Add(this.gbImagenes);
            this.Controls.Add(this.picMain);
            this.Controls.Add(this.btnExaminar);
            this.Controls.Add(this.txtImagen);
            this.Controls.Add(this.Label1);
            this.MinimumSize = new System.Drawing.Size(490, 380);
            this.Name = "fTamañoDinamico";
            this.Text = "Trocear una imagen con tamaños dinámicos (C#)";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.fTamañoDinamico_FormClosing);
            this.Load += new System.EventHandler(this.fTamañoDinamico_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picMain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnTrocear;
        private System.Windows.Forms.CheckBox chkAjustarAuto;
        private System.Windows.Forms.Label Label3;
        private System.Windows.Forms.TextBox txtColumnas;
        private System.Windows.Forms.TextBox txtFilas;
        private System.Windows.Forms.Label Label2;
        private System.Windows.Forms.GroupBox gbImagenes;
        private System.Windows.Forms.PictureBox picMain;
        private System.Windows.Forms.Button btnExaminar;
        private System.Windows.Forms.TextBox txtImagen;
        private System.Windows.Forms.Label Label1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Timer timer1;
    }
}