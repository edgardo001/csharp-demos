//-----------------------------------------------------------------------------
// Trocear una imagen                                               (17/Ene/07)
//
// �Guillermo 'guille' Som, 2007
//-----------------------------------------------------------------------------
using System;
using System.Windows.Forms;
using System.Drawing;

namespace trocearImagen_cs
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnExaminar_Click(object sender, EventArgs e)
        {
            OpenFileDialog oFD = new OpenFileDialog();
            oFD.Title = "Seleccionar imagen";
            oFD.Filter = "Im�genes|*.jpg;*.gif;*.png;*.bmp|Todos (*.*)|*.*";
            oFD.FileName = this.txtImagen.Text;
            if (oFD.ShowDialog() == DialogResult.OK)
            {
                this.txtImagen.Text = oFD.FileName;
                this.picMain.Image = Image.FromFile(this.txtImagen.Text);
            }
        }

        private void btnTrocear_Click(object sender, EventArgs e)
        {
            // Trocear una imagen en trozos m�s peque�os
            const int columnas = 3;
            const int filas = 3;
            //
            // El tama�o proporcional del ancho y alto
            // correspondientes a los trozos a usar
            int tamTrozoW = this.picMain.Image.Width / columnas;
            int tamTrozoH = this.picMain.Image.Height / filas;
            // El tama�o de cada trozo
            int nW = pic0.Width;
            int nH = pic0.Height;
            // El rect�ngulo que ocupar� cada nuevo trozo
            Rectangle rectDest = new Rectangle(0, 0, nW, nH);
            // Estas variables se usan en el bucle
            Bitmap bmpDest;
            Graphics g;
            Rectangle rectOrig;
            //
            // Array con los pictures que hay en el formulario
            PictureBox[] trozos = { pic0, pic1, pic2, pic3, pic4, pic5, pic6, pic7, pic8 };
            // Para contar cada columna
            int c = 0;
            // La posici�n X e Y en la imagen original
            int pX = 0, pY = 0;
            for (int i = 0; i < trozos.Length; i++)
            {
                // El trozo de la imagen original
                rectOrig = new Rectangle(pX, pY, tamTrozoW, tamTrozoH);
                // La imagen de destino
                bmpDest = new Bitmap(tamTrozoW, tamTrozoW);
                g = Graphics.FromImage(bmpDest);
                // Obtenemos un trozo de la imagen original
                // y lo dibujamos en la imagen de destino
                g.DrawImage(picMain.Image, rectDest, rectOrig, GraphicsUnit.Pixel);
                // Asignamos la nueva imagen al picture correspondiente
                trozos[i].Image = bmpDest;
                c += 1;
                pX += tamTrozoW;
                // Cuando hayamos recorrido las columnas,
                // pasamos a la siguiente fila
                if (c >= columnas)
                {
                    c = 0;
                    pX = 0;
                    pY += tamTrozoH;
                }
            }
        }
    }
}
