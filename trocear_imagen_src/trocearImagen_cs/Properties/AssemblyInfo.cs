﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("trocearImagen_cs")]
[assembly: AssemblyDescription("Utilidad para trocear imágenes")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("elGuille -Nerja")]
[assembly: AssemblyProduct("trocearImagen_cs, revisión del 18/Ene/2007")]
[assembly: AssemblyCopyright("©Guillermo 'guille' Som,  2007")]
[assembly: AssemblyTrademark("Microsoft Visual C# 2005 (.NET 2.0)")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("a659a18a-2d61-4706-8d95-4fe71dfdcbf1")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.2")]
