namespace trocearImagen_cs
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnTrocear = new System.Windows.Forms.Button();
            this.gbImagenes = new System.Windows.Forms.GroupBox();
            this.pic8 = new System.Windows.Forms.PictureBox();
            this.pic7 = new System.Windows.Forms.PictureBox();
            this.pic6 = new System.Windows.Forms.PictureBox();
            this.pic5 = new System.Windows.Forms.PictureBox();
            this.pic4 = new System.Windows.Forms.PictureBox();
            this.pic3 = new System.Windows.Forms.PictureBox();
            this.pic2 = new System.Windows.Forms.PictureBox();
            this.pic1 = new System.Windows.Forms.PictureBox();
            this.pic0 = new System.Windows.Forms.PictureBox();
            this.picMain = new System.Windows.Forms.PictureBox();
            this.btnExaminar = new System.Windows.Forms.Button();
            this.txtImagen = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.gbImagenes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMain)).BeginInit();
            this.SuspendLayout();
            // 
            // btnTrocear
            // 
            this.btnTrocear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTrocear.Location = new System.Drawing.Point(343, 262);
            this.btnTrocear.Name = "btnTrocear";
            this.btnTrocear.Size = new System.Drawing.Size(75, 23);
            this.btnTrocear.TabIndex = 4;
            this.btnTrocear.Text = "Trocear";
            this.btnTrocear.UseVisualStyleBackColor = true;
            this.btnTrocear.Click += new System.EventHandler(this.btnTrocear_Click);
            // 
            // gbImagenes
            // 
            this.gbImagenes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbImagenes.Controls.Add(this.pic8);
            this.gbImagenes.Controls.Add(this.pic7);
            this.gbImagenes.Controls.Add(this.pic6);
            this.gbImagenes.Controls.Add(this.pic5);
            this.gbImagenes.Controls.Add(this.pic4);
            this.gbImagenes.Controls.Add(this.pic3);
            this.gbImagenes.Controls.Add(this.pic2);
            this.gbImagenes.Controls.Add(this.pic1);
            this.gbImagenes.Controls.Add(this.pic0);
            this.gbImagenes.Location = new System.Drawing.Point(241, 48);
            this.gbImagenes.Margin = new System.Windows.Forms.Padding(12);
            this.gbImagenes.Name = "gbImagenes";
            this.gbImagenes.Size = new System.Drawing.Size(172, 186);
            this.gbImagenes.TabIndex = 3;
            this.gbImagenes.TabStop = false;
            this.gbImagenes.Text = "Imagen troceada:";
            // 
            // pic8
            // 
            this.pic8.Location = new System.Drawing.Point(114, 127);
            this.pic8.Margin = new System.Windows.Forms.Padding(1);
            this.pic8.Name = "pic8";
            this.pic8.Size = new System.Drawing.Size(52, 52);
            this.pic8.TabIndex = 8;
            this.pic8.TabStop = false;
            // 
            // pic7
            // 
            this.pic7.Location = new System.Drawing.Point(60, 127);
            this.pic7.Margin = new System.Windows.Forms.Padding(1);
            this.pic7.Name = "pic7";
            this.pic7.Size = new System.Drawing.Size(52, 52);
            this.pic7.TabIndex = 7;
            this.pic7.TabStop = false;
            // 
            // pic6
            // 
            this.pic6.Location = new System.Drawing.Point(6, 127);
            this.pic6.Margin = new System.Windows.Forms.Padding(1);
            this.pic6.Name = "pic6";
            this.pic6.Size = new System.Drawing.Size(52, 52);
            this.pic6.TabIndex = 6;
            this.pic6.TabStop = false;
            // 
            // pic5
            // 
            this.pic5.Location = new System.Drawing.Point(114, 73);
            this.pic5.Margin = new System.Windows.Forms.Padding(1);
            this.pic5.Name = "pic5";
            this.pic5.Size = new System.Drawing.Size(52, 52);
            this.pic5.TabIndex = 5;
            this.pic5.TabStop = false;
            // 
            // pic4
            // 
            this.pic4.Location = new System.Drawing.Point(60, 73);
            this.pic4.Margin = new System.Windows.Forms.Padding(1);
            this.pic4.Name = "pic4";
            this.pic4.Size = new System.Drawing.Size(52, 52);
            this.pic4.TabIndex = 4;
            this.pic4.TabStop = false;
            // 
            // pic3
            // 
            this.pic3.Location = new System.Drawing.Point(6, 73);
            this.pic3.Margin = new System.Windows.Forms.Padding(1);
            this.pic3.Name = "pic3";
            this.pic3.Size = new System.Drawing.Size(52, 52);
            this.pic3.TabIndex = 3;
            this.pic3.TabStop = false;
            // 
            // pic2
            // 
            this.pic2.Location = new System.Drawing.Point(114, 19);
            this.pic2.Margin = new System.Windows.Forms.Padding(1);
            this.pic2.Name = "pic2";
            this.pic2.Size = new System.Drawing.Size(52, 52);
            this.pic2.TabIndex = 2;
            this.pic2.TabStop = false;
            // 
            // pic1
            // 
            this.pic1.Location = new System.Drawing.Point(60, 19);
            this.pic1.Margin = new System.Windows.Forms.Padding(1);
            this.pic1.Name = "pic1";
            this.pic1.Size = new System.Drawing.Size(52, 52);
            this.pic1.TabIndex = 1;
            this.pic1.TabStop = false;
            // 
            // pic0
            // 
            this.pic0.Location = new System.Drawing.Point(6, 19);
            this.pic0.Margin = new System.Windows.Forms.Padding(1);
            this.pic0.Name = "pic0";
            this.pic0.Size = new System.Drawing.Size(52, 52);
            this.pic0.TabIndex = 0;
            this.pic0.TabStop = false;
            // 
            // picMain
            // 
            this.picMain.Image = global::trocearImagen_cs.Properties.Resources.Guille28sep06_1003_240x308;
            this.picMain.Location = new System.Drawing.Point(21, 48);
            this.picMain.Margin = new System.Windows.Forms.Padding(12);
            this.picMain.Name = "picMain";
            this.picMain.Size = new System.Drawing.Size(186, 237);
            this.picMain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picMain.TabIndex = 9;
            this.picMain.TabStop = false;
            // 
            // btnExaminar
            // 
            this.btnExaminar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExaminar.Location = new System.Drawing.Point(343, 11);
            this.btnExaminar.Name = "btnExaminar";
            this.btnExaminar.Size = new System.Drawing.Size(75, 23);
            this.btnExaminar.TabIndex = 2;
            this.btnExaminar.Text = "Examinar...";
            this.btnExaminar.UseVisualStyleBackColor = true;
            this.btnExaminar.Click += new System.EventHandler(this.btnExaminar_Click);
            // 
            // txtImagen
            // 
            this.txtImagen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtImagen.Location = new System.Drawing.Point(108, 13);
            this.txtImagen.Name = "txtImagen";
            this.txtImagen.Size = new System.Drawing.Size(229, 20);
            this.txtImagen.TabIndex = 1;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(12, 16);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(90, 13);
            this.Label1.TabIndex = 0;
            this.Label1.Text = "Imagen a trocear:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 296);
            this.Controls.Add(this.btnTrocear);
            this.Controls.Add(this.gbImagenes);
            this.Controls.Add(this.picMain);
            this.Controls.Add(this.btnExaminar);
            this.Controls.Add(this.txtImagen);
            this.Controls.Add(this.Label1);
            this.MinimumSize = new System.Drawing.Size(440, 330);
            this.Name = "Form1";
            this.Text = "Trocear imagen (C#)";
            this.gbImagenes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMain)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnTrocear;
        internal System.Windows.Forms.GroupBox gbImagenes;
        internal System.Windows.Forms.PictureBox pic8;
        internal System.Windows.Forms.PictureBox pic7;
        internal System.Windows.Forms.PictureBox pic6;
        internal System.Windows.Forms.PictureBox pic5;
        internal System.Windows.Forms.PictureBox pic4;
        internal System.Windows.Forms.PictureBox pic3;
        internal System.Windows.Forms.PictureBox pic2;
        internal System.Windows.Forms.PictureBox pic1;
        internal System.Windows.Forms.PictureBox pic0;
        internal System.Windows.Forms.Button btnExaminar;
        internal System.Windows.Forms.TextBox txtImagen;
        internal System.Windows.Forms.Label Label1;
        private System.Windows.Forms.PictureBox picMain;
    }
}

