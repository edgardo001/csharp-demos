﻿//-----------------------------------------------------------------------------
// Trocear una imagen                                               (17/Ene/07)
// Las imágenes de destino pueden cambiar de tamaño
//
// ©Guillermo 'guille' Som, 2007
//-----------------------------------------------------------------------------
using System;
using System.Windows.Forms;
using System.Drawing;

namespace trocearImagen_cs
{
    public partial class fTamañoDinamico : Form
    {
        public fTamañoDinamico()
        {
            InitializeComponent();
        }

        private string ficImagen;
        // El número de dimensiones, por ejemplo 3x3 = 9 trozos
        private int nColumnas = 3;
        private int nFilas = 3;
        // Para que no se produzca el evento Resize mientras se inicia
        private bool iniciando = true;
        // Constante con el número máximo de filas y columnas
        const int maxTrozos = 50;

        Properties.Settings mySettings = new trocearImagen_cs.Properties.Settings();

        private void fTamañoDinamico_Load(object sender, EventArgs e)
        {
            // Asignar los valores de la configuración
            ficImagen = mySettings.Fichero;
            nColumnas = mySettings.Columnas;
            nFilas = mySettings.Filas;
            this.txtColumnas.Text = nColumnas.ToString();
            this.txtFilas.Text = nFilas.ToString();
            this.chkAjustarAuto.Checked = mySettings.AjustarAuto;
            this.txtImagen.Text = ficImagen;
            // Si no existe la imagen, dejamos la mía
            if (System.IO.File.Exists(ficImagen))
            {
                this.picMain.Image = Image.FromFile(ficImagen);
            }
            else
            {
                this.picMain.Image = Properties.Resources.Guille28sep06_1003_240x308;
            }
            // Mostrar los trozos
            //trocearImagen(nColumnas, nFilas);
            // Para que le de tiempo al formulario a mostrarse
            timer1.Interval = 100;
            timer1.Start();
            iniciando = false;
        }

        private void fTamañoDinamico_FormClosing(object sender, FormClosingEventArgs e)
        {
            // C# no guarda automáticamente los cambios
            mySettings.Save();
        }

        private void btnExaminar_Click(object sender, EventArgs e)
        {
            OpenFileDialog oFD = new OpenFileDialog();
            oFD.Title = "Seleccionar imagen";
            oFD.Filter = "Imágenes|*.jpg;*.gif;*.png;*.bmp|Todos (*.*)|*.*";
            oFD.FileName = this.txtImagen.Text;
            if (oFD.ShowDialog() == DialogResult.OK)
            {
                this.txtImagen.Text = oFD.FileName;
                this.picMain.Image = Image.FromFile(this.txtImagen.Text);
                mySettings.Fichero = this.txtImagen.Text;
            }
        }

        private void chkAjustarAuto_CheckedChanged(object sender, EventArgs e)
        {
            mySettings.AjustarAuto = chkAjustarAuto.Checked;
        }

        private void txtImagen_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                ficImagen = this.txtImagen.Text;
                mySettings.Fichero = "";
                if (String.IsNullOrEmpty(ficImagen))
                {
                    this.picMain.Image = Properties.Resources.Guille28sep06_1003_240x308;
                }
            }
        }

        private void btnTrocear_Click(System.Object sender, EventArgs e)
        {
            // Trocear una imagen en trozos más pequeños
            // Asignar el tamño según las columnas y filas indicadas
            // Comprobar que son datos correctos
            bool novalido = false;
            try
            {
                nColumnas = Convert.ToInt32(txtColumnas.Text);
                if (nColumnas < 1 || nColumnas > maxTrozos)
                {
                    novalido = true;
                }
            }
            catch 
            {
                novalido = true;
            }
            if (novalido)
            {
                MessageBox.Show("Debes indicar un número entero de 1 a " + 
                                maxTrozos + " en las columnas");
                txtColumnas.Focus();
                return;
            }
            try
            {
                nFilas = Convert.ToInt32(txtFilas.Text);
                if (nFilas < 1 || nFilas > maxTrozos)
                {
                    novalido = true;
                }
            }
            catch
            {
                novalido = true;
            }
            if (novalido)
            {
                MessageBox.Show("Debes indicar un número entero de 1 a " + 
                                maxTrozos + " en las filas");
                txtFilas.Focus();
                return;
            }
            // Guardar los valores en la configuración
            mySettings.Columnas = nColumnas;
            mySettings.Filas = nFilas;
            trocearImagen(nColumnas, nFilas);
        }

        private void gbImagenes_Resize(object sender, EventArgs e)
        {
            if (iniciando) return;
            // El número de filas y de columnas será
            // el usado la última vez al pulsar en trocear
            if (this.chkAjustarAuto.Checked)
            {
                trocearImagen(nColumnas, nFilas);
            }
        }

        private void trocearImagen(int columnas, int filas)
        {
            progressBar1.Maximum = columnas * filas;
            progressBar1.Value = 1;

            // Separación entre la imagen y el borde del contenedor
            const int sepX = 6;
            const int sepY = 16;
            // La separación entre cada trozo
            const int sep = 2;
            // El tamaño de cada imagen será:
            // Ancho del groupBox \ columnas - columnas
            // Alto del groupBox \ filas - filas
            // Al usar columnas y filas, se separa más de la cuenta o menos de lo deseado
            // Pero dejando 2 para el ancho y (filas\2) para el alto parece que está bien
            int tamW = (this.gbImagenes.ClientSize.Width - sepX) / columnas - 2;
            int tamH = (this.gbImagenes.ClientSize.Height - sepY) / filas - 2;
            // El tamaño proporcional del ancho y alto
            // correspondientes a los trozos a usar
            int tamTrozoW = this.picMain.Image.Width / columnas;
            int tamTrozoH = this.picMain.Image.Height / filas;
            // El rectángulo de cada nuevo trozo
            Rectangle rectDest = new Rectangle(0, 0, tamW, tamH);
            // Estas variables se usan en el bucle
            Bitmap bmpDest;
            Graphics g;
            Rectangle rectOri;
            //
            // Array con el número de pictures necesarias
            PictureBox[] trozos = new PictureBox[columnas * filas];
            // Eliminamos las imágenes que tuviera el groupBox
            this.gbImagenes.Controls.Clear();
            // Para contar cada columna y fila
            int c = 0, f = 0;
            // La posición X e Y en la imagen original
            int pX = 0, pY = 0;
            for (int i = 0; i < trozos.Length; i++)
            {
                progressBar1.Value = i + 1;
                //progressBar1.Refresh();

                // El trozo de la imagen original
                rectOri = new Rectangle(pX, pY, tamTrozoW, tamTrozoH);
                // La imagen de destino
                bmpDest = new Bitmap(tamW, tamH);
                g = Graphics.FromImage(bmpDest);
                // Obtenemos un trozo de la imagen original
                // y lo dibujamos en la imagen de destino
                g.DrawImage(this.picMain.Image, rectDest, rectOri, GraphicsUnit.Pixel);
                //
                // Asignamos la nueva imagen al picture correspondiente
                // creamos el picture y lo asignamos al groupBox
                trozos[i] = new PictureBox();
                this.gbImagenes.Controls.Add(trozos[i]);
                // Asignamos el tamaño
                trozos[i].Size = new Size(tamW, tamH);
                // Posicionamos el picture
                trozos[i].Location = new Point(tamW * c + (sep * c) + sepX, 
                                               tamH * f + (sep * f) + sepY);
                // Asignar la imagen creada
                trozos[i].Image = bmpDest;
                //
                // Calculamos la posición del próximo trozo
                c += 1;
                pX += tamTrozoW;
                // Cuando hayamos recorrido las columnas,
                // pasamos a la siguiente fila
                if (c >= columnas)
                {
                    c = 0;
                    f += 1;
                    pX = 0;
                    pY += tamTrozoH;
                }
            }
            timer1.Start();
            //progressBar1.Value = 0;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            // Para el evento Load
            if ( timer1.Interval == 100)
            {
                timer1.Interval = 300;
                trocearImagen(nColumnas, nFilas);
            }
            progressBar1.Value = 0;
        }
    }
}
