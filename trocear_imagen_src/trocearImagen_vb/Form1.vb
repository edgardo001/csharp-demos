'------------------------------------------------------------------------------
' Trocear una imagen                                                (17/Ene/07)
'
' �Guillermo 'guille' Som, 2007
'------------------------------------------------------------------------------
Option Strict On

Imports Microsoft.VisualBasic
Imports System
Imports System.Windows.Forms
Imports System.Drawing

Public Class Form1

    'Private Sub Form1_Load(ByVal sender As Object, ByVal e As EventArgs) _
    '            Handles MyBase.Load
    '    'Me.txtImagen.Text = Application.StartupPath & "\Guille28sep06_1003_240x308.JPG"
    'End Sub

    Private Sub btnExaminar_Click(ByVal sender As Object, ByVal e As EventArgs) _
                Handles btnExaminar.Click
        Dim oFD As New OpenFileDialog
        oFD.Title = "Seleccionar imagen"
        oFD.Filter = "Im�genes|*.jpg;*.gif;*.png;*.bmp|Todos (*.*)|*.*"
        oFD.FileName = Me.txtImagen.Text
        If oFD.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.txtImagen.Text = oFD.FileName
            Me.picMain.Image = Image.FromFile(Me.txtImagen.Text)
        End If
    End Sub

    Private Sub btnTrocear_Click(ByVal sender As Object, ByVal e As EventArgs) _
                Handles btnTrocear.Click
        ' Trocear una imagen en trozos m�s peque�os
        Const columnas As Integer = 3
        Const filas As Integer = 3
        '
        ' El tama�o proporcional del ancho y alto
        ' correspondientes a los trozos a usar
        Dim tamTrozoW As Integer = Me.picMain.Image.Width \ columnas
        Dim tamTrozoH As Integer = Me.picMain.Image.Height \ filas
        ' El tama�o de cada trozo
        Dim nW As Integer = pic0.Width
        Dim nH As Integer = pic0.Height
        ' El rect�ngulo de cada nuevo trozo
        Dim rectDest As New Rectangle(0, 0, nW, nH)
        ' Estas variables se usan en el bucle
        Dim bmpDest As Bitmap
        Dim g As Graphics
        Dim rectOrig As Rectangle
        '
        ' Array con los pictures que hay en el formulario
        Dim trozos() As PictureBox = {pic0, pic1, pic2, pic3, pic4, pic5, pic6, pic7, pic8}
        ' Para contar cada columna
        Dim c As Integer
        ' La posici�n X e Y en la imagen original
        Dim pX, pY As Integer
        For i As Integer = 0 To trozos.Length - 1
            ' El trozo de la imagen original
            rectOrig = New Rectangle(pX, pY, tamTrozoW, tamTrozoH)
            ' La imagen de destino
            bmpDest = New Bitmap(tamTrozoW, tamTrozoW)
            g = Graphics.FromImage(bmpDest)
            ' Obtenemos un trozo de la imagen original
            ' y lo dibujamos en la imagen de destino
            g.DrawImage(picMain.Image, rectDest, rectOrig, GraphicsUnit.Pixel)
            ' Asignamos la nueva imagen al picture correspondiente
            trozos(i).Image = bmpDest
            c += 1
            pX += tamTrozoW
            ' Cuando hayamos recorrido las columnas,
            ' pasamos a la siguiente fila
            If c >= columnas Then
                c = 0
                pX = 0
                pY += tamTrozoH
            End If
        Next
    End Sub
End Class
