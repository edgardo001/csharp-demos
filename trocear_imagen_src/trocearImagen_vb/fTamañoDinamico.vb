﻿'------------------------------------------------------------------------------
' Trocear una imagen                                                (17/Ene/07)
' Las imágenes de destino pueden cambiar de tamaño
'
' ©Guillermo 'guille' Som, 2007
'------------------------------------------------------------------------------
Option Strict On

Imports Microsoft.VisualBasic
Imports vb = Microsoft.VisualBasic
Imports System
Imports System.Windows.Forms
Imports System.Drawing

Public Class fTamañoDinamico

    Private ficImagen As String
    ' El número de dimensiones, por ejemplo 3x3 = 9 trozos
    Private nColumnas As Integer = 3
    Private nFilas As Integer = 3
    ' Para que no se produzca el evento Resize mientras se inicia
    Private iniciando As Boolean = True
    ' Constante con el número máximo de filas y columnas
    Private Const maxTrozos As Integer = 50

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As EventArgs) _
                Handles MyBase.Load
        ' Asignar los valores de la configuración
        ficImagen = My.Settings.Fichero
        nColumnas = My.Settings.Columnas
        nFilas = My.Settings.Filas
        Me.txtColumnas.Text = nColumnas.ToString
        Me.txtFilas.Text = nFilas.ToString
        Me.chkAjustarAuto.Checked = My.Settings.AjustarAuto
        Me.txtImagen.Text = ficImagen
        ' Si no existe la imagen, dejamos la mía
        If System.IO.File.Exists(ficImagen) Then
            Me.picMain.Image = Image.FromFile(ficImagen)
        Else
            Me.picMain.Image = My.Resources.Guille28sep06_1003_240x308
        End If
        ' Mostrar los trozos
        'trocearImagen(nColumnas, nFilas)
        ' Para que le de tiempo al formulario a mostrarse
        timer1.Interval = 100
        timer1.Start()
        iniciando = False
    End Sub

    Private Sub btnExaminar_Click(ByVal sender As Object, ByVal e As EventArgs) _
                Handles btnExaminar.Click
        Dim oFD As New OpenFileDialog
        oFD.Title = "Seleccionar imagen"
        oFD.Filter = "Imágenes|*.jpg;*.gif;*.png;*.bmp|Todos (*.*)|*.*"
        oFD.FileName = Me.txtImagen.Text
        If oFD.ShowDialog = Windows.Forms.DialogResult.OK Then
            Me.txtImagen.Text = oFD.FileName
            Me.picMain.Image = Image.FromFile(Me.txtImagen.Text)
            My.Settings.Fichero = Me.txtImagen.Text
        End If
    End Sub

    Private Sub chkAjustarAuto_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) _
                Handles chkAjustarAuto.CheckedChanged
        My.Settings.AjustarAuto = chkAjustarAuto.Checked
    End Sub

    Private Sub txtImagen_KeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs) _
                Handles txtImagen.KeyPress
        If e.KeyChar = ChrW(13) Then
            ficImagen = Me.txtImagen.Text
            My.Settings.Fichero = ""
            If String.IsNullOrEmpty(ficImagen) Then
                Me.picMain.Image = My.Resources.Guille28sep06_1003_240x308
            End If
        End If
    End Sub

    Private Sub btnTrocear_Click(ByVal sender As System.Object, ByVal e As EventArgs) _
                Handles btnTrocear.Click
        ' Trocear una imagen en trozos más pequeños
        ' Asignar el tamño según las columnas y filas indicadas
        ' Comprobar que son datos correctos
        Dim novalido As Boolean = False
        If vb.IsNumeric(txtColumnas.Text) = False Then
            novalido = True
        Else
            nColumnas = CInt(txtColumnas.Text)
            If nColumnas < 1 OrElse nColumnas > maxTrozos Then
                novalido = True
            End If
        End If
        If novalido Then
            MessageBox.Show("Debes indicar un número de 1 a " & maxTrozos & " en las columnas")
            txtColumnas.Focus()
            Exit Sub
        End If
        If vb.IsNumeric(txtFilas.Text) = False Then
            novalido = True
        Else
            nFilas = CInt(txtFilas.Text)
            If nFilas < 1 OrElse nFilas > maxTrozos Then
                novalido = True
            End If
        End If
        If novalido Then
            MessageBox.Show("Debes indicar un número de 1 a " & maxTrozos & " en las filas")
            txtFilas.Focus()
            Exit Sub
        End If
        ' Guardar los valores en la configuración
        My.Settings.Columnas = nColumnas
        My.Settings.Filas = nFilas
        trocearImagen(nColumnas, nFilas)
    End Sub

    Private Sub gbImagenes_Resize(ByVal sender As Object, ByVal e As EventArgs) _
                Handles gbImagenes.Resize
        If iniciando Then Exit Sub
        ' El número de filas y de columnas será
        ' el usado la última vez al pulsar en trocear
        If Me.chkAjustarAuto.Checked Then
            trocearImagen(nColumnas, nFilas)
        End If
    End Sub

    Private Sub trocearImagen(ByVal columnas As Integer, ByVal filas As Integer)
        progressBar1.Maximum = columnas * filas
        progressBar1.Value = 1

        ' Separación entre la imagen y el borde del contenedor
        Const sepX As Integer = 6
        Const sepY As Integer = 16
        ' La separación entre cada trozo
        Const sep As Integer = 2
        ' El tamaño de cada imagen será:
        ' Ancho del groupBox \ columnas - columnas
        ' Alto del groupBox \ filas - filas
        ' Al usar columnas y filas, se separa más de la cuenta o menos de lo deseado
        ' Pero dejando 2 para el ancho y (filas\2) para el alto parece que está bien
        Dim tamW As Integer = (Me.gbImagenes.ClientSize.Width - sepX) \ columnas - 2
        Dim tamH As Integer = (Me.gbImagenes.ClientSize.Height - sepY) \ filas - 2
        ' El tamaño proporcional del ancho y alto
        ' correspondientes a los trozos a usar
        Dim tamTrozoW As Integer = Me.picMain.Image.Width \ columnas
        Dim tamTrozoH As Integer = Me.picMain.Image.Height \ filas
        ' El rectángulo de cada nuevo trozo
        Dim rectDest As New Rectangle(0, 0, tamW, tamH)
        ' Estas variables se usan en el bucle
        Dim bmpDest As Bitmap
        Dim g As Graphics
        Dim rectOri As Rectangle
        '
        ' Array con el número de pictures necesarias
        'Dim trozos(0 To (columnas * filas - 1)) As PictureBox
        Dim trozos(columnas * filas - 1) As PictureBox
        ' Eliminamos las imágenes que tuviera el groupBox
        Me.gbImagenes.Controls.Clear()
        ' Para contar cada columna y fila
        Dim c, f As Integer
        ' La posición X e Y en la imagen original
        Dim pX, pY As Integer
        For i As Integer = 0 To trozos.Length - 1
            progressBar1.Value = i + 1

            ' El trozo de la imagen original
            rectOri = New Rectangle(pX, pY, tamTrozoW, tamTrozoH)
            ' La imagen de destino
            bmpDest = New Bitmap(tamW, tamH)
            g = Graphics.FromImage(bmpDest)
            ' Obtenemos un trozo de la imagen original
            ' y lo dibujamos en la imagen de destino
            g.DrawImage(Me.picMain.Image, rectDest, rectOri, GraphicsUnit.Pixel)
            '
            ' Asignamos la nueva imagen al picture correspondiente
            ' creamos el picture y lo asignamos al groupBox
            trozos(i) = New PictureBox
            Me.gbImagenes.Controls.Add(trozos(i))
            ' Asignamos el tamaño
            trozos(i).Size = New Size(tamW, tamH)
            ' Posicionamos el picture
            trozos(i).Location = New Point(tamW * c + (sep * c) + sepX, _
                                           tamH * f + (sep * f) + sepY)
            ' Asignar la imagen creada
            trozos(i).Image = bmpDest
            '
            ' Calculamos la posición del próximo trozo
            c += 1
            pX += tamTrozoW
            ' Cuando hayamos recorrido las columnas,
            ' pasamos a la siguiente fila
            If c >= columnas Then
                c = 0
                f += 1
                pX = 0
                pY += tamTrozoH
            End If
        Next
        timer1.Start()
    End Sub

    Private Sub timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timer1.Tick
        timer1.Stop()
        ' Para el evento Load
        If timer1.Interval = 100 Then
            timer1.Interval = 300
            trocearImagen(nColumnas, nFilas)
        End If
        progressBar1.Value = 0
    End Sub
End Class