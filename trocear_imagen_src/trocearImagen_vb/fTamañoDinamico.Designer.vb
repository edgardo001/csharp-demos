﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class fTamañoDinamico
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.btnTrocear = New System.Windows.Forms.Button
        Me.gbImagenes = New System.Windows.Forms.GroupBox
        Me.btnExaminar = New System.Windows.Forms.Button
        Me.txtImagen = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.txtFilas = New System.Windows.Forms.TextBox
        Me.txtColumnas = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.chkAjustarAuto = New System.Windows.Forms.CheckBox
        Me.toolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.picMain = New System.Windows.Forms.PictureBox
        Me.progressBar1 = New System.Windows.Forms.ProgressBar
        Me.timer1 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.picMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnTrocear
        '
        Me.btnTrocear.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnTrocear.Location = New System.Drawing.Point(390, 37)
        Me.btnTrocear.Name = "btnTrocear"
        Me.btnTrocear.Size = New System.Drawing.Size(75, 23)
        Me.btnTrocear.TabIndex = 8
        Me.btnTrocear.Text = "Trocear"
        Me.btnTrocear.UseVisualStyleBackColor = True
        '
        'gbImagenes
        '
        Me.gbImagenes.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbImagenes.Location = New System.Drawing.Point(231, 74)
        Me.gbImagenes.Margin = New System.Windows.Forms.Padding(12, 12, 4, 4)
        Me.gbImagenes.Name = "gbImagenes"
        Me.gbImagenes.Size = New System.Drawing.Size(233, 255)
        Me.gbImagenes.TabIndex = 9
        Me.gbImagenes.TabStop = False
        Me.gbImagenes.Text = "Imagen troceada:"
        '
        'btnExaminar
        '
        Me.btnExaminar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExaminar.Location = New System.Drawing.Point(390, 11)
        Me.btnExaminar.Name = "btnExaminar"
        Me.btnExaminar.Size = New System.Drawing.Size(75, 23)
        Me.btnExaminar.TabIndex = 2
        Me.btnExaminar.Text = "Examinar..."
        Me.btnExaminar.UseVisualStyleBackColor = True
        '
        'txtImagen
        '
        Me.txtImagen.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtImagen.Location = New System.Drawing.Point(108, 13)
        Me.txtImagen.Name = "txtImagen"
        Me.txtImagen.Size = New System.Drawing.Size(276, 20)
        Me.txtImagen.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(90, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Imagen a trocear:"
        Me.toolTip1.SetToolTip(Me.Label1, "La imagen a trocear")
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 42)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(121, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Dimensiones, columnas:"
        Me.toolTip1.SetToolTip(Me.Label2, "El número de columnas y filas (estos valores solo se tienen en cuenta al pulsar e" & _
                "n Trocear)")
        '
        'txtFilas
        '
        Me.txtFilas.Location = New System.Drawing.Point(207, 39)
        Me.txtFilas.Name = "txtFilas"
        Me.txtFilas.Size = New System.Drawing.Size(28, 20)
        Me.txtFilas.TabIndex = 6
        Me.txtFilas.Text = "3"
        '
        'txtColumnas
        '
        Me.txtColumnas.Location = New System.Drawing.Point(139, 39)
        Me.txtColumnas.Name = "txtColumnas"
        Me.txtColumnas.Size = New System.Drawing.Size(28, 20)
        Me.txtColumnas.TabIndex = 4
        Me.txtColumnas.Text = "3"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(173, 42)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(28, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "filas:"
        '
        'chkAjustarAuto
        '
        Me.chkAjustarAuto.AutoSize = True
        Me.chkAjustarAuto.Location = New System.Drawing.Point(241, 41)
        Me.chkAjustarAuto.Name = "chkAjustarAuto"
        Me.chkAjustarAuto.Size = New System.Drawing.Size(142, 17)
        Me.chkAjustarAuto.TabIndex = 7
        Me.chkAjustarAuto.Text = "Ajustar automáticamente"
        Me.toolTip1.SetToolTip(Me.chkAjustarAuto, "Ajustar la imagen automáticamente al cambiar de tamaño")
        Me.chkAjustarAuto.UseVisualStyleBackColor = True
        '
        'picMain
        '
        Me.picMain.Image = Global.trocearImagen_vb.My.Resources.Resources.Guille28sep06_1003_240x308
        Me.picMain.Location = New System.Drawing.Point(21, 74)
        Me.picMain.Margin = New System.Windows.Forms.Padding(12, 12, 12, 3)
        Me.picMain.Name = "picMain"
        Me.picMain.Size = New System.Drawing.Size(186, 235)
        Me.picMain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picMain.TabIndex = 9
        Me.picMain.TabStop = False
        '
        'progressBar1
        '
        Me.progressBar1.Location = New System.Drawing.Point(21, 315)
        Me.progressBar1.Name = "progressBar1"
        Me.progressBar1.Size = New System.Drawing.Size(186, 16)
        Me.progressBar1.TabIndex = 10
        '
        'timer1
        '
        Me.timer1.Interval = 300
        '
        'fTamañoDinamico
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(477, 342)
        Me.Controls.Add(Me.progressBar1)
        Me.Controls.Add(Me.chkAjustarAuto)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtColumnas)
        Me.Controls.Add(Me.txtFilas)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnTrocear)
        Me.Controls.Add(Me.gbImagenes)
        Me.Controls.Add(Me.picMain)
        Me.Controls.Add(Me.btnExaminar)
        Me.Controls.Add(Me.txtImagen)
        Me.Controls.Add(Me.Label1)
        Me.MinimumSize = New System.Drawing.Size(490, 374)
        Me.Name = "fTamañoDinamico"
        Me.Text = "Trocear una imagen con tamaños dinámicos (VB)"
        CType(Me.picMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents btnTrocear As System.Windows.Forms.Button
    Friend WithEvents gbImagenes As System.Windows.Forms.GroupBox
    Friend WithEvents picMain As System.Windows.Forms.PictureBox
    Friend WithEvents btnExaminar As System.Windows.Forms.Button
    Friend WithEvents txtImagen As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtFilas As System.Windows.Forms.TextBox
    Friend WithEvents txtColumnas As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents chkAjustarAuto As System.Windows.Forms.CheckBox
    Friend WithEvents toolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents progressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents timer1 As System.Windows.Forms.Timer
End Class
