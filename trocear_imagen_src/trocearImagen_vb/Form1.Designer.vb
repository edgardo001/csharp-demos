<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtImagen = New System.Windows.Forms.TextBox
        Me.btnExaminar = New System.Windows.Forms.Button
        Me.picMain = New System.Windows.Forms.PictureBox
        Me.gbImagenes = New System.Windows.Forms.GroupBox
        Me.pic8 = New System.Windows.Forms.PictureBox
        Me.pic7 = New System.Windows.Forms.PictureBox
        Me.pic6 = New System.Windows.Forms.PictureBox
        Me.pic5 = New System.Windows.Forms.PictureBox
        Me.pic4 = New System.Windows.Forms.PictureBox
        Me.pic3 = New System.Windows.Forms.PictureBox
        Me.pic2 = New System.Windows.Forms.PictureBox
        Me.pic1 = New System.Windows.Forms.PictureBox
        Me.pic0 = New System.Windows.Forms.PictureBox
        Me.btnTrocear = New System.Windows.Forms.Button
        CType(Me.picMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbImagenes.SuspendLayout()
        CType(Me.pic8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic0, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(90, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Imagen a trocear:"
        '
        'txtImagen
        '
        Me.txtImagen.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtImagen.Location = New System.Drawing.Point(108, 12)
        Me.txtImagen.Name = "txtImagen"
        Me.txtImagen.Size = New System.Drawing.Size(229, 20)
        Me.txtImagen.TabIndex = 1
        '
        'btnExaminar
        '
        Me.btnExaminar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnExaminar.Location = New System.Drawing.Point(343, 10)
        Me.btnExaminar.Name = "btnExaminar"
        Me.btnExaminar.Size = New System.Drawing.Size(75, 23)
        Me.btnExaminar.TabIndex = 2
        Me.btnExaminar.Text = "Examinar..."
        Me.btnExaminar.UseVisualStyleBackColor = True
        '
        'picMain
        '
        Me.picMain.Image = Global.trocearImagen_vb.My.Resources.Resources.Guille28sep06_1003_240x308
        Me.picMain.Location = New System.Drawing.Point(21, 47)
        Me.picMain.Margin = New System.Windows.Forms.Padding(12)
        Me.picMain.Name = "picMain"
        Me.picMain.Size = New System.Drawing.Size(186, 237)
        Me.picMain.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picMain.TabIndex = 3
        Me.picMain.TabStop = False
        '
        'gbImagenes
        '
        Me.gbImagenes.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.gbImagenes.Controls.Add(Me.pic8)
        Me.gbImagenes.Controls.Add(Me.pic7)
        Me.gbImagenes.Controls.Add(Me.pic6)
        Me.gbImagenes.Controls.Add(Me.pic5)
        Me.gbImagenes.Controls.Add(Me.pic4)
        Me.gbImagenes.Controls.Add(Me.pic3)
        Me.gbImagenes.Controls.Add(Me.pic2)
        Me.gbImagenes.Controls.Add(Me.pic1)
        Me.gbImagenes.Controls.Add(Me.pic0)
        Me.gbImagenes.Location = New System.Drawing.Point(241, 47)
        Me.gbImagenes.Margin = New System.Windows.Forms.Padding(12)
        Me.gbImagenes.Name = "gbImagenes"
        Me.gbImagenes.Size = New System.Drawing.Size(172, 186)
        Me.gbImagenes.TabIndex = 4
        Me.gbImagenes.TabStop = False
        Me.gbImagenes.Text = "Imagen troceada:"
        '
        'pic8
        '
        Me.pic8.Location = New System.Drawing.Point(114, 127)
        Me.pic8.Margin = New System.Windows.Forms.Padding(1)
        Me.pic8.Name = "pic8"
        Me.pic8.Size = New System.Drawing.Size(52, 52)
        Me.pic8.TabIndex = 8
        Me.pic8.TabStop = False
        '
        'pic7
        '
        Me.pic7.Location = New System.Drawing.Point(60, 127)
        Me.pic7.Margin = New System.Windows.Forms.Padding(1)
        Me.pic7.Name = "pic7"
        Me.pic7.Size = New System.Drawing.Size(52, 52)
        Me.pic7.TabIndex = 7
        Me.pic7.TabStop = False
        '
        'pic6
        '
        Me.pic6.Location = New System.Drawing.Point(6, 127)
        Me.pic6.Margin = New System.Windows.Forms.Padding(1)
        Me.pic6.Name = "pic6"
        Me.pic6.Size = New System.Drawing.Size(52, 52)
        Me.pic6.TabIndex = 6
        Me.pic6.TabStop = False
        '
        'pic5
        '
        Me.pic5.Location = New System.Drawing.Point(114, 73)
        Me.pic5.Margin = New System.Windows.Forms.Padding(1)
        Me.pic5.Name = "pic5"
        Me.pic5.Size = New System.Drawing.Size(52, 52)
        Me.pic5.TabIndex = 5
        Me.pic5.TabStop = False
        '
        'pic4
        '
        Me.pic4.Location = New System.Drawing.Point(60, 73)
        Me.pic4.Margin = New System.Windows.Forms.Padding(1)
        Me.pic4.Name = "pic4"
        Me.pic4.Size = New System.Drawing.Size(52, 52)
        Me.pic4.TabIndex = 4
        Me.pic4.TabStop = False
        '
        'pic3
        '
        Me.pic3.Location = New System.Drawing.Point(6, 73)
        Me.pic3.Margin = New System.Windows.Forms.Padding(1)
        Me.pic3.Name = "pic3"
        Me.pic3.Size = New System.Drawing.Size(52, 52)
        Me.pic3.TabIndex = 3
        Me.pic3.TabStop = False
        '
        'pic2
        '
        Me.pic2.Location = New System.Drawing.Point(114, 19)
        Me.pic2.Margin = New System.Windows.Forms.Padding(1)
        Me.pic2.Name = "pic2"
        Me.pic2.Size = New System.Drawing.Size(52, 52)
        Me.pic2.TabIndex = 2
        Me.pic2.TabStop = False
        '
        'pic1
        '
        Me.pic1.Location = New System.Drawing.Point(60, 19)
        Me.pic1.Margin = New System.Windows.Forms.Padding(1)
        Me.pic1.Name = "pic1"
        Me.pic1.Size = New System.Drawing.Size(52, 52)
        Me.pic1.TabIndex = 1
        Me.pic1.TabStop = False
        '
        'pic0
        '
        Me.pic0.Location = New System.Drawing.Point(6, 19)
        Me.pic0.Margin = New System.Windows.Forms.Padding(1)
        Me.pic0.Name = "pic0"
        Me.pic0.Size = New System.Drawing.Size(52, 52)
        Me.pic0.TabIndex = 0
        Me.pic0.TabStop = False
        '
        'btnTrocear
        '
        Me.btnTrocear.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnTrocear.Location = New System.Drawing.Point(343, 261)
        Me.btnTrocear.Name = "btnTrocear"
        Me.btnTrocear.Size = New System.Drawing.Size(75, 23)
        Me.btnTrocear.TabIndex = 5
        Me.btnTrocear.Text = "Trocear"
        Me.btnTrocear.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(430, 296)
        Me.Controls.Add(Me.btnTrocear)
        Me.Controls.Add(Me.gbImagenes)
        Me.Controls.Add(Me.picMain)
        Me.Controls.Add(Me.btnExaminar)
        Me.Controls.Add(Me.txtImagen)
        Me.Controls.Add(Me.Label1)
        Me.MinimumSize = New System.Drawing.Size(440, 330)
        Me.Name = "Form1"
        Me.Text = "Trocear una imagen (VB)"
        CType(Me.picMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbImagenes.ResumeLayout(False)
        CType(Me.pic8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic0, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtImagen As System.Windows.Forms.TextBox
    Friend WithEvents btnExaminar As System.Windows.Forms.Button
    Friend WithEvents picMain As System.Windows.Forms.PictureBox
    Friend WithEvents gbImagenes As System.Windows.Forms.GroupBox
    Friend WithEvents pic8 As System.Windows.Forms.PictureBox
    Friend WithEvents pic7 As System.Windows.Forms.PictureBox
    Friend WithEvents pic6 As System.Windows.Forms.PictureBox
    Friend WithEvents pic5 As System.Windows.Forms.PictureBox
    Friend WithEvents pic4 As System.Windows.Forms.PictureBox
    Friend WithEvents pic3 As System.Windows.Forms.PictureBox
    Friend WithEvents pic2 As System.Windows.Forms.PictureBox
    Friend WithEvents pic1 As System.Windows.Forms.PictureBox
    Friend WithEvents pic0 As System.Windows.Forms.PictureBox
    Private WithEvents btnTrocear As System.Windows.Forms.Button

End Class
