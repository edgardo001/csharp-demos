﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("trocearImagen_vb")> 
<Assembly: AssemblyDescription("Trocear una imagen")> 
<Assembly: AssemblyCompany("elGuiller -Nerja")> 
<Assembly: AssemblyProduct("trocearImagen_vb, revisión del 18/Ene/2007")> 
<Assembly: AssemblyCopyright("©Guillermo 'guille' Som,  2007")> 
<Assembly: AssemblyTrademark("Microsoft Visual Basic 2005 (.NET 2.0)")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("f965b05a-e8eb-41be-9863-b0b56205510e")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.2")> 
