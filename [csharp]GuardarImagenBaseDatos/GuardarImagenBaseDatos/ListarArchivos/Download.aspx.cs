﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO; 

namespace GuardarImagenBaseDatos.ListarArchivos
{
    public partial class Download : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string filename = Request.QueryString["filename"].ToString(); 

            Response.Clear();
            Response.AddHeader("content-disposition", string.Format("attachment;filename={0}", filename));

            switch (Path.GetExtension(filename).ToLower())
            {
                case ".jpg":
                    Response.ContentType = "image/jpg";
                    break;
                case ".gif":
                    Response.ContentType = "image/gif";
                    break;
                case ".png":
                    Response.ContentType = "image/png";
                    break;
            }

            

            Response.WriteFile(Server.MapPath(Path.Combine("~/files", filename)));

            Response.End();

        }
    }
}
