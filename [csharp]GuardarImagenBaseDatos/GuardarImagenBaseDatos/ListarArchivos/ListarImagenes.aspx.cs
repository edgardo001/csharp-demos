﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace GuardarImagenBaseDatos.ListarArchivos
{
    public partial class ListarImagenes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DirectoryInfo dirInfo = new DirectoryInfo(Server.MapPath("~/files"));
                FileInfo[] fileInfo = dirInfo.GetFiles("*.*", SearchOption.AllDirectories);

                GridView1.DataSource = fileInfo;
                GridView1.DataBind();
            }
        }

        protected void btnSubirArchivo_Click(object sender, EventArgs e)
        {
            if (FileUpload1.HasFile)
            {
                string fullPath = Path.Combine(Server.MapPath("~/files"), FileUpload1.FileName);
                FileUpload1.SaveAs(fullPath);
            }
        }

    }
}
