﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace GuardarImagenBaseDatos.GuardarImagen
{
    public partial class SubirImagen : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                CargarListadImagenes(); 
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {

            if (FileUpload1.HasFile)
            {
                using (BinaryReader reader = new BinaryReader(FileUpload1.PostedFile.InputStream))
                {
                    byte[] image = reader.ReadBytes(FileUpload1.PostedFile.ContentLength);

                    ImagenesDAL.GuardarImagen(FileUpload1.FileName, FileUpload1.PostedFile.ContentLength , image);
                    
                }

                CargarListadImagenes();
            }
            

        }

        private void CargarListadImagenes()
        {
            GridView1.DataSource = ImagenesDAL.GetImagenList();
            GridView1.DataBind(); 
        }

    }
}
