﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GuardarImagenBaseDatos.GuardarImagen
{
    public partial class ListarImagenes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GridView1.DataSource = ImagenesDAL.GetImagenList();
                GridView1.DataBind();
            }

        }
    }
}
