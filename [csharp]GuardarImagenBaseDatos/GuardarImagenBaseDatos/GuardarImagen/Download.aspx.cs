﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;
using GuardarImagenBaseDatos;

namespace GuardarImagenBaseDatos.GuardarImagen
{
    public partial class Download : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(Request.QueryString["id"]);

            Imagenes imagen = ImagenesDAL.GetImagenById(id);

            Response.Clear();
            Response.AddHeader("content-disposition", string.Format("attachment;filename={0}", imagen.Nombre));

            switch (Path.GetExtension(imagen.Nombre).ToLower())
            {
                case ".jpg":
                    Response.ContentType = "image/jpg";
                    break;
                case ".gif":
                    Response.ContentType = "image/gif";
                    break;
                case ".png":
                    Response.ContentType = "image/png";
                    break;
            }

            Response.BinaryWrite(imagen.Imagen);
            Response.End();

        }
    }
}
