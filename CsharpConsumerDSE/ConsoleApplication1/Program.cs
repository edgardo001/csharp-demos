﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CsharpConsumerDSE.WebReference;
using System.IO;
namespace CsharpConsumerDSE
{
    class Program
    {
        static void Main(string[] args)
        {
            metodoFirmaPdf();
        }

        private static void metodoFirmaPdf()
        {
            try
            {
                Console.WriteLine("conectandose a:");
                byte[] pdfByte = File.ReadAllBytes(@"c:/a.pdf");
                DatasignerEnterprese webService = new DatasignerEnterprese();
                webService.Url = "http://datasoft.cl:47000/DataSignerEnterprese/services/DatasignerEnterprese.DatasignerEnterpreseHttpSoap11Endpoint/";
                //webService.Url = "http://192.168.15.27:47000/DataSignerEnterprese/services/DatasignerEnterprese.DatasignerEnterpreseHttpSoap11Endpoint/";
                Console.WriteLine(webService.Url);
                //Console.WriteLine(webService.getEncriptedPass("Passw0rd"));
                File.WriteAllBytes(@"C:\pruebaNet.pdf", webService.FirmaPDF("testing", pdfByte, "?", "?", "?", "?", false, true));
                Console.WriteLine("Archivo almacenado en la ruta");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex);
            }
            finally
            {
                Console.ReadLine();
            }            
        }
    }
}
