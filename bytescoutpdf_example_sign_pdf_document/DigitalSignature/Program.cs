using System;
using System.Collections.Generic;
using System.Text;
using Bytescout.PDF;

namespace DigitalSignature
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create main PDF Doc Engine
            PDFDocEngine engine = new PDFDocEngine("demo", "demo");

            // Open existing document
            Document document = engine.AddDocument("HelloWorld.pdf");

            document.AddSignature("dig-signature.pfx", "John Doe", "Approved", false, "123456");

            // Save document
            document.Save("signed.pdf");
        }
    }
}
