﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using log4net;

namespace TestProyectLog4Net
{
    public partial class frmTest : Form
    {

        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public frmTest()
        {
            InitializeComponent();
        }

        private void frmTest_Load(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnDebug_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.chkError.Checked)
                    try 
	                {
                        int iCero = 0;
                        int iNum = 1 / iCero;
	                }
	                catch (Exception ex)
	                {
                        //Modo Nativo de LOG4NET
                        //log.Debug(this.txtMessage.Text, ex);
                        //Modo Personalizado
                        log.Debug(string.Format("Objecto : {0} {1} Mensaje : {2} {3} Fuente  : {4}",
                                                    ex.Source.ToString(),
                                                    Environment.NewLine,
                                                    ex.Message.ToString(),
                                                    Environment.NewLine,
                                                    ex.StackTrace.ToString()));
	                }
                    
                else
                    log.Debug(this.txtMessage.Text);
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnInfo_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.chkError.Checked)
                    try
                    {
                        int iCero = 0;
                        int iNum = 1 / iCero;
                    }
                    catch (Exception ex)
                    {
                        //Modo Nativo de LOG4NET
                        //log.Info(this.txtMessage.Text, ex);
                        //Modo Personalizado
                        log.Info(string.Format("Objecto : {0} {1} Mensaje : {2} {3} Fuente  : {4}",
                                                    ex.Source.ToString(),
                                                    Environment.NewLine,
                                                    ex.Message.ToString(),
                                                    Environment.NewLine,
                                                    ex.StackTrace.ToString()));
                    }

                else
                    log.Info(this.txtMessage.Text);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnWarn_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.chkError.Checked)
                    try
                    {
                        int iCero = 0;
                        int iNum = 1 / iCero;
                    }
                    catch (Exception ex)
                    {
                        //Modo Nativo de LOG4NET
                        //log.Warn(this.txtMessage.Text, ex);
                        //Modo Personalizado
                        log.Warn(string.Format("Objecto : {0} {1} Mensaje : {2} {3} Fuente  : {4}",
                                                    ex.Source.ToString(),
                                                    Environment.NewLine,
                                                    ex.Message.ToString(),
                                                    Environment.NewLine,
                                                    ex.StackTrace.ToString()));
                    }

                else
                    log.Warn(this.txtMessage.Text);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BtnError_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.chkError.Checked)
                    try
                    {
                        int iCero = 0;
                        int iNum = 1 / iCero;
                    }
                    catch (Exception ex)
                    {
                        //Modo Nativo de LOG4NET
                        //log.Error(this.txtMessage.Text, ex);
                        //Modo Personalizado
                        log.Error(string.Format("Objecto : {0} {1} Mensaje : {2} {3} Fuente  : {4}",
                                                    ex.Source.ToString(),
                                                    Environment.NewLine,
                                                    ex.Message.ToString(),
                                                    Environment.NewLine,
                                                    ex.StackTrace.ToString()));
                    }

                else
                    log.Error(this.txtMessage.Text);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void btnFatal_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.chkError.Checked)
                    try
                    {
                        int iCero = 0;
                        int iNum = 1 / iCero;
                    }
                    catch (Exception ex)
                    {
                        //Modo Nativo de LOG4NET
                        //log.Fatal(this.txtMessage.Text, ex);
                        //Modo Personalizado
                        log.Fatal(string.Format("Objecto : {0} {1} Mensaje : {2} {3} Fuente  : {4}",
                                                    ex.Source.ToString(),
                                                    Environment.NewLine,
                                                    ex.Message.ToString(),
                                                    Environment.NewLine,
                                                    ex.StackTrace.ToString()));
                    }

                else
                    log.Fatal(this.txtMessage.Text);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
