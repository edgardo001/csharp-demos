﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testConnMySql
{
    class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();
            //p.Select();
            //p.insert();
            //p.insertParameters();
            //p.delete();
            //p.update();
            //p.prodStoreSelectAll();
            //p.prodStoreSelectbyId();
            //p.prodStoreInsertEmpresa();

        }

        private void prodStoreInsertEmpresa()
        {
            try
            {
                MySqlConnection _SqlConecction = connMySql();
                _SqlConecction.Open();
                string stmtprodStoreSelectAll = "call prod_InsertEmpresa(?nombreEmp,?rutEmp,?telEmp,?emailEmp, ?dirEmp, ?descEmp);";
                MySqlCommand comando = new MySqlCommand(stmtprodStoreSelectAll, _SqlConecction);
                comando.Parameters.AddWithValue("?nombreEmp", "daniel ltda");
                comando.Parameters.AddWithValue("?rutEmp", "2-8");
                comando.Parameters.AddWithValue("?telEmp", "99999999");
                comando.Parameters.AddWithValue("?emailEmp", "dsilva@daniel.cl");
                comando.Parameters.AddWithValue("?dirEmp", "otro lugar");
                comando.Parameters.AddWithValue("?descEmp", "no tiene descripcion");

                MySqlDataReader reader = comando.ExecuteReader();
                while (reader.Read())
                {
                    Console.WriteLine(reader["result"].ToString());
                }

                Console.WriteLine("OK - prodStoreInsertEmpresa");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadLine();
            }
        }

        private void prodStoreSelectbyId()
        {
            try
            {
                MySqlConnection _SqlConecction = connMySql();
                _SqlConecction.Open();
                string stmtprodStoreSelectAll = "prod_SelectById(?id)";
                MySqlCommand comando = new MySqlCommand(stmtprodStoreSelectAll, _SqlConecction);
                comando.Parameters.AddWithValue("?id", 6);
                MySqlDataReader reader = comando.ExecuteReader();
                while (reader.Read())
                {
                    Console.WriteLine(reader["nombre_emp"].ToString());
                    Console.WriteLine(reader["id_emp"].ToString());
                    Console.WriteLine(reader["rut_emp"].ToString());
                    Console.WriteLine(reader["telefono_emp"].ToString());
                    Console.WriteLine(reader["email_emp"].ToString());
                    Console.WriteLine("-----------------------------");
                }

                Console.WriteLine("OK - prodStoreSelectAll");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadLine();
            }
        }

        private void prodStoreSelectAll()
        {
            try
            {
                MySqlConnection _SqlConecction = connMySql();
                _SqlConecction.Open();
                string stmtprodStoreSelectAll = "prod_SelectAll";
                MySqlCommand comando = new MySqlCommand(stmtprodStoreSelectAll, _SqlConecction);
                MySqlDataReader reader = comando.ExecuteReader();

                while (reader.Read())
                {
                    Console.WriteLine(reader["nombre_emp"].ToString());
                    Console.WriteLine(reader["id_emp"].ToString());
                    Console.WriteLine(reader["rut_emp"].ToString());
                    Console.WriteLine(reader["telefono_emp"].ToString());
                    Console.WriteLine(reader["email_emp"].ToString());
                    Console.WriteLine("-----------------------------");
                }

                Console.WriteLine("OK - prodStoreSelectAll");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadLine();
            }
        }

        private void delete()
        {
            try
            {
                MySqlConnection _SqlConecction = connMySql();
                _SqlConecction.Open();
                string stmtDelete = "DELETE FROM `websigner_qa`.`empresa` WHERE `ID_EMP`=?ID_EMP;";
                MySqlCommand comando = new MySqlCommand(stmtDelete, _SqlConecction);

                comando.Parameters.AddWithValue("?ID_EMP", 4);

                comando.ExecuteNonQuery();
                Console.WriteLine("OK - Delete realizado con exito");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadLine();
            }
        }

        private void update()
        {
            try
            {
                MySqlConnection _SqlConecction = connMySql();
                _SqlConecction.Open();
                string stmtDelete = "UPDATE `websigner_qa`.`empresa` SET `NOMBRE_EMP`=?NOMBRE_EMP, `FECHAMODIFICACION_EMP`=?FECHAMODIFICACION_EMP WHERE `ID_EMP`=?ID_EMP;";
                MySqlCommand comando = new MySqlCommand(stmtDelete, _SqlConecction);

                comando.Parameters.AddWithValue("?NOMBRE_EMP", "EDGARDO S.A.");
                comando.Parameters.AddWithValue("?FECHAMODIFICACION_EMP", DateTime.Now);
                comando.Parameters.AddWithValue("?ID_EMP", 2);

                comando.ExecuteNonQuery();
                Console.WriteLine("OK - Update realizado con exito");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadLine();
            }
        }

        private void insert()
        {
            try
            {
                MySqlConnection _SqlConecction = connMySql();
                _SqlConecction.Open();
                string stmtInsert = "INSERT INTO `websigner_qa`.`empresa` (`NOMBRE_EMP`, `RUT_EMP`, `TELEFONO_EMP`, `EMAIL_EMP`, `DIRECCION_EMP`, `DESCRIPCION_EMP`, `FECHACREACION_EMP`)"
                    + "VALUES ('xxx', '1222', '3123', '312', '3123', '213123', '2016-02-01');";
                MySqlCommand comando = new MySqlCommand(stmtInsert, _SqlConecction);
                comando.ExecuteNonQuery();
                Console.WriteLine("OK - Insert realizado con exito");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadLine();
            }
        }

        private void insertParameters()
        {
            try
            {
                MySqlConnection _SqlConecction = connMySql();
                _SqlConecction.Open();
                string stmtInsert = "INSERT INTO `websigner_qa`.`empresa` (`NOMBRE_EMP`, `RUT_EMP`, `TELEFONO_EMP`, `EMAIL_EMP`, `DIRECCION_EMP`, `DESCRIPCION_EMP`, `FECHACREACION_EMP`)"
                    + "VALUES (?NOMBRE_EMP, ?RUT_EMP, ?TELEFONO_EMP, ?EMAIL_EMP, ?DIRECCION_EMP, ?DESCRIPCION_EMP, ?FECHACREACION_EMP);";
                MySqlCommand comando = new MySqlCommand(stmtInsert, _SqlConecction);

                comando.Parameters.AddWithValue("?NOMBRE_EMP", "yyy");
                comando.Parameters.AddWithValue("?RUT_EMP", "yyy");
                comando.Parameters.AddWithValue("?TELEFONO_EMP", "2342234234");
                comando.Parameters.AddWithValue("?EMAIL_EMP", "yyy@yyy.cl");
                comando.Parameters.AddWithValue("?DIRECCION_EMP", "yyy #12334");
                comando.Parameters.AddWithValue("?DESCRIPCION_EMP", "yyyyy yyyyyy-yyyyy");
                comando.Parameters.AddWithValue("?FECHACREACION_EMP", DateTime.Now);

                comando.ExecuteNonQuery();
                Console.WriteLine("OK - Insercion con parametros realizada con exito");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadLine();
            }
        }

        public void Select()
        {
            try
            {
                MySqlConnection _SqlConecction = connMySql();
                _SqlConecction.Open();
                string stmtSelect = "select * from empresa";
                MySqlCommand comando = new MySqlCommand(stmtSelect, _SqlConecction);
                MySqlDataReader reader = comando.ExecuteReader();

                while (reader.Read())
                {
                    Console.WriteLine(reader["nombre_emp"].ToString());
                    Console.WriteLine(reader["id_emp"].ToString());
                    Console.WriteLine(reader["rut_emp"].ToString());
                    Console.WriteLine(reader["telefono_emp"].ToString());
                    Console.WriteLine(reader["email_emp"].ToString());
                    Console.WriteLine("-----------------------------");
                }

                Console.WriteLine("OK - Obj Seleccionados");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Metodo que abre la conexion con la base de datos SQL server
        /// </summary>
        /// <returns></returns>
        private MySqlConnection connMySql()
        {
            try
            {
                string conn = "Server=192.168.15.27; Port=3306; Uid=websigner_qa;Pwd=Passw0rd;Database=websigner_qa;CharSet=utf8";
                MySqlConnection connection = new MySqlConnection(conn);
                return connection;
            }
            catch (Exception ex)
            {
                throw new Exception("ERROR - 'connSQLServer' " + ex.Message);
            }
        }


    }
}
