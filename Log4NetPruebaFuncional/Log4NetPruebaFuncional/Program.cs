﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//1.-Agregar libreria Log4net con la version del Framework.net usado en el proyecto,
//en mi caso, lo descarge de Apache "http://www-eu.apache.org/dist//logging/log4net/binaries/log4net-1.2.15-bin-newkey.zip"
//una vez descargado, ir a la carpeta "\log4net-1.2.15-bin-newkey\bin\net\4.5\release" y seleccionar la dll referenciandola al proyecto
using log4net;
//2.-Agregar "[assembly: log4net.Config.XmlConfigurator(ConfigFile = "Configuracion.log4net", Watch = true)]"
//esto en el proyecto/Properties/AssemblyInfo.cs y pegarla al final del codigo

//3.-Crear un archivo llamado "Configuracion.log4net" en el proyecto, ir a sus propiedades y modificar
//"Copiar en el directorio de salida" cambiar a: "copiar si es posterior"
//con esto lo copiara cada vez que se compile el codigo.

//4.-Copiar el sigiente contenido (descomentado) dentro del fichero "Configuracion.log4net"

//<? xml version="1.0" encoding="utf-8" ?>
//<log4net>
//  <appender name = "LogHeaderServicio" type="log4net.Appender.RollingFileAppender">
//    <param name = "file" value="LogExtraService\logExtraService_" />
//    <param name = "appendToFile" value="true" />
//    <param name = "maximumFileSize" value="10MB" />
//    <param name = "maxSizeRollBackups" value="100" />
//    <param name = "RollingStyle" value="Composite" />
//    <param name = "StaticLogFileName" value="false" />
//    <param name = "DatePattern" value="yyyy-MM-dd" />
//    <layout type = "log4net.Layout.PatternLayout" >
//      < !--< conversionPattern value="%newline%date [%thread]%newline %message %newline "/>-->
//      <conversionPattern value = "%newline%date [%thread] %-5level %newline %message %newline " />
//    </ layout >
//  </ appender >

//  < root >
//    < level value="ALL" />
//    <appender-ref ref="LogHeaderServicio" />
//  </root>
//</log4net>

//Nota sobre el contenido anterior: si se descomenta se copia y se pega, puede salir un error relacionado con "El nombre no puede contener ' "
namespace Log4NetPruebaFuncional
{
   class Program
    {
        //5.-Agregar la siguiente linea para obtener una instancia
        private readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static void Main(string[] args)
        {
            //6.-Ejemplo de uso
            Program p = new Program();
            p.log.Info("hola");
            p.log.Warn("hola");
            p.log.Error("hola");

            Console.ReadLine();
        }
    }
}
