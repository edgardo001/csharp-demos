﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ejemploMultiThread
{
    public class Tareas
    {
        private String name;
        private int second;
        private String dato;

        public Tareas(String name, int second)
        {
            this.name = name;
            this.second = second;
        }
        public Tareas(String name, string dato)
        {
            this.name = name;
            this.dato = dato;
        }
        public void tarea1()
        {
            Console.WriteLine(name + " Esta tarea solo tendra " + second + " second.");
            //Thread.Sleep Suspende el subproceso actual durante un periodo de tiempo especificado.
            Thread.Sleep(second * 1000);
            Console.WriteLine("Tarea terminada: " + name);
        }

        public void tarea2()
        {
            for (int i = 0; i < 200000; i++)
            {
                Console.Write(dato);
            }
        }
    }
}
