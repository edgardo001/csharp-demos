﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ejemploMultiThread
{
    class Program
    {
        static void Main(string[] args)
        {
            //Metodo1();
            //Metodo2();
            Metodo3();

            Console.ReadLine();
        }

        /// <summary>
        /// Ejecucion de hilos con tiempo predeterminado
        /// </summary>
        private static void Metodo3()
        {
            List<Tareas> tareas = new List<Tareas>();
            tareas.Add(new Tareas("Tarea a", "a"));
            tareas.Add(new Tareas("Tarea b", "b"));
            tareas.Add(new Tareas("Tarea c", "c"));
            tareas.Add(new Tareas("Tarea d", "d"));
            tareas.Add(new Tareas("Tarea e", "e"));
            tareas.Add(new Tareas("Tarea f", "f"));
            tareas.Add(new Tareas("Tarea g", "g"));
            tareas.Add(new Tareas("Tarea h", "h"));
            tareas.Add(new Tareas("Tarea i", "i"));
            tareas.Add(new Tareas("Tarea j", "j"));
            tareas.Add(new Tareas("Tarea k", "k"));
            tareas.Add(new Tareas("Tarea l", "l"));
            tareas.Add(new Tareas("Tarea m", "m"));
            tareas.Add(new Tareas("Tarea n", "n"));
            tareas.Add(new Tareas("Tarea o", "o"));
            tareas.Add(new Tareas("Tarea p", "p"));
            tareas.Add(new Tareas("Tarea q", "q"));
            tareas.Add(new Tareas("Tarea r", "r"));
            tareas.Add(new Tareas("Tarea s", "s"));
            tareas.Add(new Tareas("Tarea t", "t"));
            tareas.Add(new Tareas("Tarea u", "u"));
            tareas.Add(new Tareas("Tarea v", "v"));
            tareas.Add(new Tareas("Tarea w", "w"));
            tareas.Add(new Tareas("Tarea x", "x"));
            tareas.Add(new Tareas("Tarea y", "y"));
            tareas.Add(new Tareas("Tarea z", "z"));

            tareas.Add(new Tareas("Tarea a", "a"));
            tareas.Add(new Tareas("Tarea b", "b"));
            tareas.Add(new Tareas("Tarea c", "c"));
            tareas.Add(new Tareas("Tarea d", "d"));
            tareas.Add(new Tareas("Tarea e", "e"));
            tareas.Add(new Tareas("Tarea f", "f"));
            tareas.Add(new Tareas("Tarea g", "g"));
            tareas.Add(new Tareas("Tarea h", "h"));
            tareas.Add(new Tareas("Tarea i", "i"));
            tareas.Add(new Tareas("Tarea j", "j"));
            tareas.Add(new Tareas("Tarea k", "k"));
            tareas.Add(new Tareas("Tarea l", "l"));
            tareas.Add(new Tareas("Tarea m", "m"));
            tareas.Add(new Tareas("Tarea n", "n"));
            tareas.Add(new Tareas("Tarea o", "o"));
            tareas.Add(new Tareas("Tarea p", "p"));
            tareas.Add(new Tareas("Tarea q", "q"));
            tareas.Add(new Tareas("Tarea r", "r"));
            tareas.Add(new Tareas("Tarea s", "s"));
            tareas.Add(new Tareas("Tarea t", "t"));
            tareas.Add(new Tareas("Tarea u", "u"));
            tareas.Add(new Tareas("Tarea v", "v"));
            tareas.Add(new Tareas("Tarea w", "w"));
            tareas.Add(new Tareas("Tarea x", "x"));
            tareas.Add(new Tareas("Tarea y", "y"));
            tareas.Add(new Tareas("Tarea z", "z"));

            tareas.Add(new Tareas("Tarea a", "a"));
            tareas.Add(new Tareas("Tarea b", "b"));
            tareas.Add(new Tareas("Tarea c", "c"));
            tareas.Add(new Tareas("Tarea d", "d"));
            tareas.Add(new Tareas("Tarea e", "e"));
            tareas.Add(new Tareas("Tarea f", "f"));
            tareas.Add(new Tareas("Tarea g", "g"));
            tareas.Add(new Tareas("Tarea h", "h"));
            tareas.Add(new Tareas("Tarea i", "i"));
            tareas.Add(new Tareas("Tarea j", "j"));
            tareas.Add(new Tareas("Tarea k", "k"));
            tareas.Add(new Tareas("Tarea l", "l"));
            tareas.Add(new Tareas("Tarea m", "m"));
            tareas.Add(new Tareas("Tarea n", "n"));
            tareas.Add(new Tareas("Tarea o", "o"));
            tareas.Add(new Tareas("Tarea p", "p"));
            tareas.Add(new Tareas("Tarea q", "q"));
            tareas.Add(new Tareas("Tarea r", "r"));
            tareas.Add(new Tareas("Tarea s", "s"));
            tareas.Add(new Tareas("Tarea t", "t"));
            tareas.Add(new Tareas("Tarea u", "u"));
            tareas.Add(new Tareas("Tarea v", "v"));
            tareas.Add(new Tareas("Tarea w", "w"));
            tareas.Add(new Tareas("Tarea x", "x"));
            tareas.Add(new Tareas("Tarea y", "y"));
            tareas.Add(new Tareas("Tarea z", "z"));

            tareas.Add(new Tareas("Tarea a", "a"));
            tareas.Add(new Tareas("Tarea b", "b"));
            tareas.Add(new Tareas("Tarea c", "c"));
            tareas.Add(new Tareas("Tarea d", "d"));
            tareas.Add(new Tareas("Tarea e", "e"));
            tareas.Add(new Tareas("Tarea f", "f"));
            tareas.Add(new Tareas("Tarea g", "g"));
            tareas.Add(new Tareas("Tarea h", "h"));
            tareas.Add(new Tareas("Tarea i", "i"));
            tareas.Add(new Tareas("Tarea j", "j"));
            tareas.Add(new Tareas("Tarea k", "k"));
            tareas.Add(new Tareas("Tarea l", "l"));
            tareas.Add(new Tareas("Tarea m", "m"));
            tareas.Add(new Tareas("Tarea n", "n"));
            tareas.Add(new Tareas("Tarea o", "o"));
            tareas.Add(new Tareas("Tarea p", "p"));
            tareas.Add(new Tareas("Tarea q", "q"));
            tareas.Add(new Tareas("Tarea r", "r"));
            tareas.Add(new Tareas("Tarea s", "s"));
            tareas.Add(new Tareas("Tarea t", "t"));
            tareas.Add(new Tareas("Tarea u", "u"));
            tareas.Add(new Tareas("Tarea v", "v"));
            tareas.Add(new Tareas("Tarea w", "w"));
            tareas.Add(new Tareas("Tarea x", "x"));
            tareas.Add(new Tareas("Tarea y", "y"));
            tareas.Add(new Tareas("Tarea z", "z"));

            tareas.Add(new Tareas("Tarea a", "a"));
            tareas.Add(new Tareas("Tarea b", "b"));
            tareas.Add(new Tareas("Tarea c", "c"));
            tareas.Add(new Tareas("Tarea d", "d"));
            tareas.Add(new Tareas("Tarea e", "e"));
            tareas.Add(new Tareas("Tarea f", "f"));
            tareas.Add(new Tareas("Tarea g", "g"));
            tareas.Add(new Tareas("Tarea h", "h"));
            tareas.Add(new Tareas("Tarea i", "i"));
            tareas.Add(new Tareas("Tarea j", "j"));
            tareas.Add(new Tareas("Tarea k", "k"));
            tareas.Add(new Tareas("Tarea l", "l"));
            tareas.Add(new Tareas("Tarea m", "m"));
            tareas.Add(new Tareas("Tarea n", "n"));
            tareas.Add(new Tareas("Tarea o", "o"));
            tareas.Add(new Tareas("Tarea p", "p"));
            tareas.Add(new Tareas("Tarea q", "q"));
            tareas.Add(new Tareas("Tarea r", "r"));
            tareas.Add(new Tareas("Tarea s", "s"));
            tareas.Add(new Tareas("Tarea t", "t"));
            tareas.Add(new Tareas("Tarea u", "u"));
            tareas.Add(new Tareas("Tarea v", "v"));
            tareas.Add(new Tareas("Tarea w", "w"));
            tareas.Add(new Tareas("Tarea x", "x"));
            tareas.Add(new Tareas("Tarea y", "y"));
            tareas.Add(new Tareas("Tarea z", "z"));

            foreach (Tareas item in tareas)
            {
                Thread hilo1 = new Thread(item.tarea2);
                hilo1.Start();
            }
        }
        /// <summary>
        /// Ejecucion de hilos con tiempo predeterminado
        /// </summary>
        private static void Metodo2()
        {
            ArrayList listTareas = new ArrayList();
            listTareas.Add("Hilo_1");
            listTareas.Add("Hilo_2");
            listTareas.Add("Hilo_3");
            ArrayList listSecond = new ArrayList();
            listSecond.Add(8);
            listSecond.Add(16);
            listSecond.Add(24);
            for (int i = 0; i < listTareas.Count; i++)
            {
                object tarea = listTareas[i];
                string tareas = Convert.ToString(tarea);
                object secs = listSecond[i];
                int sec = Convert.ToInt32(secs);

                Tareas Obj1 = new Tareas(tareas, sec);

                //OPCION-1
                //Esta linea permite que los procesos se ejecuten de forma secuencial
                //Obj1.tarea1();

                //OPCION-2
                //Estas lineas permiten que procesos se llamen todos a la vez
                Thread hilo1 = new Thread(Obj1.tarea1);
                hilo1.Start();
            }
        }
        /// <summary>
        /// Ejecucion de hilos con tiempo predeterminado
        /// </summary>
        private static void Metodo1()
        {
            Tareas obj1 = new Tareas("Hilo_1", 8);
            Tareas obj2 = new Tareas("Hilo_2", 16);
            Thread hilo1 = new Thread(obj1.tarea1);
            Thread hilo2 = new Thread(obj2.tarea1);
            hilo1.Start();
            hilo2.Start();
        }
    }
}
