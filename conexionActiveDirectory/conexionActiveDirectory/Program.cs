﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace conexionActiveDirectory
{
    class Program
    {
        private static string ldap = "LDAP://192.168.15.196/cn=Users,dc=datasoft,dc=cl";
        private static string userAD = "Administrador";
        private static string passAD = "Passw0rd";


        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("***Iniciando conexion***");
                Console.WriteLine("***AD: " + ldap);
                Console.WriteLine("");

                Usuarios u = new Usuarios();
                u.Email = "";
                u.Names = "user1";
                u.Description = "";
                u.LastName = "";
                u.Login = "";

                Console.WriteLine("***Obteniendo lista***");
                List<Usuarios> list = FindName(u);
                foreach (Usuarios item in list)
                {
                    Console.WriteLine(item.ToString());
                }

                Console.WriteLine("***completado con exito!***");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadLine();
            }
        }

        public static List<Usuarios> FindName(Usuarios usuario)
        {
            try
            {
                string strSearchAD = "";

                if (usuario.Description != "")
                {
                    strSearchAD = "(description=" + usuario.Description.ToLower() + ")";
                    Console.WriteLine("Buscando por Description: " + usuario.Description.ToLower() + "\n");
                }
                if (usuario.Names != "")
                {
                    strSearchAD = "(cn=" + usuario.Names.ToLower() + ")";
                    Console.WriteLine("Buscando por CN: " + usuario.Names.ToLower() + "\n");
                }
                if (usuario.Email != "")
                {

                    strSearchAD = "(mail=" + usuario.Email.ToLower() + ")";
                    Console.WriteLine("Buscando por mail: " + usuario.Email.ToLower() + "\n");
                }
                if (usuario.LastName != "")
                {

                    strSearchAD += "(sn=" + usuario.LastName.ToLower() + ")";
                    Console.WriteLine("Buscando por SN: " + usuario.LastName.ToLower() + "\n");
                }
                if (usuario.Login != "")
                {
                    strSearchAD += "(sAMAccountName=" + usuario.Login + ")";
                    Console.WriteLine("Buscando por sAMAccountName: " + usuario.Login + "\n");
                }

                List<Usuarios> listUsuarios = new List<Usuarios>();
                DirectoryEntry adEntry = new DirectoryEntry(ldap, userAD, passAD, AuthenticationTypes.Secure);
                DirectorySearcher adSearch = new DirectorySearcher(adEntry);

                adSearch.Filter = "(&(objectClass=user)" + strSearchAD + ")";

                SearchResultCollection objResultados;
                //adResult = adSearch.FindOne(); // if only i need the first returned user 
                objResultados = adSearch.FindAll();

                foreach (SearchResult MiObjeto in objResultados)
                {
                    Usuarios usarioToAdd = new Usuarios();
                    usarioToAdd.Email = MiObjeto.Properties["mail"][0].ToString();
                    usarioToAdd.Login = MiObjeto.Properties["sAMAccountName"][0].ToString();
                    usarioToAdd.Names = MiObjeto.Properties["givenName"][0].ToString();
                    usarioToAdd.Description = MiObjeto.Properties["description"][0].ToString();
                    usarioToAdd.LastName = MiObjeto.Properties["sn"][0].ToString();
                    listUsuarios.Add(usarioToAdd);
                }
                return listUsuarios;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " - " + ex.StackTrace);
            }
        }

        public static void intento2()
        {
            DirectoryEntry deUser = new DirectoryEntry("LDAP://192.168.15.196/cn=user3,cn=Users,dc=datasoft,dc=cl");

            if (deUser != null)
            {
                Console.WriteLine(deUser.Properties["mail"][0].ToString());
            }
        }
    }

    public class Usuarios
    {
        private int id_User;

        public int Id_User
        {
            get { return id_User; }
            set { id_User = value; }
        }

        private string login;

        public string Login
        {
            get { return login; }
            set { login = value; }
        }

        private string password;

        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        private string lastName;

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        private string names;

        public string Names
        {
            get { return names; }
            set { names = value; }
        }

        public string Description { get; set; }


        public string ToString()
        {
            return "id_User: " + id_User + "\n" +
                "login: " + login + "\n" +
                "password: " + password + "\n" +
                "email:" + email + "\n" +
                "lastName: " + lastName + "\n" +
                "names: " + names + "\n" +
                "description: " + Description;
        }
    }
}
