﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PruebasC
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            //Creamos una variable tipo Bitmap
            Bitmap b;

            //Usamos el constructor para instanciar nuestro Bitmap y asignarle el tamaño 
            //de nuestro PictureBox (EN ESTE CASO EL PICTUREBOX TIENE COMO NOMBRE pictureBox1)
            b = new Bitmap(pictureBox1.Width, pictureBox1.Height);

            //Asignamos nuestro objeto Bitmap como imagen de nuestro pictureBox
            pictureBox1.Image = (Image)b;

            //Creamos el objeto Graphics que nos servirá para dibujar en nuestro Bitmap
            Graphics g = Graphics.FromImage(b);
            g.FillEllipse(Brushes.Black, new Rectangle(0, 0, 100, 60));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Creamos una variable tipo Bitmap
            Bitmap b;

            //Usamos el constructor para instanciar nuestro Bitmap y asignarle el tamaño 
            //de nuestro PictureBox (EN ESTE CASO EL PICTUREBOX TIENE COMO NOMBRE pictureBox1)
            b = new Bitmap(pictureBox1.Width, pictureBox1.Height);

            //Asignamos nuestro objeto Bitmap como imagen de nuestro pictureBox
            pictureBox1.Image = (Image)b;

            //Creamos el objeto Graphics que nos servirá para dibujar en nuestro Bitmap
            Graphics g = Graphics.FromImage(b);

            g.DrawPie(Pens.Black, new Rectangle(40, 40, 40, 40), 360, 270);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Creamos una variable tipo Bitmap
            Bitmap b;

            //Usamos el constructor para instanciar nuestro Bitmap y asignarle el tamaño 
            //de nuestro PictureBox (EN ESTE CASO EL PICTUREBOX TIENE COMO NOMBRE pictureBox1)
            b = new Bitmap(pictureBox1.Width, pictureBox1.Height);

            //Asignamos nuestro objeto Bitmap como imagen de nuestro pictureBox
            pictureBox1.Image = (Image)b;

            //Creamos el objeto Graphics que nos servirá para dibujar en nuestro Bitmap
            Graphics g = Graphics.FromImage(b);
            g.FillPolygon(Brushes.Black, new Point[] { new Point(4, 4), 
                                                        new Point(50, 10), 
                                                        new Point(70, 100), 
                                                        new Point(20, 90) });
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //Creamos una variable tipo Bitmap
            Bitmap b;

            //Usamos el constructor para instanciar nuestro Bitmap y asignarle el tamaño 
            //de nuestro PictureBox (EN ESTE CASO EL PICTUREBOX TIENE COMO NOMBRE pictureBox1)
            b = new Bitmap(pictureBox1.Width, pictureBox1.Height);

            //Asignamos nuestro objeto Bitmap como imagen de nuestro pictureBox
            pictureBox1.Image = (Image)b;
            for (int i = 0; i < pictureBox1.Width; i++)
            {
                for (int j = 0; j < pictureBox1.Height; j++)
                {
                    //Con esta instrucció dibujamos un solo pixel,
                    //Los dos ciclos for anidados harán que el pictureBox se rellene completamente
                    //dibujando pixel por pixel
                    b.SetPixel(i, j, Color.FromArgb(0, 0, 0));
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //Creamos una variable tipo Bitmap
            Bitmap b;

            //Usamos el constructor para instanciar nuestro Bitmap y asignarle el tamaño 
            //de nuestro PictureBox (EN ESTE CASO EL PICTUREBOX TIENE COMO NOMBRE pictureBox1)
            b = new Bitmap(pictureBox1.Width, pictureBox1.Height);

            //Asignamos nuestro objeto Bitmap como imagen de nuestro pictureBox
            pictureBox1.Image = (Image)b;

            //Creamos el objeto Graphics que nos servirá para dibujar en nuestro Bitmap
            Graphics g = Graphics.FromImage(b);

            g.FillRectangle(Brushes.Black,new Rectangle(20,20,60,80));
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //Creamos una variable tipo Bitmap
            Bitmap b;

            //Usamos el constructor para instanciar nuestro Bitmap y asignarle el tamaño 
            //de nuestro PictureBox (EN ESTE CASO EL PICTUREBOX TIENE COMO NOMBRE pictureBox1)
            b = new Bitmap(pictureBox1.Width, pictureBox1.Height);

            //Asignamos nuestro objeto Bitmap como imagen de nuestro pictureBox
            pictureBox1.Image = (Image)b;

            //Creamos el objeto Graphics que nos servirá para dibujar en nuestro Bitmap
            Graphics g = Graphics.FromImage(b);

            g.DrawLine(Pens.Black, new Point(0, 0), new Point(50, 50));
        } 
    }
}
