﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace testCallAjax
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //1.- Agregar Etiqueta [WebMethod]
        [WebMethod]
        public static string[] GetCategory(string term)
        {
            //Ir a "App_Start/RouteConfig.cs"
            //y modificar
            //"settings.AutoRedirectMode = RedirectMode.Permanent;"
            //por
            //"settings.AutoRedirectMode = RedirectMode.Off;"

            List<string> retCategory = new List<string>();
            retCategory.Add("edgardo");
            retCategory.Add("alberto");
            retCategory.Add("gaston");
            retCategory.Add("marcelo");
            return retCategory.ToArray();
        }
    }
}