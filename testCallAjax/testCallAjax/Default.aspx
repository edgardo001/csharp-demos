﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="testCallAjax._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
	<%-- El CSS "jquery-ui.css" es obligatorio para que muestre los datos como
		una extencion del texbox, y no como lista html independiente--%>
    <link href="Content/jquery-ui.css" rel="stylesheet" />
    <%-- se debe cargar obligatoriamente "Scripts/jquery-ui.min.js" la cual no viene 
        dentro del proyecto asp.net, si no es cargada, indicara que la propiedad autocomplete no existe --%>
    <script src="Scripts/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            //Indicar el elemento a entregar el autocomplete
            $("#txtAutoComplete").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        //Indicar la url del WebService, las dos opciones funcionan OK
                        //url: '<%= ResolveUrl("Default.aspx/GetCategory") %>',
                        url: "Default.aspx/GetCategory",
                        //Indicar el parametro de entrada en el Metedo del Webservice
                        //en este caso es "term", luego entregar el valor solicitado por el web service
                        data: "{'term':'" + $("#txtAutoComplete").val() + "'}",
                        //Formato en que volvera la peticion
                        dataType: "json",
                        //En el caso de funcionar, entra en este lugar
                        success: function (data) {
                            response(data.d);
                        },
                        //en caso de error, dara aviso, la falla sera entregada por "result[2]"
                        error: function (result) {
                            alert("Error: " + result[0] + result[2] + result[3]);
                        }
                    });
                }
            });
        });
    </script>
    <input type="text" id="txtAutoComplete" />
</asp:Content>
