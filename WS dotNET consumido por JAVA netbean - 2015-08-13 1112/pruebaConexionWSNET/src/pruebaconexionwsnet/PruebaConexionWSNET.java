/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebaconexionwsnet;

import java.util.List;

/**
 *
 * @author Datasoft
 */
public class PruebaConexionWSNET {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        lecturaWS();
    }

    /**
     * http://localhost/testeo/Service1.asmx?wsdl
     */
    private static void lecturaWS() {        
        for (Entidad col : prueba().getEntidad()) {
            System.out.println("------------------");
            System.out.println(col.val1);
            System.out.println(col.val2);
            System.out.println(col.val3);
            System.out.println(col.val4);
            System.out.println(col.val5);
            System.out.println("------------------");
        }        
    }

    private static ArrayOfEntidad prueba() {
        pruebaconexionwsnet.Service1 service = new pruebaconexionwsnet.Service1();
        pruebaconexionwsnet.Service1Soap port = service.getService1Soap();
        return port.prueba();
    }
    
}
