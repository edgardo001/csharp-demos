
package pruebaconexionwsnet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pruebaResult" type="{http://tempuri.org/}ArrayOfEntidad" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pruebaResult"
})
@XmlRootElement(name = "pruebaResponse")
public class PruebaResponse {

    protected ArrayOfEntidad pruebaResult;

    /**
     * Obtiene el valor de la propiedad pruebaResult.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfEntidad }
     *     
     */
    public ArrayOfEntidad getPruebaResult() {
        return pruebaResult;
    }

    /**
     * Define el valor de la propiedad pruebaResult.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfEntidad }
     *     
     */
    public void setPruebaResult(ArrayOfEntidad value) {
        this.pruebaResult = value;
    }

}
