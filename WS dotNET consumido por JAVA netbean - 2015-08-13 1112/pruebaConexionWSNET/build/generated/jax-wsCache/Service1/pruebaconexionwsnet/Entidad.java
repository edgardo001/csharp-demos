
package pruebaconexionwsnet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para Entidad complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Entidad">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="_val1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="_val2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="_val3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="_val4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="_val5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Entidad", propOrder = {
    "val1",
    "val2",
    "val3",
    "val4",
    "val5"
})
public class Entidad {

    @XmlElement(name = "_val1")
    protected String val1;
    @XmlElement(name = "_val2")
    protected String val2;
    @XmlElement(name = "_val3")
    protected String val3;
    @XmlElement(name = "_val4")
    protected String val4;
    @XmlElement(name = "_val5")
    protected String val5;

    /**
     * Obtiene el valor de la propiedad val1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVal1() {
        return val1;
    }

    /**
     * Define el valor de la propiedad val1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVal1(String value) {
        this.val1 = value;
    }

    /**
     * Obtiene el valor de la propiedad val2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVal2() {
        return val2;
    }

    /**
     * Define el valor de la propiedad val2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVal2(String value) {
        this.val2 = value;
    }

    /**
     * Obtiene el valor de la propiedad val3.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVal3() {
        return val3;
    }

    /**
     * Define el valor de la propiedad val3.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVal3(String value) {
        this.val3 = value;
    }

    /**
     * Obtiene el valor de la propiedad val4.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVal4() {
        return val4;
    }

    /**
     * Define el valor de la propiedad val4.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVal4(String value) {
        this.val4 = value;
    }

    /**
     * Obtiene el valor de la propiedad val5.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVal5() {
        return val5;
    }

    /**
     * Define el valor de la propiedad val5.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVal5(String value) {
        this.val5 = value;
    }

}
