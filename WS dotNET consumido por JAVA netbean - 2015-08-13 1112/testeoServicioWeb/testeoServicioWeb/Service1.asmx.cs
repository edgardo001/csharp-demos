﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;

namespace testeoServicioWeb
{
    /// <summary>
    /// Descripción breve de Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class Service1 : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld(String var)
        {
            return "Hello World: " + var ;
        }

        [WebMethod]
        public List<Entidad> prueba()
        {
            List<Entidad> l = new List<Entidad>();

            for (int i = 0; i < 10; i++)
            {
               Entidad e = new Entidad(""+i + 1,""+ i + 2, ""+i + 3,""+ i + 4,""+ i + 5);
               l.Add(e);
            }
            return l;
        }
    }
}