﻿using System;
using System.Collections.Generic;
using System.Web;

namespace testeoServicioWeb
{
    public class Entidad
    {
        public String _val1;
        public String _val2;
        public String _val3;
        public String _val4;
        public String _val5;

        public Entidad()
        {
        }

        public Entidad(String val1, String val2, String val3, String val4, String val5)
        {
            _val1 = val1;
            _val2 = val2;
            _val3 = val3;
            _val4 = val4;
            _val5 = val5;
        }

    }
}