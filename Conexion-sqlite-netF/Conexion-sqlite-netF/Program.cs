﻿using System;
using System.Data.SQLite;
//Agregar referencia System.Data.SQLite (System.Data.SQLite.dll) en el proyecto.

//Ademas se debe copiar "SQLite.Interop.dll"(no es referenciable) al directorio donde estara el ejecutable
//Lo que yo hice, fue agregarlas al proyecto como elementos existentes,
//luego seleccionarlas en proyecto e ir a propiedades, luego modificar:
//"Copiar en el directorio de salida: Copiar Siempre"

/** Todo lo necesario esta dentro del proyecto, revisar el interior de sus carpeta **/

namespace Conexion_sqlite_netF
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("1.- Metodo que se conexta a una DB existente");
            connEjemploBDexistente();
            Console.WriteLine("2.- Metodo que crea, inserta y obtiene datos de la DB");
            createDB();
        }

        private static void createDB()
        {
            SQLiteConnection conn = new SQLiteConnection();
            //conn.ConnectionString = @"Data Source=bd.db;Version=3;New=True;Compress=True;Password=Passw0rd";//Para crear la db si no existe
            conn.ConnectionString = @"Data Source=bd.db;Version=3;New=True;Compress=True;";//Para crear la db si no existe
            try
            {
                //Abro la conexion
                conn.Open();
                Console.WriteLine("Conexion Sqlite3 OK");
                String createTable = "CREATE TABLE COMPANY(" +
                                    "ID INT PRIMARY KEY NOT NULL," +
                                    "NAME TEXT NOT NULL," +
                                    "AGE INT NOT NULL," +
                                    "ADDRESS CHAR(50), " +
                                    "SALARY REAL" +
                                    "); ";
                //Creo una tabla
                SQLiteCommand command = new SQLiteCommand(createTable, conn);
                int varCreateTable = command.ExecuteNonQuery();
                Console.WriteLine("Se crea tabla COMPANY con retorno ExecuteNonQuery(): " + varCreateTable);

                //inserto un registro en la tabla
                command.CommandText = "insert into company(ID, NAME, AGE, ADDRESS, SALARY)values(1, 'edgardo vasquez', 26, 'Av Independencia #1234', 1000000);";
                int varInsertData = command.ExecuteNonQuery();
                Console.WriteLine("Se inserta registro en COMPANY con retorno ExecuteNonQuery(): " + varCreateTable);

                //obtengo el registro de la tabla
                command.CommandText = "Select * from COMPANY";
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    Console.WriteLine("El nombre ingresado es: " + reader["NAME"].ToString());
                    Console.WriteLine("Su sueldo es de: " + reader["SALARY"].ToString());
                }

                conn.Close();
                Console.WriteLine("Se cierra conexion a Sqlite3");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to connect to data source: " + ex.Message);
            }
            finally
            {
                //Cierro la conexion
                conn.Close();
                Console.ReadLine();
            }
        }

        private static void connEjemploBDexistente()
        {
            int idBuscar = 1;

            String cadena = @"Data Source=ZKTimeNet.db;Version=3";
            SQLiteConnection conn = new SQLiteConnection(cadena, true);

            try
            {
                conn.Open();
                Console.WriteLine("Conexion a SQLite OK");

                string sql = "select * from sys_user where id=@id;";
                SQLiteCommand command = new SQLiteCommand(sql, conn);
                command.Parameters.AddWithValue("@id", idBuscar);
                SQLiteDataReader reader = command.ExecuteReader();
                Console.WriteLine("Se ejecuta la consulta: " + sql);
                while (reader.Read())
                {
                    Console.WriteLine("El nombre del usuario con id=" + idBuscar + " es: " + reader["username"].ToString());
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to connect to data source: " + ex.Message);
            }
            finally
            {
                conn.Close();
                Console.WriteLine("Se cierra la conexion con SQLite");
                Console.ReadLine();
            }
        }
    }
}
