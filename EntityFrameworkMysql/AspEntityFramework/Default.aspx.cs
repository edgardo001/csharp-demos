﻿using Conexion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AspEntityFramework
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                select();
            }
            catch (Exception ex)
            {
                lblTest.Text = ex.Message + "\n" + ex.StackTrace;
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!txtRut.Text.Equals(string.Empty) || !txtCorreo.Text.Equals(string.Empty) || !txtClave.Text.Equals(string.Empty))
                {
                    Insert();
                }
                else
                {
                    Response.Write("<script language=javascript>alert('Debe ingresar parametros como minimo');</script>");
                }
            }
            catch (Exception ex)
            {
                lblTest.Text = ex.Message + "\n" + ex.StackTrace;
            }
        }

        protected void btnConsulta_Click(object sender, EventArgs e)
        {
            try
            {
                selectByRut();
            }
            catch (Exception ex)
            {
                lblTest.Text = ex.Message + "\n" + ex.StackTrace;
            }
        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                Update();
            }
            catch (Exception ex)
            {
                lblTest.Text = ex.Message + "\n" + ex.StackTrace;
            }
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                delete();
            }
            catch (Exception ex)
            {
                lblTest.Text = ex.Message + "\n" + ex.StackTrace;
            }
        }


        //-----------------------------------------

        private void select()
        {
            try
            {
                grilla.DataSource = bbdd.select();
                grilla.DataBind();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }
        private void selectByRut()
        {
            try
            {
                usuario u = bbdd.selectByRut(txtRut.Text);
                txtCorreo.Text = u.EMAIL_USR;
                txtClave.Text = u.CLAVE_USR;
                txtDescripcion.Text = u.DESCRIPCION_USR;
                txtNombreConf.Text = u.NOMBRECONFIGFIRMA_USR;
                txtNombre.Text = u.NOMBRE_USR;
                txtApellido.Text = u.APELLIDO_USR;
                txtRut.Text = u.RUT_USR;
                txtDireccion.Text = u.DIRECCION_USR;
                txtTelefono.Text = u.TELEFONO_USR;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }
        private void delete()
        {
            try
            {
                String aux = bbdd.delete(txtRut.Text);
                Response.Write("<script language=javascript>alert('" + aux + "'); window.location = '" + Request.Url.Segments[Request.Url.Segments.Length - 1] + "';</script>");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }
        private void Update()
        {
            try
            {
                usuario u = new usuario
                {
                    EMAIL_USR = txtCorreo.Text,
                    CLAVE_USR = txtClave.Text,
                    ESTADO_USR = 1,
                    DESCRIPCION_USR = txtDescripcion.Text,
                    NOMBRECONFIGFIRMA_USR = txtNombreConf.Text,
                    PERMITLOGIN_USR = true,
                    NOMBRE_USR = txtNombre.Text,
                    APELLIDO_USR = txtApellido.Text,
                    RUT_USR = txtRut.Text,
                    DIRECCION_USR = txtDireccion.Text,
                    TELEFONO_USR = txtTelefono.Text,
                };

                String aux = bbdd.update(u);
                Response.Write("<script language=javascript>alert('"+aux+"'); window.location = '" + Request.Url.Segments[Request.Url.Segments.Length - 1] + "';</script>");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }
        private void Insert()
        {
            try
            {
                usuario u = new usuario
                {
                    EMAIL_USR = txtCorreo.Text,
                    CLAVE_USR = txtClave.Text,
                    ESTADO_USR = 1,
                    DESCRIPCION_USR = txtDescripcion.Text,
                    NOMBRECONFIGFIRMA_USR = txtNombreConf.Text,
                    PERMITLOGIN_USR = true,
                    NOMBRE_USR = txtNombre.Text,
                    APELLIDO_USR = txtApellido.Text,
                    RUT_USR = txtRut.Text,
                    DIRECCION_USR = txtDireccion.Text,
                    TELEFONO_USR = txtTelefono.Text,
                };

                bbdd.insert(u);
                Response.Write("<script language=javascript>alert('Sus datos fueron almacenados con exito'); window.location = '" + Request.Url.Segments[Request.Url.Segments.Length - 1] + "';</script>");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }
    }
}