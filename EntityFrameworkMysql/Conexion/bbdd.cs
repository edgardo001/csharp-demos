﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conexion
{
    public class bbdd
    {
        public static string insert(usuario u)
        {
            try
            {
                //6.- Instancio a "websigner_devEntities" la cual se nombro en el momento de la creacion, 
                //por defecto es algo como "myBdEntities",
                //se puede verificar en App.config etiqute "connectionStrings", elemento "add" atributo "name"
                websigner_devEntities testcontext = new websigner_devEntities();
                //Agrego el usuario creado anteriormente
                testcontext.usuarios.Add(u);
                //Guardo los cambios realizados
                int aux = testcontext.SaveChanges();

                return "Insercion OK: " + aux;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public static List<usuario> select()
        {
            try
            {
                websigner_devEntities testcontext = new websigner_devEntities();
                var load = from g in testcontext.usuarios select g;
                if (load != null)
                {
                    return load.ToList();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public static string update(usuario u)
        {
            try
            {
                websigner_devEntities testcontext = new websigner_devEntities();
                usuario uS = testcontext.usuarios.First(i => i.RUT_USR == u.RUT_USR);
                if (!uS.Equals(null))
                {
                    uS.EMAIL_USR = u.EMAIL_USR;
                    uS.CLAVE_USR = u.CLAVE_USR;
                    uS.ESTADO_USR = u.ESTADO_USR;
                    uS.DESCRIPCION_USR = u.DESCRIPCION_USR;
                    uS.NOMBRECONFIGFIRMA_USR = u.NOMBRECONFIGFIRMA_USR;
                    uS.PERMITLOGIN_USR = u.PERMITLOGIN_USR;
                    uS.NOMBRE_USR = u.NOMBRE_USR;
                    uS.APELLIDO_USR = u.APELLIDO_USR;
                    uS.RUT_USR = u.RUT_USR;
                    uS.DIRECCION_USR = u.DIRECCION_USR;
                    uS.TELEFONO_USR = u.TELEFONO_USR;

                    testcontext.SaveChanges();
                }

                return "El registro ha sido eliminado";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public static string delete(string text)
        {
            try
            {
                websigner_devEntities testcontext = new websigner_devEntities();
                var load = from g in testcontext.usuarios where g.RUT_USR.Equals(text) select g;
                if (!load.Equals(null))
                {
                    testcontext.usuarios.Remove(load.First());
                    testcontext.SaveChanges();
                }
                return "El registro ha sido eliminado";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        public static usuario selectByRut(string text)
        {
            try
            {
                websigner_devEntities testcontext = new websigner_devEntities();
                var load = from g in testcontext.usuarios where g.RUT_USR.Equals(text) select g;
                usuario u = load.First();
                return u;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

    }
}
