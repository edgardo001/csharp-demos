CREATE DATABASE  IF NOT EXISTS `websigner_dev` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `websigner_dev`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: 192.168.15.27    Database: websigner_dev
-- ------------------------------------------------------
-- Server version	5.6.19-enterprise-commercial-advanced-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `configuracion`
--

DROP TABLE IF EXISTS `configuracion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuracion` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NOMBREEMPRESA` varchar(50) NOT NULL,
  `RUTEMPRESA` varchar(10) NOT NULL,
  `TELEFONOEMPRESA` varchar(15) NOT NULL,
  `EMAILEMPRESA1` varchar(100) NOT NULL,
  `EMAILEMPRESA1CLAVE` varchar(50) NOT NULL,
  `EMAILEMPRESA1PUERTO` varchar(5) NOT NULL,
  `EMAILEMPRESA1RUTAHOST` varchar(100) NOT NULL,
  `EMAILEMPRESA1AUTOSEND` tinyint(1) DEFAULT NULL,
  `EMAILEMPRESA2` varchar(100) DEFAULT NULL,
  `EMAILEMPRESA3` varchar(100) DEFAULT NULL,
  `DIRECCIONEMPRESA` varchar(100) NOT NULL,
  `WEBSITEEMPRESA` varchar(500) NOT NULL,
  `WEBSITEVALIDACIONEMPRESA` varchar(1000) DEFAULT NULL,
  `DESCRIPCIONEMPRESA` varchar(250) DEFAULT NULL,
  `FECHACREACION_EMP` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `FECHAMODIFICACION_EMP` datetime NOT NULL,
  `LOGO_EMP` longblob,
  `ENDPOINTFIRMADOR1` varchar(500) DEFAULT NULL,
  `RUTAALMACENAMIENTOBASE1` varchar(250) NOT NULL,
  `RUTAALMACENAMIENTOBASE2` varchar(250) DEFAULT NULL,
  `RUTAALMACENAMIENTOBASE3` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Tabla que almacena la configuracion y rutas del sitio web';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuracion`
--

LOCK TABLES `configuracion` WRITE;
/*!40000 ALTER TABLE `configuracion` DISABLE KEYS */;
INSERT INTO `configuracion` VALUES (1,'DEMO Datasoft','99999999-9','56228419800','contacto@outlook.com','NJV+m6NFWkqZ1yK0hpOsESO6Xbo=','587','smtp-mail.outlook.com',0,'none','none','Padre Mariano 210','http://www.datasoft.cl',NULL,'Empresa de servicios de firma electronica','2016-04-14 16:28:50','2016-04-14 16:28:50',NULL,'http://192.168.15.32:8080/DataSignerEnterprese/services/DatasignerEnterprese?wsdl','C:\\\\PRUEBAS1','C:\\\\PRUEBAS2','C:\\\\PRUEBAS3');
/*!40000 ALTER TABLE `configuracion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `docaut`
--

DROP TABLE IF EXISTS `docaut`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docaut` (
  `ID_UTD` int(11) NOT NULL AUTO_INCREMENT,
  `ID_USR` int(11) DEFAULT NULL,
  `ID_TIPDOC` int(11) DEFAULT NULL,
  `FECHACREACION_DA` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_UTD`),
  KEY `FK_REFERENCE_30` (`ID_USR`),
  KEY `FK_REFERENCE_31` (`ID_TIPDOC`),
  CONSTRAINT `FK_REFERENCE_30` FOREIGN KEY (`ID_USR`) REFERENCES `usuario` (`ID_USR`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_REFERENCE_31` FOREIGN KEY (`ID_TIPDOC`) REFERENCES `tipodocumento` (`ID_TIPDOC`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='nivel del documento a firmar';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `docaut`
--

LOCK TABLES `docaut` WRITE;
/*!40000 ALTER TABLE `docaut` DISABLE KEYS */;
INSERT INTO `docaut` VALUES (1,300,100,'2016-04-14 16:14:57'),(2,300,101,'2016-04-14 16:14:57'),(3,300,102,'2016-04-14 16:14:57'),(4,300,103,'2016-04-14 16:14:57'),(5,300,104,'2016-04-14 16:14:57'),(6,300,105,'2016-04-14 16:14:57'),(7,300,106,'2016-04-14 16:14:57'),(8,300,107,'2016-04-14 16:14:57'),(9,1000,107,'2016-04-14 16:14:57'),(10,1000,106,'2016-04-14 16:14:57'),(11,1000,105,'2016-04-14 16:14:57'),(12,1000,104,'2016-04-14 16:14:57'),(13,1000,102,'2016-04-14 16:14:57');
/*!40000 ALTER TABLE `docaut` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documento`
--

DROP TABLE IF EXISTS `documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documento` (
  `ID_D` int(11) NOT NULL AUTO_INCREMENT COMMENT 'identificador unico de la tabla',
  `ID_REC` int(11) DEFAULT NULL COMMENT 'identificador del rechazo',
  `ID_ED` int(11) DEFAULT NULL COMMENT 'identificador del estado del documento',
  `ID_TIPDOC` int(11) DEFAULT NULL COMMENT 'identificador del tipo de documento',
  `ID_USR` int(11) DEFAULT NULL COMMENT 'identificador del usuario dueño del documento',
  `ID_GF` int(11) DEFAULT NULL COMMENT 'identificador del grupofirmante del documento',
  `ID_TFD` int(11) DEFAULT NULL COMMENT 'identificador del tipo de firma del documento unica, grupal o multiple',
  `NOMBREDOC_D` varchar(50) DEFAULT NULL,
  `RUTANOSIGN_D` varchar(230) DEFAULT NULL,
  `RUTASIGN_D` varchar(230) DEFAULT NULL,
  `SIGNCOMPLETADO_D` tinyint(1) DEFAULT NULL COMMENT 'es 1 cuando se completan las firmas del doc',
  `FECHAHORACREACION_D` datetime DEFAULT CURRENT_TIMESTAMP,
  `FECHAHORAMODICICACION_D` datetime DEFAULT NULL,
  `FECHAHORACOMPLETADO_D` datetime DEFAULT NULL,
  `CANTIDAD_FIRMANTES` int(11) DEFAULT NULL COMMENT 'cantidad de personas que firmaran este documento',
  `FIRMASACTUALESDOC_D` int(11) DEFAULT NULL COMMENT 'numero de firmas actuales del documento',
  `DESCRIPCION_D` varchar(250) DEFAULT NULL,
  `DESCRIPCIONRECHAZO_D` varchar(250) DEFAULT NULL COMMENT 'motivo breve del rechazo',
  `ID_USRANTERIOR` int(11) DEFAULT NULL COMMENT 'indica quien es el usuario que firmo antes el doc',
  `ID_USRACTUAL` int(11) DEFAULT NULL COMMENT 'indica el usuario actual que debe firmar el doc',
  `ID_USRSIGUIENTE` int(11) DEFAULT NULL COMMENT 'indica el usario siguiente que debe firmar el documento',
  PRIMARY KEY (`ID_D`),
  KEY `FK_REFERENCE_16` (`ID_REC`),
  KEY `FK_REFERENCE_17` (`ID_ED`),
  KEY `FK_REFERENCE_19` (`ID_GF`),
  KEY `FK_REFERENCE_20` (`ID_TFD`),
  KEY `FK_REFERENCE_24` (`ID_TIPDOC`),
  KEY `FK_REFERENCE_25` (`ID_USR`),
  CONSTRAINT `FK_REFERENCE_16` FOREIGN KEY (`ID_REC`) REFERENCES `tiporechazo` (`ID_REC`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_REFERENCE_17` FOREIGN KEY (`ID_ED`) REFERENCES `estadodocumento` (`ID_ED`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_REFERENCE_19` FOREIGN KEY (`ID_GF`) REFERENCES `grupofirmantes` (`ID_GF`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_REFERENCE_20` FOREIGN KEY (`ID_TFD`) REFERENCES `tipofirmadoc` (`ID_TFD`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_REFERENCE_24` FOREIGN KEY (`ID_TIPDOC`) REFERENCES `tipodocumento` (`ID_TIPDOC`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_REFERENCE_25` FOREIGN KEY (`ID_USR`) REFERENCES `usuario` (`ID_USR`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tabla que almacena le documentos original (el primero subido';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documento`
--

LOCK TABLES `documento` WRITE;
/*!40000 ALTER TABLE `documento` DISABLE KEYS */;
/*!40000 ALTER TABLE `documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estadodocumento`
--

DROP TABLE IF EXISTS `estadodocumento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadodocumento` (
  `ID_ED` int(11) NOT NULL AUTO_INCREMENT,
  `NOMBRE_ED` varchar(20) NOT NULL,
  `DESCRIPCION_ED` varchar(200) NOT NULL,
  PRIMARY KEY (`ID_ED`)
) ENGINE=InnoDB AUTO_INCREMENT=401 DEFAULT CHARSET=utf8 COMMENT='Tabla que almacena los estados posibles de un documento';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadodocumento`
--

LOCK TABLES `estadodocumento` WRITE;
/*!40000 ALTER TABLE `estadodocumento` DISABLE KEYS */;
INSERT INTO `estadodocumento` VALUES (100,'firmado','documento firmado'),(200,'rechazado','documento rechazado por el firmante'),(300,'firmar','documento preparado para ser firmado'),(400,'historico','documento con proceso terminado');
/*!40000 ALTER TABLE `estadodocumento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evento`
--

DROP TABLE IF EXISTS `evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento` (
  `ID_EVT` int(11) NOT NULL AUTO_INCREMENT,
  `ID_TIPEVT` int(11) DEFAULT NULL,
  `ID_USR` int(11) DEFAULT NULL,
  `MENSAJE_EVT` varchar(250) NOT NULL,
  `FECHAHORA_EVT` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID_EVT`),
  KEY `FK_REFERENCE_26` (`ID_TIPEVT`),
  KEY `FK_REFERENCE_27` (`ID_USR`),
  CONSTRAINT `FK_REFERENCE_26` FOREIGN KEY (`ID_TIPEVT`) REFERENCES `tipoevento` (`ID_TIPEVT`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_REFERENCE_27` FOREIGN KEY (`ID_USR`) REFERENCES `usuario` (`ID_USR`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tabla que registra los eventos realizados por un usuario';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento`
--

LOCK TABLES `evento` WRITE;
/*!40000 ALTER TABLE `evento` DISABLE KEYS */;
/*!40000 ALTER TABLE `evento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eventodocumento`
--

DROP TABLE IF EXISTS `eventodocumento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eventodocumento` (
  `ID_ED` int(11) NOT NULL AUTO_INCREMENT,
  `ID_TED` int(11) DEFAULT NULL,
  `ID_D` int(11) DEFAULT NULL,
  `ID_USR` int(10) DEFAULT NULL,
  `FECHAHORA_ED` datetime NOT NULL,
  `DESCRIPCION_ED` varchar(250) NOT NULL,
  PRIMARY KEY (`ID_ED`),
  KEY `FK_REFERENCE_21` (`ID_TED`),
  KEY `FK_REFERENCE_22` (`ID_D`),
  KEY `FK_REFERENCE_23` (`ID_USR`),
  CONSTRAINT `FK_REFERENCE_21` FOREIGN KEY (`ID_TED`) REFERENCES `tipoeventodoc` (`ID_TED`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_REFERENCE_22` FOREIGN KEY (`ID_D`) REFERENCES `documento` (`ID_D`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_REFERENCE_23` FOREIGN KEY (`ID_USR`) REFERENCES `usuario` (`ID_USR`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eventodocumento`
--

LOCK TABLES `eventodocumento` WRITE;
/*!40000 ALTER TABLE `eventodocumento` DISABLE KEYS */;
/*!40000 ALTER TABLE `eventodocumento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupofirmantes`
--

DROP TABLE IF EXISTS `grupofirmantes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupofirmantes` (
  `ID_GF` int(11) NOT NULL AUTO_INCREMENT,
  `NOMBRE_GF` varchar(100) NOT NULL,
  `DESCRIPCION_GF` varchar(250) NOT NULL,
  `FECHACREACION_GF` datetime DEFAULT CURRENT_TIMESTAMP,
  `FECHAM_ODIFICACION_GF` datetime DEFAULT NULL,
  `ESTADO_GF` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID_GF`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupofirmantes`
--

LOCK TABLES `grupofirmantes` WRITE;
/*!40000 ALTER TABLE `grupofirmantes` DISABLE KEYS */;
/*!40000 ALTER TABLE `grupofirmantes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `grupofirmanteusuario`
--

DROP TABLE IF EXISTS `grupofirmanteusuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupofirmanteusuario` (
  `ID_GFU` int(11) NOT NULL AUTO_INCREMENT,
  `ID_GF` int(11) DEFAULT NULL,
  `ID_USR` int(11) DEFAULT NULL,
  `FECHACREACION` datetime DEFAULT CURRENT_TIMESTAMP,
  `FECHAMODIFICACION` datetime DEFAULT NULL,
  `ORDENUSUARIO_GFU` int(11) NOT NULL,
  `TOTALUSUARIOS_GFU` int(11) NOT NULL,
  PRIMARY KEY (`ID_GFU`),
  KEY `FK_REFERENCE_18` (`ID_USR`),
  KEY `FK_REFERENCE_8` (`ID_GF`),
  CONSTRAINT `FK_REFERENCE_18` FOREIGN KEY (`ID_USR`) REFERENCES `usuario` (`ID_USR`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_REFERENCE_8` FOREIGN KEY (`ID_GF`) REFERENCES `grupofirmantes` (`ID_GF`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupofirmanteusuario`
--

LOCK TABLES `grupofirmanteusuario` WRITE;
/*!40000 ALTER TABLE `grupofirmanteusuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `grupofirmanteusuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rol` (
  `ID_ROL` int(11) NOT NULL AUTO_INCREMENT,
  `NOMBRE_ROL` varchar(25) NOT NULL,
  `ESTADO_ROL` smallint(6) NOT NULL,
  `DESCRIPCION_ROL` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`ID_ROL`)
) ENGINE=InnoDB AUTO_INCREMENT=601 DEFAULT CHARSET=utf8 COMMENT='Tabla que almacena los roles a usar por cada usuario';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES (100,'system',1,'Usuario usado para registro de log'),(200,'superadmin',1,'Usuario con mayores privilegios (Datasoft)'),(300,'administrador',1,'Usuario con privilegio elevado (Cliente)'),(400,'firmante',1,'Usuario con permiso de firmas'),(401,'firmante simple',1,'Usuario con permiso de firmas'),(402,'firmante avanzado',1,'Usuario con permiso de firmas'),(500,'visualizador',1,'Usuario con permiso para ver documentos'),(600,'usuario',1,'Usuario del sistema');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipodocumento`
--

DROP TABLE IF EXISTS `tipodocumento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipodocumento` (
  `ID_TIPDOC` int(11) NOT NULL AUTO_INCREMENT,
  `NOMBRE_TIPDOC` varchar(50) NOT NULL,
  `DESCRIPCION_TIPODOC` varchar(250) NOT NULL,
  `ESTADO_TIPODOC` smallint(6) NOT NULL,
  PRIMARY KEY (`ID_TIPDOC`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8 COMMENT='Tabla que almacena los posibles tipos de documentos';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipodocumento`
--

LOCK TABLES `tipodocumento` WRITE;
/*!40000 ALTER TABLE `tipodocumento` DISABLE KEYS */;
INSERT INTO `tipodocumento` VALUES (100,'contrato de trabajo','contrato de trabajo',1),(101,'anexo de contrato','anexo de contrato',1),(102,'actualizacion de contrato','actualizacion de contrato',1),(103,'finiquito','finiquito',1),(104,'anexo de finiquito','anexo de finiquito',1),(105,'comprobante feriados legales y progresivos','comprobante feriados legales y progresivos',1),(106,'permiso sin goce de remuneraciones','permiso sin goce de remuneraciones',1),(107,'otro','otro',1);
/*!40000 ALTER TABLE `tipodocumento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoevento`
--

DROP TABLE IF EXISTS `tipoevento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoevento` (
  `ID_TIPEVT` int(11) NOT NULL AUTO_INCREMENT,
  `NOMBRE_TIPEVT` varchar(25) NOT NULL,
  `DESCRIPCION_TIPEVT` varchar(250) NOT NULL,
  PRIMARY KEY (`ID_TIPEVT`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8 COMMENT='tabla que almacena los tipos de eventos (Log) del sitio web';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoevento`
--

LOCK TABLES `tipoevento` WRITE;
/*!40000 ALTER TABLE `tipoevento` DISABLE KEYS */;
INSERT INTO `tipoevento` VALUES (100,'Other','Para eventos no registrados'),(101,'Systema','Para eventos del sistema'),(102,'BBDD','Registros o log de la base de datos'),(103,'Web Site','Registros o log desde el sitio web');
/*!40000 ALTER TABLE `tipoevento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoeventodoc`
--

DROP TABLE IF EXISTS `tipoeventodoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoeventodoc` (
  `ID_TED` int(11) NOT NULL AUTO_INCREMENT,
  `NOMBRE_TED` varchar(50) NOT NULL,
  `DESCRIPCION_TED` varchar(250) NOT NULL,
  `ESTADO_TED` tinyint(1) NOT NULL,
  PRIMARY KEY (`ID_TED`)
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoeventodoc`
--

LOCK TABLES `tipoeventodoc` WRITE;
/*!40000 ALTER TABLE `tipoeventodoc` DISABLE KEYS */;
INSERT INTO `tipoeventodoc` VALUES (100,'otro','cuando se realiza una accion no especificada',1),(200,'firmado','cuando se realiza firma de un documento',1),(300,'rechazado','cuando se rechaza la firma de  un documento',1);
/*!40000 ALTER TABLE `tipoeventodoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipofirmadoc`
--

DROP TABLE IF EXISTS `tipofirmadoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipofirmadoc` (
  `ID_TFD` int(11) NOT NULL AUTO_INCREMENT,
  `NOMBRE_TFD` varchar(50) NOT NULL,
  `DESCRIPCION_TFD` varchar(250) NOT NULL,
  PRIMARY KEY (`ID_TFD`)
) ENGINE=InnoDB AUTO_INCREMENT=301 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipofirmadoc`
--

LOCK TABLES `tipofirmadoc` WRITE;
/*!40000 ALTER TABLE `tipofirmadoc` DISABLE KEYS */;
INSERT INTO `tipofirmadoc` VALUES (100,'unica','cuando el usuario dueño debe realizar la firma'),(200,'grupal','cuando un grupo ordenado debe firmar un documento'),(300,'multiple','cuando un grupo ordenado debe firmar un documento y por ultimo el usuario dueño');
/*!40000 ALTER TABLE `tipofirmadoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiporechazo`
--

DROP TABLE IF EXISTS `tiporechazo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tiporechazo` (
  `ID_REC` int(11) NOT NULL,
  `NOMBRE_REC` varchar(100) NOT NULL,
  `DESCRIPCION_REC` varchar(250) NOT NULL,
  PRIMARY KEY (`ID_REC`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabla que almacena los tipos de rechazo de un documentos, re';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiporechazo`
--

LOCK TABLES `tiporechazo` WRITE;
/*!40000 ALTER TABLE `tiporechazo` DISABLE KEYS */;
INSERT INTO `tiporechazo` VALUES (100,'otro','otro'),(101,'error en informacion','error en informacion'),(102,'no conforme','no conforme'),(103,'documento mal asignado','documento mal asignado');
/*!40000 ALTER TABLE `tiporechazo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `ID_USR` int(11) NOT NULL AUTO_INCREMENT,
  `EMAIL_USR` varchar(250) DEFAULT NULL,
  `CLAVE_USR` varchar(500) NOT NULL,
  `ESTADO_USR` smallint(6) NOT NULL,
  `DESCRIPCION_USR` varchar(250) DEFAULT NULL,
  `FECHACREACION_USR` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `FECHAMODIFICACION_USR` datetime NOT NULL,
  `NOMBRECONFIGFIRMA_USR` varchar(100) DEFAULT NULL,
  `PERMITLOGIN_USR` tinyint(1) NOT NULL,
  `FOTO_USR` longblob,
  `NOMBRE_USR` varchar(50) NOT NULL,
  `APELLIDO_USR` varchar(50) NOT NULL,
  `RUT_USR` varchar(10) NOT NULL,
  `DIRECCION_USR` varchar(250) NOT NULL,
  `TELEFONO_USR` varchar(20) NOT NULL,
  PRIMARY KEY (`ID_USR`)
) ENGINE=InnoDB AUTO_INCREMENT=1024 DEFAULT CHARSET=utf8 COMMENT='Tabla que almacena los datos de un usuario, este usuario deb';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (100,'sys@sys.cl','NJV+m6NFWkqZ1yK0hpOsESO6Xbo=',1,'usuario de sistema','2016-03-30 11:13:19','2016-03-30 11:13:19',NULL,0,NULL,'system','system','00000010-0','Padre Mariano 210, Of. 201 Providencia','56228419800'),(200,'superadmin@superadmin.cl','NJV+m6NFWkqZ1yK0hpOsESO6Xbo=',1,'usario con privilegios elevados','2016-03-30 11:13:19','2016-03-30 11:13:19',NULL,1,NULL,'super administrado','del sistema','00000011-0','Padre Mariano 210, Of. 201 Providencia','56228419800'),(300,'admin@admin.cl','NJV+m6NFWkqZ1yK0hpOsESO6Xbo=',1,'usuario con privilegios para cliente','2016-03-30 11:13:19','2016-03-30 11:13:19',NULL,1,NULL,'administrador','de sistema','00000012-0','Padre Mariano 210, Of. 201 Providencia','56228419800'),(1000,'demo@demo.cl','NJV+m6NFWkqZ1yK0hpOsESO6Xbo=',1,'usuario demo','2016-03-30 11:13:19','2016-03-30 11:13:19','testing',1,NULL,'usuario','demostracion','99999999-0','Padre Mariano 210, Of. 201 Providencia','56227777777'),(1022,'edgardo.a.v@gmail.com','clave',1,'mi descripcion personal','2016-04-19 08:24:08','0001-01-01 00:00:00','firmaElectronica',1,NULL,'edgardo','vasquez','21268364-7','algun lugar','9999');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario_rol`
--

DROP TABLE IF EXISTS `usuario_rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario_rol` (
  `ID_USRROL` char(10) NOT NULL,
  `ID_ROL` int(11) DEFAULT NULL,
  `ID_USR` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID_USRROL`),
  KEY `FK_REFERENCE_28` (`ID_ROL`),
  KEY `FK_REFERENCE_29` (`ID_USR`),
  CONSTRAINT `FK_REFERENCE_28` FOREIGN KEY (`ID_ROL`) REFERENCES `rol` (`ID_ROL`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_REFERENCE_29` FOREIGN KEY (`ID_USR`) REFERENCES `usuario` (`ID_USR`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tabla que enlaza a rol y usuarios para que cada usuario pued';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario_rol`
--

LOCK TABLES `usuario_rol` WRITE;
/*!40000 ALTER TABLE `usuario_rol` DISABLE KEYS */;
INSERT INTO `usuario_rol` VALUES ('100',100,100),('101',200,200),('102',300,300),('103',400,300),('104',401,300),('105',402,300),('106',500,300),('107',600,300),('108',400,1000),('109',401,1000),('110',402,1000),('111',500,1000),('112',600,1000);
/*!40000 ALTER TABLE `usuario_rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'websigner_dev'
--

--
-- Dumping routines for database 'websigner_dev'
--
/*!50003 DROP PROCEDURE IF EXISTS `spIns_User` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`websigner_dev`@`%` PROCEDURE `spIns_User`(
IN	EMAIL  varchar(50), 
IN	CLAVE varchar(500), 
IN	ESTADO boolean, 
IN	DESCRIPCION  varchar(250), 
IN	NOMBRECONFIGFIRMA  varchar(100), 
IN	PERMITLOGIN boolean, 
IN	NOMBRE varchar(50), 
IN	APELLIDO varchar(50), 
IN	RUT varchar(10), 
IN	DIRECCION varchar(250), 
IN	TELEFONO  varchar(20)
)
BEGIN
	DECLARE existenciaUser int;##Almacena la exitencia del usuario en caso de que exista	
    DECLARE idUsuario int;##variable que rescata a el id del usuario insertado y lo utiliza para insertar los roles correspondientes	
    
    ##Para realizar el ROLLBACK en MySQL Procedimiento Almacenado, se debe declarar la salida de controlador en el procedimiento almacenado.
    ##sqlexception se ejecutará cuando hay cualquier error se produce durante la ejecución de la consulta
	#DECLARE exit handler for sqlexception
	#BEGIN
		-- ERROR
		#SELECT 'FALSE' AS exito;
        #SELECT 'Problemas al insertar el registro "sqlexception"' result;
		#ROLLBACK;
	#END;
	##sqlwarning se ejecutará cuando se produce la advertencia de Procedimiento Almacenado en MySQL
	#DECLARE exit handler for sqlwarning
		#BEGIN
		-- WARNING
		#SELECT 'FALSE' AS exito;
        #SELECT 'Problemas al insertar el registro "sqlwarning"' result;
		#ROLLBACK;
	#END;	
    
	###############Inicia La Ejecuacion###############################
    SET SQL_SAFE_UPDATES = 0;
    START TRANSACTION;	
    select count(*) INTO existenciaUser from usuario where RUT_USR = RUT;
	IF existenciaUser = 0 THEN #Insercion de los datos en caso de que no se encuentre en la bbdd      			
		INSERT INTO `usuario` 
		(
			`EMAIL_USR`, 
			`CLAVE_USR`, 
			`ESTADO_USR`, 
			`DESCRIPCION_USR`, 
			`FECHAMODIFICACION_USR`, 
			`NOMBRECONFIGFIRMA_USR`, 
			`PERMITLOGIN_USR`, 
			`NOMBRE_USR`, 
			`APELLIDO_USR`, 
			`RUT_USR`, 
			`DIRECCION_USR`, 
			`TELEFONO_USR`
		) 
		VALUES (
			EMAIL, 
			CLAVE, 
			ESTADO, 
			DESCRIPCION, 
			NOW(), 
			NOMBRECONFIGFIRMA, 
			PERMITLOGIN, 
			NOMBRE, 
			APELLIDO, 
			RUT, 
			DIRECCION, 
			TELEFONO
		);
		SELECT @@identity INTO idUsuario;
        COMMIT;
        #SELECT 'TRUE'  AS exito;        
		select 'Usuario registrado con exito' result, idUsuario idusr;  
	ELSE #Actualizacion de los datos en caso de que se encuentre en la bbdd		
		SELECT 'El usuario ya se encuentra registrado' result, 0 idusr; 
    END IF;
		SET SQL_SAFE_UPDATES = 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spMod_User` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`websigner_dev`@`%` PROCEDURE `spMod_User`(
	IN	RUT varchar(10), 
	IN	EMAIL  varchar(50), 
	IN	CLAVE varchar(500), 
	IN	ESTADO boolean, 
	IN	DESCRIPCION  varchar(250), 
	IN	NOMBRECONFIGFIRMA  varchar(100), 
	IN	PERMITLOGIN boolean, 
	IN	NOMBRE varchar(50), 
	IN	APELLIDO varchar(50), 
	IN	DIRECCION varchar(250), 
	IN	TELEFONO  varchar(20)
)
BEGIN
    DECLARE idUsuario int;##variable que rescata a el id del usuario insertado y lo utiliza para insertar los roles correspondientes	
    
	###############Inicia La Ejecuacion###############################
    SET SQL_SAFE_UPDATES = 0;
    START TRANSACTION;	
    select ID_USR INTO idUsuario  from usuario where RUT_USR = RUT;  
    
    IF idUsuario IS NOT NULL THEN #Insercion de los datos en caso de que no se encuentre en la bbdd		
        UPDATE `usuario` SET 
			`EMAIL_USR`=EMAIL, 
			`CLAVE_USR`=CLAVE, 
			`ESTADO_USR`=ESTADO, 
			`DESCRIPCION_USR`=DESCRIPCION, 
			`FECHAMODIFICACION_USR`=NOW(), 
			`NOMBRECONFIGFIRMA_USR`=NOMBRECONFIGFIRMA, 
			`PERMITLOGIN_USR`=PERMITLOGIN, 
			`NOMBRE_USR`=NOMBRE, 
			`APELLIDO_USR`=APELLIDO, 
			`RUT_USR`=RUT, 
			`DIRECCION_USR`=DIRECCION, 
			`TELEFONO_USR`=TELEFONO 
        WHERE 
			`ID_USR`=idUsuario; 
        COMMIT;
        #SELECT 'TRUE'  AS exito;        
		select 'Usuario se actualizo con exito' result, idUsuario idusr, RUT rutusr;  
	ELSE #Actualizacion de los datos en caso de que se encuentre en la bbdd		
		SELECT 'El usuario no se encuentra registrado' result, idUsuario idusr, RUT rutusr; 
    END IF;
		SET SQL_SAFE_UPDATES = 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spRec_UserPer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`websigner_dev`@`%` PROCEDURE `spRec_UserPer`(in findBy varchar(20), in toFind1 varchar(250), in toFind2 varchar(250))
BEGIN
	case findBy
		when 'all' then SELECT * FROM usuario, persona where usuario.ID_PER = persona.ID_PER;
		when 'byIdPer' then SELECT * FROM usuario, persona where usuario.ID_PER = persona.ID_PER  and persona.id_per = toFind1;
        when 'byIdUsr' then SELECT * FROM usuario, persona where usuario.ID_PER = persona.ID_PER  and usuario.id_usr = toFind1;
		when 'byFistName' then SELECT * FROM usuario, persona where usuario.ID_PER = persona.ID_PER and persona.nombres_per LIKE CONCAT(toFind1,'%');
        when 'byLastName' then SELECT * FROM usuario, persona where usuario.ID_PER = persona.ID_PER and persona.apellidos_per LIKE CONCAT(toFind1,'%');
		when 'byRut' then SELECT * FROM usuario, persona where usuario.ID_PER = persona.ID_PER  and persona.RUT_PER LIKE CONCAT(toFind1,'%');
		when 'byEmail' then SELECT * FROM usuario, persona where usuario.ID_PER = persona.ID_PER and usuario.EMAIL_USR LIKE CONCAT(toFind1,'%');
        when 'byEmailPass' then	SELECT * FROM usuario, persona where usuario.ID_PER = persona.ID_PER AND usuario.EMAIL_USR = toFind1 AND usuario.CLAVE_USR = toFind2;  
    end case;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `spRec_UserRol` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`websigner_dev`@`%` PROCEDURE `spRec_UserRol`(in findBy varchar(20), in toFind1 varchar(250))
BEGIN
	case findBy
		when 'byEmail' then select rol.ID_ROL, rol.NOMBRE_ROL,rol.ESTADO_ROL,rol.DESCRIPCION_ROL, usuario.EMAIL_USR from usuario,usuario_rol,rol where usuario.ID_USR = usuario_rol.ID_USR and rol.ID_ROL = usuario_rol.ID_ROL and usuario.EMAIL_USR = toFind1;		
        when 'byIdUsr' then select rol.ID_ROL, rol.NOMBRE_ROL,rol.ESTADO_ROL,rol.DESCRIPCION_ROL, usuario.EMAIL_USR from usuario,usuario_rol,rol where usuario.ID_USR = usuario_rol.ID_USR and rol.ID_ROL = usuario_rol.ID_ROL and usuario.id_usr = toFind1;		
        when 'byIdPer' then select rol.ID_ROL, rol.NOMBRE_ROL,rol.ESTADO_ROL,rol.DESCRIPCION_ROL, usuario.EMAIL_USR from usuario,usuario_rol,rol where usuario.ID_USR = usuario_rol.ID_USR and rol.ID_ROL = usuario_rol.ID_ROL and persona.id_per = toFind1;		
    end case;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-19 16:36:35
