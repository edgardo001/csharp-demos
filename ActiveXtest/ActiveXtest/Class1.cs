﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace ActiveXtest{

    [ComVisibleAttribute(true)]  //Deja la clase visible para COM
    //EL UID puede ser generado en "Herramientas/Crear GUID/Opcion 2 (DEFINE_GUID)/Boton COPIAR"
    [Guid("099A5E1F-0A22-4A5B-86C8-0881CD504E65")] //GUID que generamos, Identificador de la Libreria
    [ProgId("ActiveXtest.Class")] //Identificador para poder Acceder a esta clase desde el exterior(Por lo visto no es necesario usar el mismo nombre de la clase)
    //Ver Final del codigo para implementar con HTML y JAVASCRIPT
    
    /*  Para Pruebas de uso rapido durante el desarrollo ir a 
     *  Explorador de Soluciones/Proyecto(No solucion)
     *  Click derecho, Propiedades
     *  Pestaña Generar y Marcar "Registrar para interoperabilidad COM , por ultimo "F6" o Generar Solucion
     *  Los pasos anteriores Generaran la DLL y la registraran automaticamente en el registro de windows
     *  **/

    /**
     * Visual Studio 2010 - 
     * Para crea un instalador,
     * realizar los pasos inversos a "Pruebas de uso rapido durante el desarrollo" y
     * agregar un nuevo proyecto de instalacion en la solucion
     * y agregar el resultado de la solucion "dll" creada.
     * Importante es que la dll debe ser registrada en el registro de windows,
     * para esto ir a proyecto de instalacion creado y seleccionar "resultado principal de **nombre de proyecto***"
     * y en sus propiedades se debe modificar "Register" y seleccionar "vsdrpCOM".
     * Aora podra ser instalado y auotoregistrado en windows
     * **/
    
    public class Class1
    {
        /// <summary>
        /// Metodo que solo retorna un String
        /// </summary>
        /// <returns></returns>
        public string Hola()
        {
            return "Hola Mundo desde .NET";
        }

        /// <summary>
        /// Metodo que recive una cadena y la retorna concatenandola a otra
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public string test(String t)
        {
            return "Hola Mundo desde .NET, tu variable es: " + t;
        }
    }

    /**
     * Para el Uso en IIS y explorer:
     * Habilitar en explorer "Opciones de internet/seguridad/Nivel personalizado/Intenet,Internet Local y Sitios de confianza"
     * Marcar Opciones "Controles y complementos de ActiveX/Inicializar y generar script de los controles Activex no marcados como seguros para script"
     * Debe marcar la opcion en "Intenet,Internet Local y Sitios de confianza" para evitar problemas durante pruebas
     * **/

    //El siguiente Script puede ser copiado(Sin lineas de comentarios) directo en un archivo HTML y ejecutar de forma local 
    //No es necesario inicializar en IIS
    //<html>
    //    <head>
    //        <script type="text/javascript">
    //            /* Creando una instancia de ActiveXObject para poder acceder a nuestra libreria */
    //            var obj = new ActiveXObject("ActiveXtest.Class"); /* ProgId de la Clase */
    //            //var obj = new ActiveXObject("099A5E1F-0A22-4A5B-86C8-0881CD504E65"); /* ProgId de la Clase */
    //            alert("Inicio");
    //            alert(obj.Hola()); /* llamando al metodo Hola de la DLL */
    //            alert(obj.test("Edgardo Vasquez")); /* llamando al metodo Hola de la DLL */
    //            alert("Fin");
    //        </script>
    //    </head>
    //    <body>
    //        <!-- Cuerpo -->
    //    </body>
    //</html>

}
