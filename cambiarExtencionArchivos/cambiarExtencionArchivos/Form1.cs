﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace cambiarExtencionArchivos
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string[] ficheros = null;

        private void button1_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = folderBrowserDialog1.SelectedPath;
                string directorio = folderBrowserDialog1.SelectedPath;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text.Equals(String.Empty))
                {
                    MessageBox.Show("Debe seleccionar una ruta a convertir", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }


                ficheros = Directory.GetFiles(textBox1.Text, "*.*");
                if (MessageBox.Show("¿Desea agregar la extencion '.cbr' y/o 'cbz' a la siguiente cantidad de ficheros: "+ ficheros.Length, "Confirmacion", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {                    
                    if (ficheros != null)
                    {
                        foreach (var item in ficheros)
                        {
                            if (Path.GetExtension(item).Equals(".zip"))
                            {
                                File.Move(item, item + ".cbz");
                            }
                            if (Path.GetExtension(item).Equals(".rar"))
                            {
                                File.Move(item, item + ".cbr");
                            }
                        }
                        MessageBox.Show("Se ha terminado al convercion", "Informacion", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        System.Diagnostics.Process.Start(textBox1.Text);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR: " + ex, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

    }
}
