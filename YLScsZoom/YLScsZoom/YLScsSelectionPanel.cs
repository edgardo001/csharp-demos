////////////////////////////////////////////////////////////////
// Copyright (C) YLScs, 2008              //
// yls_cs@yahoo.com                           //
// FREE for any non commercial usage //
////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace YLScsZoom
{
    public partial class YLScsSelectionPanel : YLScsPanel 
    {
        public YLScsSelectionPanel()
        {
            InitializeComponent();
        }
        Selection selection;

        public Selection Selection
        {
            get { return selection; }
            set { selection = value; Invalidate(); }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            if (selection != null)
                selection.Draw(e.Graphics);
        }
    }
}
