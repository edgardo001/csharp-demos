////////////////////////////////////////////////////////////////
// Copyright (C) YLScs, 2008              //
// yls_cs@yahoo.com                           //
// FREE for any non commercial usage //
////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace YLScsZoom
{
    public partial class YLScsPanel : UserControl
    {
        public YLScsPanel()
        {
            InitializeComponent();
       // Set the value of the double-buffering style bits to true.
            this.SetStyle(ControlStyles.AllPaintingInWmPaint |
              ControlStyles.UserPaint | ControlStyles.ResizeRedraw |
              ControlStyles.UserPaint | ControlStyles.DoubleBuffer, true);
        }

        int viewRectWidth, viewRectHeight; // view window width and height

        float zoom = 1.0f;
        public float Zoom
        {
            get { return zoom; }
            set
            {
                if (value < 0.001f) value = 0.001f;
                zoom = value;

                displayScrollbar();
                setScrollbarValues();
                Invalidate();
            }
        }

        Size canvasSize = new Size(60, 40);
        public Size CanvasSize
        {
            get { return canvasSize; }
            set
            {
                canvasSize = value;
                displayScrollbar();
                setScrollbarValues();
                Invalidate();
            }
        }

        Bitmap image;
        public Bitmap Image
        {
            get { return image; }
            set
            {
                image = value;
                displayScrollbar();
                setScrollbarValues();
                Invalidate();
            }
        }

        InterpolationMode interMode = InterpolationMode.HighQualityBilinear;
        public InterpolationMode InterpolationMode
        {
            get { return interMode; }
            set { interMode = value; }
        }

        public Point ConvertControlPointToCanvas(Point point)
        {
            Point pt = new Point();
            if (viewRectWidth > canvasSize.Width * zoom)
            {
                pt.X = (int)((float)(point.X - viewRectWidth / 2 + canvasSize.Width * zoom / 2f) / zoom);
                pt.X = Math.Min(Math.Max(pt.X, 1), canvasSize.Width - 1);
            }
            else pt.X = (int)((float)(point.X + hScrollBar1.Value) / zoom);
            if (viewRectHeight > canvasSize.Height * zoom)
            {
                pt.Y = (int)((float)(point.Y - viewRectHeight / 2 + canvasSize.Height * zoom / 2f) / zoom);
                pt.Y = Math.Max(Math.Min(pt.Y, canvasSize.Height - 1), 1);
            }
            else pt.Y = (int)((float)(point.Y + vScrollBar1.Value) / zoom);
            return pt;
        }

        public Point ConvertCanvasPointToControl(Point point)
        {
            float xOffset = viewRectWidth > canvasSize.Width * zoom ? (viewRectWidth - canvasSize.Width * zoom) / 2f :
                -hScrollBar1.Value;
            float yOffset = viewRectHeight > canvasSize.Height * zoom ? (viewRectHeight - canvasSize.Height * zoom) / 2f :
                -vScrollBar1.Value;
            Matrix mxCanvastoContol = new Matrix();
            mxCanvastoContol.Scale(zoom, zoom);
            mxCanvastoContol.Translate(xOffset, yOffset, MatrixOrder.Append);
            Point[] pts = new Point[] { point };
            mxCanvastoContol.TransformPoints(pts);
            return pts[0];
        }

        protected override void OnLoad(EventArgs e)
        {
            displayScrollbar();
            setScrollbarValues();
            base.OnLoad(e);
        }

        protected override void OnResize(EventArgs e)
        {
            displayScrollbar();
            setScrollbarValues();
            base.OnResize(e);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            Graphics g = e.Graphics;
            g.InterpolationMode = interMode;
            float xOffset = viewRectWidth > canvasSize.Width * zoom ? (viewRectWidth - canvasSize.Width * zoom) / 2f :
                 -hScrollBar1.Value;
            float yOffset = viewRectHeight > canvasSize.Height * zoom ? (viewRectHeight - canvasSize.Height * zoom) / 2f :
                -vScrollBar1.Value;
            Matrix mxCanvastoContol = new Matrix();
            mxCanvastoContol.Scale(zoom, zoom);
            mxCanvastoContol.Translate(xOffset, yOffset, MatrixOrder.Append);
            g.Transform = mxCanvastoContol;
            //draw image
            if (image != null)
            {
                Rectangle srcRect;
                Point pt = new Point((int)(hScrollBar1.Value / zoom), (int)(vScrollBar1.Value / zoom));
                if (canvasSize.Width * zoom < viewRectWidth && canvasSize.Height * zoom < viewRectHeight)
                    srcRect = new Rectangle(0, 0, canvasSize.Width, canvasSize.Height);  // view all image
                else srcRect = new Rectangle(pt, new Size((int)(viewRectWidth / zoom), (int)(viewRectHeight / zoom))); // view a portion of image


                g.DrawImage(image, srcRect, srcRect, GraphicsUnit.Pixel);
            }
        }

        private void displayScrollbar()
        {
            viewRectWidth = this.Width;
            viewRectHeight = this.Height;

            if (image != null) canvasSize = image.Size;

            // If the zoomed image is wider than view window, show the HScrollBar and adjust the view window
            if (viewRectWidth > canvasSize.Width * zoom)
            {
                hScrollBar1.Visible = false;
                viewRectHeight = Height;
            }
            else
            {
                hScrollBar1.Visible = true;
                viewRectHeight = Height - hScrollBar1.Height;
            }

            // If the zoomed image is taller than view window, show the VScrollBar and adjust the view window
            if (viewRectHeight > canvasSize.Height * zoom)
            {
                vScrollBar1.Visible = false;
                viewRectWidth = Width;
            }
            else
            {
                vScrollBar1.Visible = true;
                viewRectWidth = Width - vScrollBar1.Width;
            }

            // Set up scrollbars
            hScrollBar1.Location = new Point(0, Height - hScrollBar1.Height);
            hScrollBar1.Width = viewRectWidth;
            vScrollBar1.Location = new Point(Width - vScrollBar1.Width, 0);
            vScrollBar1.Height = viewRectHeight;

            // set label1 to cover the coner 
            label1.Size = new Size(vScrollBar1.Width, hScrollBar1.Height);
            label1.Location = new Point(Width - vScrollBar1.Width, Height - hScrollBar1.Height);
            if (vScrollBar1.Visible && hScrollBar1.Visible)
                label1.Visible = true;
            else label1.Visible = false;
        }

        private void setScrollbarValues()
        {
            // Set the Maximum, Minimum, LargeChange and SmallChange properties.
            this.vScrollBar1.Minimum = 0;
            this.hScrollBar1.Minimum = 0;

            // If the offset does not make the Maximum less than zero, set its value. 
            if ((canvasSize.Width * zoom - viewRectWidth) > 0)
            {
                this.hScrollBar1.Maximum = (int)(canvasSize.Width * zoom) - viewRectWidth;
            }
            // If the VScrollBar is visible, adjust the Maximum of the 
            // HSCrollBar to account for the width of the VScrollBar.  
            if (this.vScrollBar1.Visible)
            {
                this.hScrollBar1.Maximum += this.vScrollBar1.Width;
            }
            this.hScrollBar1.LargeChange = this.hScrollBar1.Maximum / 10;
            this.hScrollBar1.SmallChange = this.hScrollBar1.Maximum / 20;

            // Adjust the Maximum value to make the raw Maximum value 
            // attainable by user interaction.
            this.hScrollBar1.Maximum += this.hScrollBar1.LargeChange;

            // If the offset does not make the Maximum less than zero, set its value.    
            if ((canvasSize.Height * zoom - viewRectHeight) > 0)
            {
                this.vScrollBar1.Maximum = (int)(canvasSize.Height * zoom) - viewRectHeight;
            }

            // If the HScrollBar is visible, adjust the Maximum of the 
            // VSCrollBar to account for the width of the HScrollBar.
            if (this.hScrollBar1.Visible)
            {
                this.vScrollBar1.Maximum += this.hScrollBar1.Height;
            }
            this.vScrollBar1.LargeChange = this.vScrollBar1.Maximum / 10;
            this.vScrollBar1.SmallChange = this.vScrollBar1.Maximum / 20;

            // Adjust the Maximum value to make the raw Maximum value 
            // attainable by user interaction.
            this.vScrollBar1.Maximum += this.vScrollBar1.LargeChange;
        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            this.Invalidate();
        }
    }
}
