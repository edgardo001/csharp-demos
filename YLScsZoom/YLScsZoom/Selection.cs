////////////////////////////////////////////////////////////////
// Copyright (C) YLScs, 2008              //
// yls_cs@yahoo.com                           //
// FREE for any non commercial usage //
////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;


namespace YLScsZoom
{
    public class Selection
    {
        Color lineColor = Color.Black;
        float lineWidth = 1.0f;

        Point location = new Point(0, 0);
        Size size = new Size(0, 0);

        public Color LineColor
        {
            get { return lineColor; }
            set { lineColor = value; }
        }

        public float LineWidth
        {
            get { return lineWidth; }
            set { lineWidth = value; }
        }

        public Size Size
        {
            get { return size; }
            set { size = value; }
        }

        public Point Location
        {
            get { return location; }
            set { location = value; }
        }

        public void Draw(Graphics g)
        {
            Pen p = new Pen(lineColor, lineWidth);
            g.DrawRectangle(p, new Rectangle(location, size));
            p.Dispose();
        }

        public virtual bool isHitting(Point pt)
        {
            Rectangle r = new Rectangle(location, size);
            if (r.Contains(pt))
                return true;
            else return false;
        }
    }
}