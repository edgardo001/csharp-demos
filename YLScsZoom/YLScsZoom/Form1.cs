////////////////////////////////////////////////////////////////
// Copyright (C) YLScs, 2008              //
// yls_cs@yahoo.com                           //
// FREE for any non commercial usage //
////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using YLScsLib.Imaging;

namespace YLScsZoom
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Bitmap srcBmp, pic;
        Selection selection = new Selection();
        bool isHit = false;
        int nw, nh;
        Point newLocation = new Point(0, 0);
        Size newSize;
        float zoom1 = 1f;
        float zoom2 = 1.0f, sharpness = 0f;

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            dlg.Filter = "Image files (*.jpg,*.bmp,*.gif,*.png)|*.jpg;*.bmp;*.gif;*.png|All files (*.*)|*.*";

            if (dlg.ShowDialog() != DialogResult.OK)
            {
                return; // Return if no useful file has been selected.
            }

            // Now load it into the bitmap object...
            string strFileName = dlg.FileName;
            try
            {
                srcBmp = new Bitmap(strFileName);
            }
            catch
            {
                MessageBox.Show("Wrong file style, please select image file.");
            }
            if (srcBmp != null)
            {
                zoom1 = srcBmp.Width > srcBmp.Height ? 399f / (float)srcBmp.Width : 399f / (float)srcBmp.Height;
                zoom1 = Math.Min(1f, zoom1);
                YLScsPanel1.Zoom = zoom1;
                setSelection();
                YLScsPanel1.Image = srcBmp;
                showPanel2();
            }
        }

        private void setSelection()
        {
            if (srcBmp == null) return;
            nw = Math.Min(srcBmp.Width, (int)(399f / zoom2));
            nh = Math.Min(srcBmp.Height, (int)(399f / zoom2));
            newSize = new Size(nw, nh);
            selection.LineColor = Color.White;
            selection.LineWidth = 0.01f;
            selection.Location = newLocation;
            selection.Size = newSize;
            YLScsPanel1.Selection = selection;
        }

        private void showPanel2()
        {
            if (srcBmp == null) return;
            pic = (Bitmap)srcBmp.Clone(new Rectangle(newLocation, newSize), srcBmp.PixelFormat);
            YLScsPanel2.Image = YLScsLib.Imaging.YLScsZoom.Apply(pic, zoom2, sharpness);
            pic = null;
            StatusLabel.Text = "Zoom Factor: " + zoom2.ToString() + "; Sharpness Factor: " + sharpness.ToString() +
                "; Image Size: " + ((int)((float)srcBmp.Width * zoom2)).ToString() + "Pixels X " +
                ((int)((float)srcBmp.Height * zoom2)).ToString() + "Pixels.";
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            if (srcBmp == null) return;
            zoom2 = (float)trackBar1.Value * 0.1f;
            setSelection();
            showPanel2();
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            sharpness = (float)trackBar2.Value * 0.1f;
            showPanel2();
        }

        private void YLScsPanel1_MouseDown(object sender, MouseEventArgs e)
        {
            Point pt = new Point(e.X, e.Y);
            pt = YLScsPanel1.ConvertControlPointToCanvas(pt);
            if (selection.isHitting(pt) && e.Button == MouseButtons.Left && srcBmp != null)
            {
                isHit = true;
               YLScsPanel1.Cursor = Cursors.Hand;
            }
        }

        private void YLScsPanel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (isHit)
            {
                YLScsPanel1.Cursor = Cursors.Hand;
                Point pt = new Point(e.X, e.Y);
                pt = YLScsPanel1.ConvertControlPointToCanvas(pt);
                newLocation.X = Math.Max(0, Math.Min(pt.X, srcBmp.Width - newSize.Width));
                newLocation.Y = Math.Max(0, Math.Min(pt.Y, srcBmp.Height - newSize.Height));
                selection.Location = newLocation;
                YLScsPanel1.Selection = selection;
            }
        }

        private void YLScsPanel1_MouseUp(object sender, MouseEventArgs e)
        {
            if (isHit)
            {
                isHit = false;
                YLScsPanel1.Cursor = Cursors.Default;
                showPanel2();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (srcBmp == null) return;
            StatusLabel.Text = "It takes time, please wait ...";

            progressBar1.Maximum = 15;
            progressBar1.Value = 0;
            // Displays a SaveFileDialog so the user can save the Image
            // assigned to Button2.
            SaveFileDialog d = new SaveFileDialog();
            d.Filter = "JPEG Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif|Png Image|*.png";
            d.Title = "Save an Image File";
            d.ShowDialog();
            progressBar1.Visible = true;

            nw = (int)((float)srcBmp.Width * zoom2);
            nh = (int)((float)srcBmp.Height * zoom2);
            YLScsLib.Imaging.ChannelBytes ch = YLScsLib.Imaging.ImageData.GetChannelBytes(srcBmp);
            progressBar1.Value = 2;
            ch.R = YLScsLib.Imaging.YLScsZoom.Apply(ch.R, nw, nh, sharpness);
            progressBar1.Value = 4;
            ch.B = YLScsLib.Imaging.YLScsZoom.Apply(ch.B, nw, nh, sharpness);
            progressBar1.Value = 6;
            ch.G = YLScsLib.Imaging.YLScsZoom.Apply(ch.G, nw, nh, sharpness);
            progressBar1.Value = 8;
            ch.A = YLScsLib.Imaging.YLScsZoom.Apply(ch.A, nw, nh, sharpness);
            progressBar1.Value = 10;
            srcBmp = YLScsLib.Imaging.ImageData.ConvertChannelbytesToBitmap(ch, nw, nh);
            progressBar1.Value = 12;

            // If the file name is not an empty string open it for saving.
            if (d.FileName != "")
            {
                // Saves the Image via a FileStream created by the OpenFile method.
                System.IO.FileStream fs =
                   (System.IO.FileStream)d.OpenFile();
                // Saves the Image in the appropriate ImageFormat based upon the
                // File type selected in the dialog box.
                // NOTE that the FilterIndex property is one-based.
                switch (d.FilterIndex)
                {
                    case 1:
                        srcBmp.Save(fs,
                           System.Drawing.Imaging.ImageFormat.Jpeg);
                        break;

                    case 2:
                        srcBmp.Save(fs,
                           System.Drawing.Imaging.ImageFormat.Bmp);
                        break;

                    case 3:
                        srcBmp.Save(fs,
                           System.Drawing.Imaging.ImageFormat.Gif);
                        break;

                    case 4:
                        srcBmp.Save(fs,System.Drawing.Imaging.ImageFormat.Png);
                        break;
                }

                this.Close();
            }
        }
    }
}