﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Diagnostics;

namespace Demo2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(Form1_FormClosing);
        }

        void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Trace.TraceInformation("Aplicación Cerrada");
            Trace.Flush();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                Trace.TraceInformation("Aplicación Iniciada");
                Trace.Flush();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, Application.ProductName);
            }
        }

        private void btnTest1_Click(object sender, EventArgs e)
        {
            int a = 2;
            int b = 3;

            int ret = a + b;

            Trace.TraceInformation("btnTest1 Ejecutado. Resultado: " + ret);
            Trace.Flush();
            MessageBox.Show("Competado");
        }

        private void btnTest2_Click(object sender, EventArgs e)
        {
            int segundos = DateTime.Now.Second;

            Trace.TraceInformation("btnTest2 Ejecutado");
            Trace.Flush();

            if (segundos < 30)
                Trace.TraceWarning("El valor es menor de 30");
            else
                Trace.TraceWarning("El valor es mayor de 30");

            Trace.Flush();
            MessageBox.Show("Competado");
        }

        private void btnTest3_Click(object sender, EventArgs e)
        {
            int segundos = DateTime.Now.Second;
            int a = 0;
            int ret;

            Trace.TraceInformation("btnTest3 Ejecutado");

            try
            {
                if (segundos < 30)
                    Trace.TraceError("El valor es menor de 30");
                else
                    Trace.TraceError("El valor es mayor de 30");

                Trace.Flush();
                ret = segundos / a; // <---excepción segura (división entre 0)

                MessageBox.Show("Competado");
            }

            catch (Exception ex)
            {
                Trace.TraceError("@@@ Excepción en btnTest3(). ex.Message = " + ex.Message);
                Trace.Flush();
                // TODO. Tratar la excepción. De momento se muestra un messagebox
                MessageBox.Show(ex.Message, Application.ProductName);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Trace.TraceInformation("Cargando Form2...");
            Trace.Flush();
            Form2 f = new Form2();
            f.ShowDialog();
        }
    }
}
