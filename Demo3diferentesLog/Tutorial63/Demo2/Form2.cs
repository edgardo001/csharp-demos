﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Diagnostics;

namespace Demo2
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(Form2_FormClosing);
        }

        void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            Trace.TraceInformation("Formulario Cerrado");
            Trace.Flush();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            Trace.TraceInformation("Formulario Cargado");
            Trace.Flush();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Trace.TraceInformation("button1_Click pulsado");
            Trace.Flush();
            Trace.TraceWarning("Mensaje 2");
            Trace.Flush();
            Trace.TraceError("Mensaje 3");
            Trace.Flush();
            MessageBox.Show("Completado");
        }
    }
}
