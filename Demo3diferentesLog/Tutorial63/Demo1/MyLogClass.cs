﻿using System;
using System.IO;

namespace Demo1
{
    public class MyLogClass
    {
        public enum LogLevel
        {
            Info,
            Warn,
            Error,
            Fatal
        }

        const string LOG_FILE_NAME = "Demo1_Log.txt"; 
        private readonly string _folderSave;
        private readonly string _logName;

        public MyLogClass(string folderSave, string logName)
        {
            _folderSave = folderSave;
            _logName = logName;

            if (!_folderSave.EndsWith(@"\"))
                _folderSave += @"\";
            
            if (!Directory.Exists(_folderSave))
                throw new DirectoryNotFoundException("Directory Not Found");
        }

        public void Write(LogLevel logLevel, string dataToSave)
        {
            try
            {
                using (FileStream fs = new FileStream(_folderSave + LOG_FILE_NAME, FileMode.Append))
                {
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        string tmpDataToSave = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " ";
                        tmpDataToSave += logLevel.ToString().PadRight(6).ToUpper();
                        tmpDataToSave += _logName.PadRight(15) + " - ";
                        tmpDataToSave += dataToSave.Replace(Environment.NewLine, " ");
                        
                        sw.WriteLine(tmpDataToSave);
                    }
                }
            }
            catch (Exception)
            {
                // No hacer nada
            }
        }
    }
}
