﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Demo1
{
    public partial class Form1 : Form
    {
        private static MyLogClass Log;
        public Form1()
        {
            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(Form1_FormClosing);
        }

        void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Log.Write(MyLogClass.LogLevel.Info, "Aplicación Cerrada");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                Log = new MyLogClass(Application.StartupPath, "Form1");
                Log.Write(MyLogClass.LogLevel.Info, "Aplicación Iniciada");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message,Application.ProductName);
            }
        }

        private void btnTest1_Click(object sender, EventArgs e)
        {
            int a = 2;
            int b = 3;

            int ret = a + b;

            Log.Write(MyLogClass.LogLevel.Info, "btnTest1 Ejecutado. Resultado: " + ret);
            MessageBox.Show("Competado");
        }

        private void btnTest2_Click(object sender, EventArgs e)
        {
            int segundos = DateTime.Now.Second;

            Log.Write(MyLogClass.LogLevel.Info, "btnTest2 Ejecutado");

            if (segundos < 30)
                Log.Write(MyLogClass.LogLevel.Warn, "El valor es menor de 30");
            else
                Log.Write(MyLogClass.LogLevel.Warn, "El valor es mayor de 30");

            MessageBox.Show("Competado");
        }

        private void btnTest3_Click(object sender, EventArgs e)
        {
            int segundos = DateTime.Now.Second;
            int a = 0;
            int ret;

            Log.Write(MyLogClass.LogLevel.Info, "btnTest3 Ejecutado");

            try
            {
                if (segundos < 30)
                    Log.Write(MyLogClass.LogLevel.Error, "El valor es menor de 30");
                else
                    Log.Write(MyLogClass.LogLevel.Error, "El valor es mayor de 30");

                ret = segundos / a; // <---excepción segura (división entre 0)

                MessageBox.Show("Competado");
            }

            catch (Exception ex)
            {
                Log.Write(MyLogClass.LogLevel.Fatal, "@@@ Excepción en btnTest3(). ex.Message = " + ex.Message);
                // TODO. Tratar la excepción. De momento se muestra un messagebox
                MessageBox.Show(ex.Message, Application.ProductName);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Log.Write(MyLogClass.LogLevel.Info, "Cargando Form2...");
            Form2 f = new Form2();
            f.ShowDialog();
        }
    }
}
