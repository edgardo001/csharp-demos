﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Demo1
{
    public partial class Form2 : Form
    {
        private static MyLogClass Log;

        public Form2()
        {
            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(Form2_FormClosing);
        }

        void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            Log.Write(MyLogClass.LogLevel.Info, "Formulario Cerrado");
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            try
            {
                Log = new MyLogClass(Application.StartupPath, "Form2");
                Log.Write(MyLogClass.LogLevel.Info, "Formulario Cargado");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, Application.ProductName);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Log.Write(MyLogClass.LogLevel.Info, "button1_Click pulsado");
            Log.Write(MyLogClass.LogLevel.Warn, "Mensaje 2");
            Log.Write(MyLogClass.LogLevel.Error, "Mensaje 3");
            Log.Write(MyLogClass.LogLevel.Fatal, "Mensaje 4");
            MessageBox.Show("Completado");
        }
    }
}
