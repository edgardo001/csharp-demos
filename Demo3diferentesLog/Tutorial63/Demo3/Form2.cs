﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using log4net;

namespace Demo3
{
    public partial class Form2 : Form
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Form2));

        public Form2()
        {
            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(Form2_FormClosing);
        }

        void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            log.Info("Formulario Cerrado");
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            log.Info("Formulario Cargado");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            log.Info("button1_Click pulsado");
            log.Debug("Mensaje 1");
            log.Warn("Mensaje 2");
            log.Error("Mensaje 3");
            log.Fatal("Mensaje 4");
            MessageBox.Show("Completado");
        }
    }
}
