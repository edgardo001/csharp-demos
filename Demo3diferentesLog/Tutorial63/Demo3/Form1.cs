﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using log4net;

namespace Demo3
{
    public partial class Form1 : Form
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Form1));

        public Form1()
        {
            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(Form1_FormClosing);
        }

        void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Log.Info("Aplicacion Cerrada");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                Log.Info("Aplicacion Iniciada");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message, Application.ProductName);
            }
        }

        private void btnTest1_Click(object sender, EventArgs e)
        {
            int a = 2;
            int b = 3;

            int ret = a + b;

            Log.Info("btnTest1 Ejecutado. Resultado: " + ret);
            MessageBox.Show("Competado");
        }

        private void btnTest2_Click(object sender, EventArgs e)
        {
            int segundos = DateTime.Now.Second;

            Log.Info("btnTest2 Ejecutado");

            if (segundos < 30)
                Log.Warn("El valor es menor de 30");
            else
                Log.Warn("El valor es mayor de 30");

            MessageBox.Show("Competado");
        }

        private void btnTest3_Click(object sender, EventArgs e)
        {
            int segundos = DateTime.Now.Second;
            int a = 0;
            int ret;

            Log.Info("btnTest3 Ejecutado");

            try
            {
                if (segundos < 30)
                    Log.Error("El valor es menor de 30");
                else
                    Log.Error("El valor es mayor de 30");

                ret = segundos / a; // <---excepción segura (división entre 0)

                MessageBox.Show("Competado");
            }

            catch (Exception ex)
            {
                Log.Fatal("@@@ Excepcion en btnTest3(). ex.Message = " + ex.Message);
                // TODO. Tratar la excepción. De momento se muestra un messagebox
                MessageBox.Show(ex.Message, Application.ProductName);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Log.Info("Cargando Form2...");
            Form2 f = new Form2();
            f.ShowDialog();
        }
    }
}
