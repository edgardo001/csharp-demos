﻿using Fleck;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iWsConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var server = new WebSocketServer("ws://localhost:8181");

            server.Start(socket =>
            {
                socket.OnOpen = () => Console.WriteLine("Listening... Start a WebSocket client e.g. use HTML5+Javascript.");
                socket.OnClose = () => Console.WriteLine("Closed!");
                socket.OnMessage = message =>
                {
                    string msg = message.ToLower();

                    if (msg == "hi")
                    {
                        socket.Send("hi dude!");
                    }
                    else
                        if (msg == "age")
                        {
                            socket.Send("old enough!");
                        }
                        else
                            if (msg == "today")
                            {
                                socket.Send(DateTime.Now.ToString());
                            }
                            else
                                if (msg == "name")
                                {
                                    socket.Send("Fleck WebSocket Server");
                                }
                                else
                                {
                                    Console.WriteLine("Unknown command [" + message + "] received from the client");
                                }
                };
            });

            Console.ReadLine();

        }
    }
}
