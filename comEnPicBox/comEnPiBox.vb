Imports System.Math
Public Class comEnPiBox
    Inherits System.Windows.Forms.UserControl


#Region " Windows 窗体设计器生成的代码 "

    Public Sub New()
        MyBase.New()

        '该调用是 Windows 窗体设计器所必需的。
        InitializeComponent()

        '在 InitializeComponent() 调用之后添加任何初始化
        InstanceID = NextInstanceID
        NextInstanceID += 1
        ClassInstanceCount += 1

        Dim g As Graphics = PictureBox1.CreateGraphics
        Dim p As New Pen(Color.FromArgb(255, 0, 0, 0), 3)
        Dim p1 As New Pen(Color.FromArgb(255, 0, 0, 0), 3)
        Dim rec As New Rectangle(0, 0, 0, 0)
        p.DashStyle = Drawing.Drawing2D.DashStyle.Dash
        SelRect = rec

        PreGraph = g
        PenDash = p
        PenSolid = p1
        'PictureBox1.SizeMode = PictureBoxSizeMode.AutoSize


    End Sub

    Protected Overrides Sub Finalize()
        ClassInstanceCount -= 1
    End Sub

    'UserControl1 重写 dispose 以清理组件列表。
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意：以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改此过程。
    '不要使用代码编辑器修改它。
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Menu_Popup As System.Windows.Forms.ContextMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MarkWide As System.Windows.Forms.MenuItem
    Friend WithEvents MarkMid As System.Windows.Forms.MenuItem
    Friend WithEvents MarkThin As System.Windows.Forms.MenuItem
    Friend WithEvents MarkRed As System.Windows.Forms.MenuItem
    Friend WithEvents MarkWhite As System.Windows.Forms.MenuItem
    Friend WithEvents MarkBlack As System.Windows.Forms.MenuItem
    Friend WithEvents ClearMark As System.Windows.Forms.MenuItem
    Friend WithEvents ConfirmSelection As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Menu_Popup = New System.Windows.Forms.ContextMenu()
        Me.ClearMark = New System.Windows.Forms.MenuItem()
        Me.ConfirmSelection = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MarkWide = New System.Windows.Forms.MenuItem()
        Me.MarkMid = New System.Windows.Forms.MenuItem()
        Me.MarkThin = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MarkRed = New System.Windows.Forms.MenuItem()
        Me.MarkWhite = New System.Windows.Forms.MenuItem()
        Me.MarkBlack = New System.Windows.Forms.MenuItem()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = (((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.PictureBox1.Location = New System.Drawing.Point(0, 8)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(114, 71)
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Menu_Popup
        '
        Me.Menu_Popup.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.ClearMark, Me.ConfirmSelection, Me.MenuItem1, Me.MenuItem2})
        '
        'ClearMark
        '
        Me.ClearMark.Index = 0
        Me.ClearMark.Text = "Clear Mark"
        '
        'ConfirmSelection
        '
        Me.ConfirmSelection.Index = 1
        Me.ConfirmSelection.Text = "Confirm Selection"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 2
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MarkWide, Me.MarkMid, Me.MarkThin})
        Me.MenuItem1.Text = "Mark Width"
        '
        'MarkWide
        '
        Me.MarkWide.Index = 0
        Me.MarkWide.Text = "Wide"
        '
        'MarkMid
        '
        Me.MarkMid.Index = 1
        Me.MarkMid.Text = "Middle"
        '
        'MarkThin
        '
        Me.MarkThin.Index = 2
        Me.MarkThin.Text = "Thin"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 3
        Me.MenuItem2.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MarkRed, Me.MarkWhite, Me.MarkBlack})
        Me.MenuItem2.Text = "Mark Color"
        '
        'MarkRed
        '
        Me.MarkRed.Index = 0
        Me.MarkRed.Text = "Red"
        '
        'MarkWhite
        '
        Me.MarkWhite.Index = 1
        Me.MarkWhite.Text = "White"
        '
        'MarkBlack
        '
        Me.MarkBlack.Index = 2
        Me.MarkBlack.Text = "Black"
        '
        'comEnPiBox
        '
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.PictureBox1})
        Me.Name = "comEnPiBox"
        Me.Size = New System.Drawing.Size(120, 64)
        Me.ResumeLayout(False)

    End Sub

#End Region
    'assign ID to each instance
    Public ReadOnly InstanceID As Integer
    Private Shared NextInstanceID As Integer = 0
    Private Shared ClassInstanceCount As Long = 0

    Private iStartPos As Point
    Private iEndPos As Point
    Private OriginalImage As Bitmap

    Private SelectedImage As Bitmap

    Private ShowHeight As Double
    Private ShowWidth As Double
    Private PreGraph As Graphics

    'GDI+ components for marking the image
    Private PenDash As Pen
    Private PenSolid As Pen
    Private SelRect As Rectangle
    Private bufferrec As Rectangle

    Public Shared ReadOnly Property InstanceCount() As Long
        Get
            Return ClassInstanceCount
        End Get
    End Property

    Public Property SourceImage() As Bitmap
        Get
            Return OriginalImage
        End Get
        Set(ByVal inputImg As Bitmap)
            Dim drawImg As Bitmap
            OriginalImage = inputImg
            ShowHeight = PictureBox1.Height
            ShowWidth = PictureBox1.Width
            drawImg = FormPreview(OriginalImage, PictureBox1.Height, PictureBox1.Width)
            PictureBox1.Image = drawImg
            
        End Set
    End Property

    Public ReadOnly Property SelectArea() As Bitmap
        Get
            Return SelectedImage
        End Get
    End Property

   
    'this function forma a resized image according to the size of picturebox1
    Private Function FormPreview(ByRef srcBMP As Bitmap, ByVal expHeight As Integer, _
                                    ByVal expWidth As Integer) As Bitmap
        'setup a return image 
        Dim previewImg As New Bitmap(CInt(expWidth), CInt(expHeight))

        Dim gr_dest As Graphics = Graphics.FromImage(previewImg)

        If srcBMP Is Nothing Then
            Exit Function
        Else
            gr_dest.DrawImage(srcBMP, 0, 0, previewImg.Width + 1, previewImg.Height + 1)
        End If
        PreGraph = gr_dest
        Return previewImg
    End Function

    Private Function d2i(ByVal d As Double) As Integer
        Dim i As Integer
        Dim di As Decimal

        i = Int(d)
        di = d - i

        If di >= 0.5 Then
            i = i + 1
        End If

        Return i
    End Function

    Protected Overrides Sub OnPaint(ByVal pe As System.Windows.Forms.PaintEventArgs)
        MyBase.OnPaint(pe)
        'Add your custom paint code here
        Draw()
    End Sub

    'Following 3 functions deal with mouse button messages and remembers select mark coordinate
    Private Sub PictureBox1_MouseDown(ByVal sender As System.Object, _
                                        ByVal e As System.Windows.Forms.MouseEventArgs) _
                                        Handles PictureBox1.MouseDown
        If e.Button = MouseButtons.Left Then
            iStartPos.X = e.X
            iStartPos.Y = e.Y
        ElseIf e.Button = MouseButtons.Right Then
            Menu_Popup.Show(PictureBox1, iEndPos)
        ElseIf e.Button = MouseButtons.Left Then
            iEndPos.X = e.X
            iEndPos.Y = e.Y
        End If
    End Sub

    Private Sub PictureBox1_MouseUp(ByVal sender As System.Object, _
                                    ByVal e As System.Windows.Forms.MouseEventArgs) _
                                    Handles PictureBox1.MouseUp
        iEndPos.X = e.X
        iEndPos.Y = e.Y
    End Sub

    Private Sub PictureBox1_MouseMove(ByVal sender As System.Object, _
                                    ByVal e As System.Windows.Forms.MouseEventArgs) _
                                    Handles PictureBox1.MouseMove

        If e.Button = MouseButtons.Left Then
            iEndPos.X = e.X
            iEndPos.Y = e.Y
            Draw()
        End If

    End Sub

    'Onpaint message helper function
    Private Sub Draw()
        Dim drawImg As Bitmap
        drawImg = FormPreview(OriginalImage, PictureBox1.Height, PictureBox1.Width)
        PictureBox1.Image = drawImg
        DrawSelRect()
    End Sub

    'Draw Mark on the displaying area 
    Private Sub DrawSelRect()
        SelRect.Location = iStartPos

        If iEndPos.X = iStartPos.X And iEndPos.Y = iStartPos.Y Then
            Exit Sub
        End If

        If iStartPos.X > iEndPos.X Or iStartPos.Y > iEndPos.Y Then
            If iStartPos.X > iEndPos.X And iStartPos.Y > iEndPos.Y Then
                SelRect.Location = iEndPos
            ElseIf iStartPos.X < iEndPos.X And iStartPos.Y > iEndPos.Y Then
                SelRect.X = iStartPos.X
                SelRect.Y = iEndPos.Y
            Else
                SelRect.X = iEndPos.X
                SelRect.Y = iStartPos.Y
            End If

        End If

        bufferrec = SelRect


        SelRect.Height = Abs(iEndPos.Y - iStartPos.Y)
        SelRect.Width = Abs(iEndPos.X - iStartPos.X)
        If SelRect.Height > 10 And SelRect.Width > 10 Then
            PreGraph.DrawRectangle(PenDash, SelRect)
        End If

    End Sub

    'Following 3 functions for setting 3 mark widths
    Private Sub MarkWide_Click(ByVal sender As System.Object, _
                                ByVal e As System.EventArgs) _
                                Handles MarkWide.Click
        PenDash.Width = 6
        PenSolid.Width = 6
        MarkWide.Checked = True
        MarkMid.Checked = False
        MarkThin.Checked = False
        Draw()
    End Sub


    Private Sub MarkMid_Click(ByVal sender As System.Object, _
                                ByVal e As System.EventArgs) _
                                Handles MarkMid.Click
        PenDash.Width = 3
        PenSolid.Width = 3
        MarkWide.Checked = False
        MarkMid.Checked = True
        MarkThin.Checked = False
        Draw()
    End Sub

    Private Sub MarkThin_Click(ByVal sender As System.Object, _
                                ByVal e As System.EventArgs) _
                                Handles MarkThin.Click
        PenDash.Width = 1
        PenSolid.Width = 1
        MarkWide.Checked = False
        MarkMid.Checked = False
        MarkThin.Checked = True
        Draw()
    End Sub

    'Following 3 functions for setting different mark color
    Private Sub MarkRed_Click(ByVal sender As System.Object, _
                                ByVal e As System.EventArgs) _
                                Handles MarkRed.Click
        PenDash.Color = Color.Red
        MarkRed.Checked = True
        MarkWhite.Checked = False
        MarkBlack.Checked = False
        Draw()
    End Sub

    Private Sub MarkWhite_Click(ByVal sender As System.Object, _
                                ByVal e As System.EventArgs) _
                                Handles MarkWhite.Click
        PenDash.Color = Color.White
        MarkRed.Checked = False
        MarkWhite.Checked = True
        MarkBlack.Checked = False
        Draw()
    End Sub

    Private Sub MarkBlack_Click(ByVal sender As System.Object, _
                                ByVal e As System.EventArgs) _
                                Handles MarkBlack.Click
        PenDash.Color = Color.Black
        MarkRed.Checked = False
        MarkWhite.Checked = False
        MarkBlack.Checked = True
        Draw()
    End Sub

    Private Sub ClearMark_Click(ByVal sender As System.Object, _
                                ByVal e As System.EventArgs) _
                                Handles ClearMark.Click
        'Clear Mark
        iEndPos.X = iStartPos.X
        iEndPos.Y = iStartPos.Y
        Draw()
    End Sub

    Private Function SelectedArea()
        Dim oriHeight, oriWidth As Double
        Dim ratioH, ratioW As Double
        Dim row, col As Double
        Dim height, width, r, c, i, j As Integer

        oriHeight = OriginalImage.Height
        oriWidth = OriginalImage.Width

        ratioH = oriHeight / PictureBox1.Height
        ratioW = oriWidth / PictureBox1.Width
        'Get left x,y coordinate of selected area in the original image
        row = ratioH * SelRect.Top
        col = ratioW * SelRect.Left
        oriHeight = ratioH * SelRect.Size.Height
        oriWidth = ratioW * SelRect.Size.Width
        'Get the height and width of selected area in the original image
        height = d2i(oriHeight)
        width = d2i(oriWidth)
        'get the upper left coordiante of selected area in the original image
        r = d2i(row)
        c = d2i(col)

        Dim tempimage As New Bitmap(width, height)

        For i = 0 To height - 1
            For j = 0 To width - 1
                tempimage.SetPixel(j, i, OriginalImage.GetPixel(c + j, r + i))
            Next j
        Next i

        SelectedImage = tempimage

    End Function

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.DoubleClick

        If SelRect.Size.Height = 0 Or SelRect.Size.Width = 0 Then
            Exit Sub
        Else
            SelectedArea()
        End If

    End Sub

    Private Sub ConfirmSelection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConfirmSelection.Click

        If SelRect.Size.Height = 0 Or SelRect.Size.Width = 0 Then
            Exit Sub
        Else
            SelectedArea()
        End If
    End Sub
End Class
