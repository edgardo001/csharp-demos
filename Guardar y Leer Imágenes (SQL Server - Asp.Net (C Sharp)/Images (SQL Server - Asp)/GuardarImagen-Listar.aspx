﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GuardarImagen-Listar.aspx.cs" Inherits="Images__SQL_Server___Asp_.GuardarImagen_Listar" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div align="center">
            <h2>Guardar y Leer Imágenes (SQL Server - Asp.Net)</h2>
            <asp:Panel ID="panelRegistrar" runat="server" BackColor="#FFCC00">
                <table border="1">
                    <tr>
                        <td>Descripción:</td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtDescripcion" runat="server" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Fecha:</td>
                        <td style="text-align: left">
                            <asp:TextBox ID="txtFecha" runat="server" TextMode="Date"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Imagen:</td>
                        <td>
                            <asp:FileUpload ID="FileUpload" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click" Width="150px" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Label ID="lblMensaje" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:Panel ID="panelMostrar" runat="server" BackColor="#CCCCFF">
                <table>
                    <tr>
                        <td style="text-align: center">
                            <asp:Button ID="btnNuevo" runat="server" Text="Nuevo" OnClick="btnNuevo_Click" Width="150px" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="gridListado" runat="server" AutoGenerateColumns="False"
                                CellPadding="4" ForeColor="#333333" GridLines="None" AllowPaging="True"
                                OnPageIndexChanging="gridListado_PageIndexChanging" PageSize="5">
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                                <Columns>
                                    <asp:BoundField DataField="codigo" HeaderText="Código" />
                                    <asp:BoundField DataField="descripcion" HeaderText="Descripción" />
                                    <asp:BoundField DataField="fecha" HeaderText="Fecha Registro" DataFormatString="{0:d}" />
                                    <asp:TemplateField HeaderText="Imagen">
                                        <ItemTemplate>
                                            <asp:Image ID="Imagen" runat="server" ImageUrl='<%# Eval("codigo", "Manejador.ashx?codigo={0}") %>'
                                                Width="50px" Height="50px" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#999999" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center">Visítame <a href="http://www.sistemasddm.blogspot.com" title="Sistemas DDM" target="_blank">Sistemas DDM</a>.</td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
    </form>
</body>
</html>
