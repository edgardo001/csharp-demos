﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Web.UI.WebControls;
namespace Images__SQL_Server___Asp_
{
    public partial class GuardarImagen_Listar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                panelRegistrar.Visible = true;
                panelMostrar.Visible = false;    
            }            
        }
        private void borrar() {
            txtDescripcion.Text = "";
            txtFecha.Text = "";
        }
        private Boolean ValidarExtension(string sExtension)
        {
            Boolean verif = false;
            switch (sExtension)
            {
                case ".jpg":
                case ".jpeg":
                case ".png":
                case ".gif":
                case ".bmp":
                    verif = true;
                    break;
                default:
                    verif = false;
                    break;
            }
            return verif;
        }
        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                string Extension = string.Empty;
                string Nombre = string.Empty;

                if (txtDescripcion.Text != "" && txtFecha.Text != "" && FileUpload.HasFile)
                {
                    Nombre = FileUpload.FileName;
                    Extension = Path.GetExtension(Nombre);

                    if (ValidarExtension(Extension))
                    {
                        using (SqlConnection conexi = new SqlConnection(ConfigurationManager.ConnectionStrings["conexion"].ToString()))
                        using (SqlCommand cmd = new SqlCommand("usp_Guardar_Registro", conexi))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("descripcion", txtDescripcion.Text.ToUpper());
                            cmd.Parameters.AddWithValue("fecha", Convert.ToDateTime(txtFecha.Text.ToUpper()));
                            cmd.Parameters.AddWithValue("imagen", FileUpload.FileBytes);
                            cmd.Connection.Open();
                            cmd.ExecuteNonQuery();
                            cmd.Connection.Close();
                        }
                        //Mostrar el panel de Registros
                        borrar();
                        panelMostrar.Visible = true;
                        panelRegistrar.Visible = false;
                        ListarRegistro();
                    }
                    else
                    {
                        lblMensaje.Text = "El archivo no es de tipo imagen.";
                    }
                }
                else
                {
                    lblMensaje.Text = "Datos Incorrectos.";
                }
            }
            catch (Exception ex)
            {
                lblMensaje.Text = ex.Message.ToString();
            }                            
        }
        private void ListarRegistro(){
            try
            {
                using (SqlConnection conexi = new SqlConnection(ConfigurationManager.ConnectionStrings["conexion"].ToString()))
                using (SqlDataAdapter da = new SqlDataAdapter("usp_Listar_Registro", conexi))
                {
                    DataTable tbRegistro = new DataTable();
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.Fill(tbRegistro);
                    gridListado.DataSource = tbRegistro;
                    gridListado.DataBind();
                    Session["Registro"] = tbRegistro;
                }
            }
            catch (Exception)
            {
                throw; 
            }                       
        }
        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            panelRegistrar.Visible = true;
            panelMostrar.Visible = false;
        }
        protected void gridListado_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gridListado.PageIndex = e.NewPageIndex;
                gridListado.DataBind();
                ListarRegistro();
            }
            catch (Exception)
            {
                throw;
            }
        } 

    }
}