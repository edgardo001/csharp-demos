create database bdimagenes
go
use bdimagenes
go
create table registro(
codigo int identity(1,1) primary key,
descripcion varchar(50), 
fecha datetime, 
imagen varbinary (max)
)
go
--Procedimiento para guardar
CREATE PROCEDURE usp_Guardar_Registro
@descripcion varchar(50), 
@fecha datetime, 
@imagen varbinary (max)
as
insert into registro values (@descripcion, @fecha, @imagen)
go
--Procedimiento para Listar
CREATE PROCEDURE usp_Listar_Registro
AS
SELECT codigo, descripcion, fecha, imagen from registro
go