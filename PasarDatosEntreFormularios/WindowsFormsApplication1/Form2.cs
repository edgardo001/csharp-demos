﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            IForm miInterfaz = this.Owner as IForm;
            if (miInterfaz != null)
                miInterfaz.cambiarTexto(textBox1.Text);
            this.Dispose();
        }
    }
}
