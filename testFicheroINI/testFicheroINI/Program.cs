﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ini;

namespace testFicheroINI
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //iniFile();
                iniFile2();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadLine();
            }

        }

        private static void iniFile2()
        {
            try
            {
                //Open the INI file in one of the 3 following ways:

                // Creates or loads an INI file in the same directory as your executable
                // named EXE.ini (where EXE is the name of your executable)
                var MyIni = new IniFile2();

                //// Or specify a specific name in the current dir
                //var MyIni = new IniFile("Settings.ini");

                //// Or specify a specific name in a specific dir
                //var MyIni = new IniFile(@"C:\Settings.ini");


                //==============================================================
                //==============================================================
                //==============================================================

                Console.WriteLine("Escibo en la seccion por defecto 'testFicheroINI'");
                //You can write some values like so:
                MyIni.Write("DefaultVolume", "100");
                MyIni.Write("HomePage", "http://www.google.com");

                //To create a file like this:

                //[MyProg]
                // DefaultVolume=100
                //HomePage=http://www.google.com

                Console.WriteLine("");

                //==============================================================
                //==============================================================
                //==============================================================

                Console.WriteLine("Leo las variables en la seccion por defecto ''testFicheroINI''");
                //To read the values out of the INI file:
                var DefaultVolume = MyIni.Read("DefaultVolume");
                var HomePage = MyIni.Read("HomePage");

                Console.WriteLine(DefaultVolume);
                Console.WriteLine(HomePage);

                Console.WriteLine("");

                //==============================================================
                //==============================================================
                //==============================================================

                Console.WriteLine("Escibo en la seccion 'Audio'");
                Console.WriteLine("Escibo en la seccion 'Web'");
                //Optionally, you can set[Section]'s:
                MyIni.Write("DefaultVolume", "100", "Audio");                
                MyIni.Write("HomePage", "http://www.google.com", "Web");

                //To create a file like this:
                //[Audio]
                //DefaultVolume=100

                //[Web]
                //HomePage=http://www.google.com

                Console.WriteLine("");

                //==============================================================
                //==============================================================
                //==============================================================

                Console.WriteLine("Consulto si la llave existe");

                //You can also check for the existence of a key like so:
                if (!MyIni.KeyExists("DefaultVolume", "Audio"))
                {
                    MyIni.Write("DefaultVolume", "100", "Audio");
                    
                }

                Console.WriteLine("");

                //==============================================================
                //==============================================================
                //==============================================================

                //You can delete a key like so:
                //MyIni.DeleteKey("DefaultVolume", "Audio");

                //You can also delete a whole section(including all keys) like so:
                //MyIni.DeleteSection("Web");



            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        private static void iniFile()
        {
            try
            {
                IniFile ini = new IniFile(@"C:\Users\Datasoft\Desktop\test1.ini");
                ini.IniWriteValue("Prueba", "llave", "v1lor");

                String r = ini.IniReadValue("Prueba", "llave");

                Console.WriteLine(r);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }
    }
}
