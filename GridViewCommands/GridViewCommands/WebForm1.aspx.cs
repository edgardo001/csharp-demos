﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DataAccess;

namespace GridViewCommands
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataBindGridView();
            }
        }

        private void DataBindGridView()
        {
            gvPerson.DataSource = PersonDAL.GetAllPerson();
            gvPerson.DataBind();
        }

        protected void gvPerson_SelectedIndexChanged(object sender, EventArgs e)
        {
            //
            // Se obtiene la fila seleccionada del gridview
            //
            GridViewRow row = gvPerson.SelectedRow;

            //
            // Obtengo el id de la entidad que se esta editando
            // en este caso de la entidad Person
            //
            int id = Convert.ToInt32(gvPerson.DataKeys[row.RowIndex].Value);


        }


    }
}
