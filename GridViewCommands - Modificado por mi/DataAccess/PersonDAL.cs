﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Objects;

namespace DataAccess
{
    public static class PersonDAL
    {

        public static List<Person> GetAllPerson()
        {
            SchoolEntities context = new SchoolEntities();

            return context.Person.ToList();
                     
        }

    }
}
