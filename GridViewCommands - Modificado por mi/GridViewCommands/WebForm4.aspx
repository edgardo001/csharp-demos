﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm4.aspx.cs" Inherits="GridViewCommands.WebForm4" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:GridView ID="gvPerson" runat="server" AutoGenerateColumns="False" 
            CellPadding="4" ForeColor="#333333" 
            GridLines="None" DataKeyNames="PersonID" 
            onselectedindexchanged="gvPerson_SelectedIndexChanged" 
            onrowcommand="gvPerson_RowCommand" >
            <RowStyle BackColor="#EFF3FB" />
            <Columns>
           
                <asp:TemplateField HeaderText="Seleccionar" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:ImageButton Id="imgSeleccion" runat="server" 
                           CommandName="Select" ImageUrl="~/imagenes/editar.gif" 
                           CommandArgument="<%# ((GridViewRow)Container).RowIndex %>" ></asp:ImageButton>
                    </ItemTemplate>
                </asp:TemplateField>
           
                <asp:BoundField DataField="FirstName" HeaderText="First Name" />
                <asp:BoundField DataField="LastName" HeaderText="Last Name" />
                <asp:BoundField DataField="HireDate" HeaderText="Hire Date" 
                    DataFormatString="{0:dd/MM/yyyy}" />
                <asp:BoundField DataField="EnrollmentDate" HeaderText="Enrollment Date" 
                    DataFormatString="{0:dd/MM/yyyy}" />
            </Columns>
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <AlternatingRowStyle BackColor="White" />
        </asp:GridView>
    </div>
    </form>
</body>
</html>
