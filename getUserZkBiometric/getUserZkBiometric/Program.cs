﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using zkemkeeper;//el proyecto debe compilarse en "x86" no en "x64" y tampoco en "Any CPU"
//probado con la version de sdk de ZKTimeNet 2.0

namespace getUserZkBiometric
{
    class Program
    {
        //no es obligacion las variables como estaticas, solo es para el uso dentro de "Main"
        public static CZKEM dispositivo = new CZKEM(); // Objeto de tipo ZK, es lo primero que se crea
        public static int error = 5; //Variable para obtener diferentes errores ocurridos
        private static int iMachineNumber = 1; //Representara el numero de serie del equipo, es usado en muchas funciones
        private static bool conexion = false; //Variable que checa el estado de la conexion

        static void Main(string[] args)
        {
            try
            {
                List<usrBiometrico> l = getUsuarios("192.168.15.52", 4370);
                Console.WriteLine("Cantidad de Usuarios obtenida: " + l.Count);

                //Los usuarios se repiten por la cantidad de huellas tenga cada usuario
                //si el usuario posee 3 huellas registradas, esto motrara 3 veces al usuario
                //dentro de las pruebas, algunos usuario aparecen 2 veces, pero solo figuran con una huella, se desconoce el motivo
                foreach (usrBiometrico item in l)
                {
                    Console.WriteLine(item.ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadLine();
            }
        }

        public static List<usrBiometrico> getUsuarios(String ipDipositivo, int puerto)
        {
            try
            {
                conexion = dispositivo.Connect_Net(ipDipositivo, puerto); //Parametros: IP del dispositivo y puerto del dispositivo
                dispositivo.RefreshData(iMachineNumber);
                List<usrBiometrico> l = new List<usrBiometrico>();

                int num = 1;
                //Variables de usuario
                string userID, nombreUsuario, Password;
                int tipoDeUsuario; //Super Administrador = 3, Usuario = 0
                bool usuarioHabilitado; //Si el usuario esta habilitado manda true
                                        //Variables de huella
                int numeroHuella; //Podemos poner hasta 10 huellas (nuestros 10 dedos)
                string templateHuella;
                int tamanoHuella;
                int huellaValida;//0=huella invalida, 1=valida,2=Modificada (falsa)

                dispositivo.EnableDevice(iMachineNumber, false); //Deshabilitar el equipo para la extraccion de datos

                //Lectura de datos en el dispositivo
                dispositivo.ReadAllUserID(iMachineNumber); //Lee User ID, password, nombre, y numero de tarjeta, sin esto, getAllUserID o getAllUserInfo no funciona
                dispositivo.ReadAllTemplate(iMachineNumber);// 

                //Obtencion de datos del equipo
                while (dispositivo.SSR_GetAllUserInfo(iMachineNumber, out userID, out nombreUsuario, out Password, out tipoDeUsuario, out usuarioHabilitado))//Extraer toda la informacion de usuario
                {
                    for (numeroHuella = 0; numeroHuella < 10; numeroHuella++) //Como solo tenemos 10 huellas, por eso el loop es de 0 a 9
                    {
                        if (dispositivo.GetUserTmpExStr(iMachineNumber, userID, numeroHuella, out huellaValida, out templateHuella, out tamanoHuella) || (numeroHuella == 0)) //El OR es por si el usuario no tiene huella, tambien aparezca
                        {
                            //dgvUsuarios.Rows.Add(num, userID, nombreUsuario, Password, privilegioUser, usuarioHabilitado, numeroHuella, tamanoHuella, templateHuella, huellaValida); //Mostrar datos en la tabla
                            usrBiometrico u = new usrBiometrico();
                            u.Id = userID;
                            u.nombre = nombreUsuario;
                            l.Add(u);
                            num++;
                        }
                    }
                }

                dispositivo.EnableDevice(iMachineNumber, true); //Volver a habilitar el equipo
                return l;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }

    public class usrBiometrico
    {
        public String Id { get; set; }
        public String nombre { get; set; }

        public string ToString()
        {
            return "id: " + Id + "\n" + 
                "nombre: " + nombre + "\n";
        }
    }
}
