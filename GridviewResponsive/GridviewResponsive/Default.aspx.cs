﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GridviewResponsive
{
    public partial class Default : System.Web.UI.Page
    {
        NorthwindDataContext dc = new NorthwindDataContext();
        protected void Page_Load(object sender, EventArgs e)
        {
            var qry = from s in dc.Customers
                      select s;
            grdCustomer.DataSource = qry;
            grdCustomer.DataBind();
        }
    }
}