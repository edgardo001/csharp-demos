﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace conexionAccess
{
    class Program
    {
        static void Main(string[] args)
        {
            ConnectToAccess("212683647");
            Console.ReadLine();
        }

        // C#
        public static void ConnectToAccess(String buscar)
        {
            OleDbConnection conn = new
            OleDbConnection();
            // TODO: Modify the connection string and include any
            // additional required properties for your database.
            conn.ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0; Data source= att2000.mdb";
            try
            {
                conn.Open();
                // Insert code to process data.
                Console.WriteLine("Conexion OK a access");

                string sql = "SELECT * FROM USERINFO where Badgenumber = @Badgenumber";
                OleDbCommand command = new OleDbCommand(sql, conn);
                command.Parameters.AddWithValue("@Badgenumber", buscar);
                OleDbDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Console.WriteLine("Valor obtenido desde Access: " + reader["Badgenumber"].ToString() + " - " + reader["Name"].ToString());
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to connect to data source: " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
