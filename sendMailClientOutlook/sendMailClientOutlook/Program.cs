﻿using Microsoft.Office.Interop.Outlook;//se debe agregar en las referencias del proyecto
// probado con Microsoft.Office.Interop.Outlook "15" y una cuenta de outlook de registrada en el cliente
// Microsoft.Office.Interop.Outlook "14" entrega error de identificador COM de la clase

// caracteristicas del equipo de prueba: 
// windows 7 64 bit
// office 2016 con outlook 2016

using System;

namespace sendMailClientOutlook
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //se debe configurar cliente outlook previamente, con un correo valido
                //dependiendo la configuracion de outlook, solicitara clave o autorizacion de envio o ambas
                Boolean flag = SendEmailWithOutlook("evasquez@datasoft.cl", "Asunto de prueba", "Cuerpo del mesaje de prueba");
                if (flag)
                {
                    Console.WriteLine("Mensaje enviado");
                }
                else
                {
                    Console.WriteLine("Mensaje no pudo ser envido, se desconoce el motivo.");
                }
                
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Metodo encargado de 
        /// </summary>
        /// <param name="mailDirection">Correo de destino</param>
        /// <param name="mailSubject">Asunto del correo</param>
        /// <param name="mailContent">Cuerpo del correo</param>
        /// <returns></returns>
        public static Boolean SendEmailWithOutlook(string mailDirection, string mailSubject, string mailContent)
        {
            try
            {
                var oApp = new Microsoft.Office.Interop.Outlook.Application();

                NameSpace ns = oApp.GetNamespace("MAPI");
                var f = ns.GetDefaultFolder(OlDefaultFolders.olFolderInbox);

                System.Threading.Thread.Sleep(1000);

                var mailItem = (MailItem)oApp.CreateItem(OlItemType.olMailItem);
                mailItem.Subject = mailSubject;
                mailItem.HTMLBody = mailContent;
                mailItem.To = mailDirection;
                mailItem.Send();
                return true;
            }
            catch (System.Exception ex)
            {
                throw new System.Exception(ex.Message);
            }
        }
    }
}
