﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DatabaseDAL;

namespace DropDownListCargaCascada
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadComboArtist();
            }
        }

        protected void ddlArtist_SelectedIndexChanged(object sender, EventArgs e)
        {
            int ArtistId = Convert.ToInt32(ddlArtist.SelectedValue);

            LoadComboAlbum(ArtistId);
            
        }

        protected void ddlAlbum_SelectedIndexChanged(object sender, EventArgs e)
        {
            int AlbumId = Convert.ToInt32(ddlAlbum.SelectedValue);

            LoadComboTrack(AlbumId);
        }

        protected void ddlTrack_SelectedIndexChanged(object sender, EventArgs e)
        {
            int TrackId = Convert.ToInt32(ddlTrack.SelectedValue);

            LoadGridViewCustomer(TrackId);
        }

        private void LoadComboArtist()
        {
            ddlArtist.DataSource = ChinookDAL.GellAllArtist();
            ddlArtist.DataTextField = "Name";
            ddlArtist.DataValueField = "ArtistId";
            ddlArtist.DataBind();

            if (ddlArtist.Items.Count != 0)
            {
                int ArtistId = Convert.ToInt32(ddlArtist.SelectedValue);

                LoadComboAlbum(ArtistId);
            }
            else
            {
                ddlAlbum.Items.Clear();
                ddlTrack.Items.Clear();
                dvCustomer.DataSource = null;
                dvCustomer.DataBind();
            }
        }

        private void LoadComboAlbum(int ArtistId)
        {
            ddlAlbum.DataSource = ChinookDAL.GellAlbumByArtist(ArtistId);
            ddlAlbum.DataTextField = "Title";
            ddlAlbum.DataValueField = "AlbumId";
            ddlAlbum.DataBind();


            if (ddlAlbum.Items.Count != 0)
            {
                int AlbumId = Convert.ToInt32(ddlAlbum.SelectedValue);

                LoadComboTrack(AlbumId);
            }
            else
            {
                ddlTrack.Items.Clear();
                dvCustomer.DataSource = null;
                dvCustomer.DataBind();
            }
        }

        private void LoadComboTrack(int AlbumId)
        {
            ddlTrack.DataSource = ChinookDAL.GellTrackByAlbum(AlbumId);
            ddlTrack.DataTextField = "Name";
            ddlTrack.DataValueField = "TrackId";
            ddlTrack.DataBind();

            if (ddlTrack.Items.Count != 0)
            {
                int TrackId = Convert.ToInt32(ddlTrack.SelectedValue);

                LoadGridViewCustomer(TrackId);
            }
            else
            {
                dvCustomer.DataSource = null;
                dvCustomer.DataBind();
            }
        }

        private void LoadGridViewCustomer(int TrackId)
        {
            dvCustomer.DataSource = ChinookDAL.GellCustomerByTrack(TrackId);
            dvCustomer.DataBind();
        }

    }
}
