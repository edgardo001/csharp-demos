﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="DropDownListCargaCascada._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="style1">
            <tr>
                <td >
                    Artist:
                </td>
                <td>
                    <asp:DropDownList ID="ddlArtist" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="ddlArtist_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td >
                    Album:
                </td>
                <td>
                    <asp:DropDownList ID="ddlAlbum" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="ddlAlbum_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td >
                    Track:</td>
                <td>
                    <asp:DropDownList ID="ddlTrack" runat="server" AutoPostBack="True" 
                        onselectedindexchanged="ddlTrack_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
            <td colspan="2">
                Clientes
            </td>
            </tr>
            <tr>
                <td colspan="2" align="left">
                    <asp:GridView ID="dvCustomer" runat="server" AutoGenerateColumns="False" 
                        CellPadding="4" ForeColor="#333333" GridLines="None">
                        <RowStyle BackColor="#EFF3FB" />
                        <Columns>
                           <asp:TemplateField HeaderText="Nombre">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" 
                                        Text='<%# string.Format("{0}, {1}", Eval("LastName"), Eval("FirstName"))%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Direccion">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" 
                                        Text='<%# string.Format("{0} - {1}, {2}", Eval("Address"), Eval("City"), Eval("State"))%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <EditRowStyle BackColor="#2461BF" />
                        <AlternatingRowStyle BackColor="White" />
                    </asp:GridView>
                </td>

            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
