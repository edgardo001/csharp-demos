﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;


namespace DatabaseDAL
{
    public class ChinookDAL
    {

        public static List<Artist> GellAllArtist()
        {
            string sql = @"SELECT ArtistId, Name FROM Artist ORDER BY Name ASC";

            List<Artist> list = new List<Artist>();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(sql, conn);

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    list.Add(LoadArtist(reader));
                }

            }

            return list;
        }

        private static Artist LoadArtist(IDataReader reader)
        {
            Artist item = new Artist();

            item.ArtistId = Convert.ToInt32(reader["ArtistId"]);
            item.Name = Convert.ToString(reader["Name"]);

            return item;
        }



        public static List<Album> GellAlbumByArtist(int ArtistId)
        {
            string sql = @"SELECT AlbumId, Title, ArtistId FROM Album WHERE ArtistId = @ArtistId";

            List<Album> list = new List<Album>();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@ArtistId", ArtistId);

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    list.Add(LoadAlbum(reader));
                }

            }

            return list;
        }

        private static Album LoadAlbum(IDataReader reader)
        {
            Album item = new Album();

            item.AlbumId = Convert.ToInt32(reader["AlbumId"]);
            item.Title = Convert.ToString(reader["Title"]);

            return item;
        }



        public static List<Track> GellTrackByAlbum(int AlbumId)
        {
            string sql = @"SELECT TrackId, Name FROM Track WHERE AlbumId = @AlbumId";

            List<Track> list = new List<Track>();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@AlbumId", AlbumId);

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    list.Add(LoadTrack(reader));
                }

            }

            return list;
        }

        private static Track LoadTrack(IDataReader reader)
        {
            Track item = new Track();

            item.TrackId = Convert.ToInt32(reader["TrackId"]);
            item.Name = Convert.ToString(reader["Name"]);


            return item;
        }



        public static List<Customer> GellCustomerByTrack(int TrackId)
        {
            string sql = @"SELECT C.FirstName, C.LastName, C.Address, C.City, C.State
                            FROM Customer AS C INNER JOIN
                              Invoice AS I ON C.CustomerId = I.CustomerId INNER JOIN
                              InvoiceLine AS IL ON I.InvoiceId = IL.InvoiceId INNER JOIN
                              Track AS T ON IL.TrackId = T.TrackId
                            WHERE (T.TrackId = @TrackId)";

            List<Customer> list = new List<Customer>();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["default"].ToString()))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@TrackId", TrackId);

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    list.Add(LoadCustomer(reader));
                }

            }

            return list;
        }

        private static Customer LoadCustomer(IDataReader reader)
        {
            Customer item = new Customer();

            item.FirstName = Convert.ToString(reader["FirstName"]);
            item.LastName = Convert.ToString(reader["LastName"]);
            item.Address = Convert.ToString(reader["Address"]);
            item.City = Convert.ToString(reader["City"]);
            item.State = Convert.ToString(reader["State"]);

            return item;
        }
    }
}
