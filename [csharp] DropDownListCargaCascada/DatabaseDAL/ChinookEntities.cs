﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DatabaseDAL
{
    public class Artist
    {
        public int ArtistId { get; set; }
        public string Name { get; set; }

    }

    public class Album
    {
        public int AlbumId { get; set; }
        public string Title { get; set; }

    }

    public class Track
    {
        public int TrackId { get; set; }
        public string Name { get; set; }

    }

    public class Customer
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }

    }
}
