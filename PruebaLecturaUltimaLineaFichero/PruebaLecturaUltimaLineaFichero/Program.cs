﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace PruebaLecturaUltimaLineaFichero
{
    class Program
    {
        static void Main(string[] args)
        {
            PruebaLecturaUltimaLineaFichero(@"c:\fichero.txt");
        }

        private static void PruebaLecturaUltimaLineaFichero(string p)
        {
            string[] lineas = File.ReadAllLines(p);
            String ultimaLinea = lineas[lineas.Length - 1].ToString();
            Console.WriteLine(ultimaLinea);
            Console.ReadLine();


            string cadenaBuscada = "8";
            bool firstCharacter = ultimaLinea.Contains(cadenaBuscada);

            Console.WriteLine("First occurrence: {0}", firstCharacter);
            Console.ReadLine();
        }


    }
}
